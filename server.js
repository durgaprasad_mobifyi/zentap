//Install express server
require('dotenv').config()
const express = require('express');
const compression = require('compression');
const path = require('path');
const cors = require('cors');
// const SlackBot = require('slackbots');
var bodyParser = require("body-parser");
if (!process.env.PORT) {
  var fs = require('fs');
  var https = require('https');
  var privateKey = fs.readFileSync('ssl/server.key', 'utf8');
  var certificate = fs.readFileSync('ssl/server.crt', 'utf8');
  var credentials = {
    key: privateKey,
    cert: certificate
  };
}
const app = express();
// const bot = new SlackBot({
//   token: process.env.SLACK_TOKEN, // Add a bot https://my.slack.com/services/new/bot and put the token
//   name: 'Fuckup Bot'
// });
app.use(cors());
app.use(compression());
app.options('*', cors());
app.use(function (req, res, next) {
  if ((process.env.ENVIRONMENT === 'development' || req.get('X-Forwarded-Proto') !== 'https') && !req.secure) {
    const newDest = 'https://' + req.get('Host') + req.url
    console.log(newDest, req.get('X-Forwarded-Proto'), req.secure);
    res.redirect(newDest);
  } else
    next();
});
// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/client-dashboard'));
app.use(bodyParser.json());
app.post('/error', function (req, res, next) {
  // bot.postMessage('#customer_dash_errors', req.body.text, req.body)
  //   .always(function (data) {
  //     res.setHeader('Content-Type', 'application/json');
  //     res.send(data);
  //   });
  res.send(200);

});
app.get('/*', function (req, res) {

  res.sendFile(path.join(__dirname + '/dist/client-dashboard/index.html'));
});
if (!process.env.PORT) {
  var httpsServer = https.createServer(credentials, app);
  httpsServer.listen(4200)
} else {
  // Start the app by listening on the default Heroku port
  app.listen(process.env.PORT || 8080);
}
