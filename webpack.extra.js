const GitRevisionPlugin = require('git-revision-webpack-plugin');
const gitRevisionPlugin = new GitRevisionPlugin()
const webpack = require('webpack');
const {
  resolve,
  relative
} = require('path');
const {
  writeFileSync
} = require('fs');
const file = resolve(__dirname, 'src', 'environments', 'version.ts');
writeFileSync(file,
  `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
export const VERSION = ${JSON.stringify(gitRevisionPlugin.version(), null, 4)};
/* tslint:enable */
`, {
    encoding: 'utf-8'
  });

console.log(`Wrote version info ${gitRevisionPlugin.version()} to ${relative(resolve(__dirname, '..'), file)}`);
module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      "VERSION": JSON.stringify(gitRevisionPlugin.version())
    })
  ]
};
