import { Directive, HostListener, ElementRef, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NumberInputFormatterPipe } from './number-input-formatter.pipe';

@Directive({
  selector: '[appNumberFormatter]'
})
export class NumberFormatterDirective implements OnInit {
  private el: HTMLInputElement;
  @Input()
  decimalPlaces = 2;
  @Output()
  parsedValue = new EventEmitter();

  constructor(
    private elementRef: ElementRef,
    private currencyPipe: NumberInputFormatterPipe
  ) {
    this.el = this.elementRef.nativeElement;
  }

  ngOnInit() {
    this.el.value = this.currencyPipe.transform(this.el.value);

  }

  @HostListener('ngOnChanges', [ '$event' ])
  onChange(value) {
    // console.log("Model CHanged")
    this.el.value = this.currencyPipe.transform(this.currencyPipe.parse(value, this.decimalPlaces), this.decimalPlaces);
    this.parsedValue.emit(this.currencyPipe.parse(value, this.decimalPlaces));
  }
  @HostListener('focus', [ '$event.target.value' ])
  onFocus(value) {
    this.el.value = this.currencyPipe.transform(this.currencyPipe.parse(value, this.decimalPlaces), this.decimalPlaces);
    this.parsedValue.emit(this.currencyPipe.parse(value, this.decimalPlaces));
  }

  @HostListener('keyup', [ '$event.target.value' ])
  onKeyup(value: string) {
    this.el.value = this.currencyPipe.transform(this.currencyPipe.parse(value, 0, false), 0, false);
    this.parsedValue.emit(this.currencyPipe.parse(value, this.decimalPlaces));
  }

  @HostListener('blur', [ '$event.target.value' ])
  onBlur(value) {
    this.el.value = this.currencyPipe.transform(this.currencyPipe.parse(value, this.decimalPlaces), this.decimalPlaces);
    this.parsedValue.emit(this.currencyPipe.parse(value, this.decimalPlaces));
  }


}
