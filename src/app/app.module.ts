import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FacebookModule } from 'ngx-facebook';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTokenInterceptor } from './services/jwt/http.token.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { JwtService } from 'src/app/services/jwt/jwt.service';
import { SharedModule } from './shared/shared.module';
import { MccColorPickerModule } from 'material-community-components';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NumberInputFormatterPipe } from './pipes/formatting/number-input-formatter.pipe';
import { DeviceDetectorModule } from 'ngx-device-detector';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ScrollDispatcher, ScrollingModule} from '@angular/cdk/scrolling';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {WindowService} from './services/window/window.service';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {AppGuard} from './app.guard';
import {LoginGuard} from './components/login/login.guard';
import { OnboardingGuard } from './components/registration-form/onboarding.guard';
import { ContentPreviewDialogComponent } from './shared/content-preview-dialog/content-preview-dialog.component';
import {ImageCropperDialogComponent} from './shared/image-cropper-dialog/image-cropper-dialog.component';
import {SidebarComponent} from './shared/sidebar/sidebar.component';
import {NotificationTabComponent} from './shared/notification-tab/notification-tab.component';
import {BreadCrumComponent} from './shared/bread-crum/bread-crum.component';
import {InsightsAnalyticsComponent} from './shared/insights-analytics/insights-analytics.component';
import { HomeComponent } from './components/home/home.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {ContentFormComponent} from './shared/content-form/content-form.component';
import {CustomerFormComponent} from './shared/customer-form/customer-form.component';
import {ImageGalleryComponent} from './shared/image-gallery/image-gallery.component';
import {GenericDialogComponent} from './shared/generic-dialog/generic-dialog.component';
import {NumberFormatterDirective} from './pipes/formatting/number-formatter.directive';
import {ContactsUploaderComponent} from './shared/contacts-uploader/contacts-uploader.component';
import {UsericonComponent} from './shared/usericon/usericon.component';
import {UpgradeDialogComponent} from './shared/upgrade-dialog/upgrade-dialog.component';
import {ListingImageGalleryComponent} from './shared/listing-image-gallery/listing-image-gallery.component';
import {SetPasswordFormComponent} from './shared/set-password-form/set-password-form.component';
import {StripeCardCollectorComponent} from './shared/stripe-card-collector/stripe-card-collector.component';
import {FacebookStepsComponent} from './components/facebook-steps/facebook-steps.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {PlansComponent} from './components/plans/plans.component';
import { SocialPostDialogComponent } from './shared/social-post-dialog/social-post-dialog.component';
import { ContentReceiptDialogComponent } from './shared/content-receipt-dialog/content-receipt-dialog.component';
import { LoginComponent } from './components/login/login.component';

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    NotificationTabComponent,
    SidebarComponent,
    BreadCrumComponent,
    InsightsAnalyticsComponent,
    DashboardComponent,
    ImageGalleryComponent,
    GenericDialogComponent,
    FacebookStepsComponent,
    NumberFormatterDirective,
    NotFoundComponent,
    ContactsUploaderComponent,
    UsericonComponent,
    SetPasswordFormComponent,
    PlansComponent,
    ContentPreviewDialogComponent,
    SocialPostDialogComponent,
    HomeComponent,
    UpgradeDialogComponent,
    ListingImageGalleryComponent,
    ContentReceiptDialogComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    FacebookModule.forRoot(),
    MccColorPickerModule.forRoot({
      empty_color: 'FFFFFF'
    }),
    FontAwesomeModule,
    DeviceDetectorModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    SharedModule,
    FlexLayoutModule,
    ScrollingModule,
    InfiniteScrollModule,
    DeviceDetectorModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    AuthenticationService,
    JwtService,
    NumberInputFormatterPipe,
    ScrollDispatcher,
    AppGuard,
    OnboardingGuard,
    LoginGuard,
    WindowService,
    { provide: DateAdapter, useClass: MomentDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }

  ],
  entryComponents: [ ImageGalleryComponent, GenericDialogComponent, FacebookStepsComponent,
    StripeCardCollectorComponent, ContactsUploaderComponent, UsericonComponent, UpgradeDialogComponent, ListingImageGalleryComponent,
    SetPasswordFormComponent, ContentPreviewDialogComponent, SocialPostDialogComponent,
    ContentReceiptDialogComponent, ImageCropperDialogComponent ],
  bootstrap: [ AppComponent ]
})

/**
 * Main app module. Import your submodules here.
 */
export class AppModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    library.add(fab, fas, far);
    // bugsnagClient.notify(new Error('Test error'));
  }
}
