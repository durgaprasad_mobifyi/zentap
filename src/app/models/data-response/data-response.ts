export interface DataResponse<T> {

    /**
     * links?
     */
    links?: {
        next: string;
        prev: string;
    };

    /**
     * data
     */
    data: T;

}
