import {GenericDialogConfig} from '../../shared/generic-dialog/generic-dialog.component';

export interface CardColors {
  bg?: string;
  bgSelected?: string;
  text?: string;
  textSelected?: string;
  hoverColor?: string;
}
export interface CardButtonConfig {
  id: any;
  title: string;
  actionText: string;
  centerText?: string;
  icon?: string;
  image?: string;
  link?: string;
  unavailable?: GenericDialogConfig;
  colors?: Partial<CardColors>;
  buttonConfigs?: { name: string; action: string; path: string }[];
  antIcon?: string;
  antTheme?: string;
  queryParams?: object;
  topText?: string;
  description?: string;
  createdAt?: any;
  isSkeleton?: boolean;
}
