import { Component, OnInit } from '@angular/core';
import { Notification } from '../../vos/notification/notification';
import {NotificationService} from '../../services/notification/notification.service';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {SocialPostDialogComponent} from '../../shared/social-post-dialog/social-post-dialog.component';
import {NzModalService} from 'ng-zorro-antd/modal';
import {Content} from '../../vos/content/content';
import {ContentsService} from '../../services/contents/contents.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: [ './notification.component.scss' ]
})

export class NotificationComponent implements OnInit {
  loading = true;
  nloading = false;
  markAllRead = false;
  notifications: Notification[];
  pageSizeOptions = [10];
  notificationPagination = {
    limit: 10,
    total: 10,
    page: 1,
    totalPages: 10
  };
  isLinkedin = false;
  content: Content = new Content();
  constructor(
    private notificationService: NotificationService,
    private breadcrumService: BreadCrumService,
    private modalService: NzModalService,
    private contentService: ContentsService,
  ) {}
  ngOnInit() {
    this.fetch_notifications();
    this.breadcrumService.set_breadcrum();
  }
  mark_read(notification_id: number, opened: boolean, notification?) {
    const data = {
      id: notification_id,
      mark_single: true
    };
    if (!opened) {
      this.nloading = true;
      this.notificationService.mark_read(notification_id).subscribe();
      const index = this.notifications.map(function(e) { return e.id; }).indexOf(notification_id);
      this.nloading = false;
      this.notifications[index].opened = 'true';
      this.notificationService.sendClearNotification(data);
    }
    if (notification.key === 'content.linkedin.expired') {
      this.connectLinkedin(notification.notifiable_id);
    }
  }
  mark_read_all() {
    const data = {
      status: true,
      mark_single: false
    };
    this.markAllRead = true;
    this.notificationService.mark_read_all().subscribe();
    this.notificationService.sendClearNotification(data);
  }
  didPage(pageIndex) {
    this.notificationPagination.page = pageIndex;
    this.loading = true;
    this.fetch_notifications();
  }
  fetch_notifications() {
    const pageNo = this.notificationPagination.page;
    const query = `?page=${pageNo}`;
    this.notificationService.list(query).subscribe(n => {
      this.notificationPagination.limit = n['per_page'];
      this.notificationPagination.total = n['total_entries'];
      const limit = this.notificationPagination.limit;
      const total_items = this.notificationPagination.total;
      this.notificationPagination.totalPages = Math.ceil(total_items / limit);
      this.notifications = n.data;
      this.loading = false;
    });
  }
  checkNotificationLink(notification) {
   return notification.group_member_count === 0 && notification.type === 'Website';
  }
  connectLinkedin(contentId) {
    this.fetchContent(contentId);
    const modal = this.modalService.create({
      nzContent: SocialPostDialogComponent,
      nzComponentParams: {
        content: this.content,
        hideFacebook: false
      },
      nzFooter: null
    });
    modal.afterClose.subscribe(response => {
    });
  }
  fetchContent(contentId) {
  this.contentService.show(contentId).subscribe(content => {
    this.content = content.data;
  });
  }
}
