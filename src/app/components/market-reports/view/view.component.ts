import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { CardButtonConfig } from 'src/app/models';
import { Product } from 'src/app/vos/product/product';
import { ProductsService } from 'src/app/services/products/product-api.service';
import { Content } from 'src/app/vos/content/content';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {MarketReport} from '../../../vos/market-report/market-report';
import {MarketReportsService} from '../../../services/market-reports/market-reports.service';
import {BreadCrumService} from '../../../services/breadcrum/bread-crum.service';
import {ContentsService} from '../../../services/contents/contents.service';
import {GenericDialogComponent} from '../../../shared/generic-dialog/generic-dialog.component';

@Component({
  selector: 'app-listings-view',
  templateUrl: './view.component.html',
  styleUrls: [ './view.component.scss' ]
})
export class MarketReportsViewComponent implements OnInit {
  marketreport_id;
  marketreport: MarketReport;
  products: CardButtonConfig[] = [];
  contents: Content[];
  customer: Customer;
  loading = false;
  insights = [];
  nocontents = false;
  get editLink(): string {
    return `/market_reports/${this.marketreport_id}/edit`;
  }
  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private marketreportService: MarketReportsService,
    private contentService: ContentsService,
    private breadcrumService: BreadCrumService,
    private router: Router,
    public message: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductsService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.customer = this.authService.currentCustomerValue;
    this.marketreport =  this.route.snapshot.data[ 'data'];
    this.marketreport_id = this.route.snapshot.paramMap.get('id');
    for (let i = 0; i < 4; i++) {
      this.products.push({ id: '', title: '', actionText: '', isSkeleton: true })
    }
    this.marketreportService.insights(this.marketreport.id).subscribe(response => {
        this.insights = response;
        this.loading = false;
      }, err => {
        this.loading = false;
      }
    );
    this.fetchMarketProducts();
    // const products: Product[] = this.marketreport.products.map((product: Product) => new Product(product));
    // this.products = products.map(p => p.cardButton(this.customer, 'market_reports', this.marketreport));
    // Get All contents of Market Report
    this.contentService.listFilter(this.marketreport.id).subscribe( response => {
      this.contents = response.data;
      this.nocontents = this.contents.length === 0;
    });
    this.breadcrumService.set_breadcrum();
  }

  fetchMarketProducts() {
    const params: Record<string, any> = {
      'q[additional_service_true]': 'false',
      'q[available_true]': 'true',
      'q[s]': 'rank asc'
    };

    this.productService.productList(
      `customers/${this.customer.id}/products`,
      params
    ).subscribe( resp => {
      const products: Product[] = resp.data
        .filter(pr => pr.parent_type.includes('market_report'))
        .map((product: Product) => new Product(product));
      this.products = products.map(pd => pd.cardButton(this.customer, 'market_reports', this.marketreport));
    });
  }
  contentDeleted(content: Content) {
    this.contents = this.contents.filter(c => c.id !== content.id);
  }
  generateInsight(obj) {
    const keys = Object.keys(obj);
    return keys;
  }
  generateFirstColumn(obj) {
    const key = Object.keys(obj)[0];
    const childKey = Object.keys(obj[key])[0];
    return childKey;
  }
  generatesecondColumn(obj) {
    const key = Object.keys(obj)[0];
    const childKey = Object.keys(obj[key])[1];
    return childKey;
  }
  getFormatedDate(insight_date) {
    const d = insight_date.split('-');
    return d[1] + '-' + d[0];
  }
  camelToTitle(key) {
    // no side-effects
    return key
    // inject space before the upper case letters
      .replace(/([A-Z])/g, function(match) {
        return ' ' + match;
      })
      // replace first char with upper case
      .replace(/^./, function(match) {
        return match.toUpperCase();
      });
  }
  deleteReport() {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: 'Are you sure you want to delete this market report?',
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.loading = true;
        this.marketreportService.destroy(this.marketreport).subscribe(r => {
          this.router.navigateByUrl('/');
        }, e => {
          this.loading = false;
          this.message.create('error', 'Error deleting this market report. Please try again');
        });
      }
    });
   }
}
