import { Component, OnInit } from '@angular/core';
import {  MarketReportsService } from 'src/app/services/market-reports/market-reports.service';
import { MarketReport } from 'src/app/vos/market-report/market-report';
import { CardButtonConfig } from 'src/app/models';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';

@Component({
  selector: 'app-marketreports',
  templateUrl: './market-reports.component.html',
  styleUrls: ['./market-reports.component.scss']
})
export class MarketReportsComponent implements OnInit {
  marketreports: MarketReport[] = [];
  loading = false;
  maxmarketreports = 5;
  disableNew = false;
  newCardButton: CardButtonConfig = {
    id: 'new',
    title: 'CREATE',
    actionText: 'NEW MARKET REPORT',
    icon: 'fas plus',
    link: 'market_reports/new',
    colors: {
      bg: '#f4faff',
      text: '#326771',
      textSelected: '#f4faff'
    }
  };
  constructor(
    private marketreportService: MarketReportsService,
    private breadcrumservice: BreadCrumService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.marketreportService.list().subscribe(response => {
      this.loading = false;
      this.marketreports = response.data;
      this.removeAddMarketReportCard();
    });
    this.breadcrumservice.set_breadcrum();
  }

  configFor(marketReport: MarketReport): CardButtonConfig {
    return {
      id: marketReport.id,
      title: 'EDIT',
      actionText: marketReport.zips.join(', '),
      // centerText: marketReport.region_name,
      centerText: `${marketReport.region_name} (${marketReport.zips.join(', ')})`,
      link: `market_reports/${marketReport.id}`,
      colors: {
        bg: '#ffaf47',
        textSelected: 'white'
      }
    };
  }
  removeAddMarketReportCard() {
    console.log(this.marketreports.length);
    if (this.marketreports.length < this.maxmarketreports) {
      this.disableNew = true;
    }
  }
}
