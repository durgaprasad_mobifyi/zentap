import {
  Component,
  OnInit,
  HostListener,
  ViewChild
} from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { Product } from 'src/app/vos/product/product';
import { SwiperComponent } from 'ngx-useful-swiper';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ProductsService } from '../../services/products/product-api.service';
import { CardButtonConfig } from '../../models/interfaces';
import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';
import { GlobalsService } from '../../services/globals/globals.service';
import { NzModalService } from 'ng-zorro-antd';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('usefulSwiper') usefulSwiper: SwiperComponent;
  productsConfigs: CardButtonConfig[] = [];
  isMobile = false;
  isTablet = false;
  isDesktop = false;
  productSwipe = this.globalsService.getSwiperOptions();
  public innerWidth: any;
  customer: Customer;
  cardType = "dashboard-card"
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.productSwipe.slidesPerView = Math.floor(event.target.innerWidth / 250);
    this.productSwipe.slidesPerGroup = Math.floor(event.target.innerWidth / 250);
  }
  constructor(
    private authService: AuthenticationService,
    private deviceService: DeviceDetectorService,
    private productsService: ProductsService,
    private breadcrumService: BreadCrumService,
    private globalsService: GlobalsService,
  ) { }
  ngOnInit() {
    this.customer = this.authService.currentCustomerValue;
    this.authService.currentCustomer.subscribe(c => {
      if (c) {
        this.setCustomer(c);
      }
    });

    for (let i = 0; i < 9; i++) {
      this.productsConfigs.push({ id: '', title: '', actionText: '', isSkeleton: true })
    }
    this.breadcrumService.set_breadcrum();
    this.detectDevice();
  }

  detectDevice() {
    this.isMobile = this.deviceService.isMobile();
    this.isTablet = this.deviceService.isTablet();
    this.isDesktop = this.deviceService.isDesktop();
  }
  setCustomer(c: Customer) {
    this.customer = c;
    this.fetchCustomerProducts();
  }
  fetchCustomerProducts() {
    const params: Record<string, any> = {
      'q[additional_service_true]': 'false',
      'q[featured_true]': 'true',
      'q[s][]': ['rank asc', 'available desc']
    };

    this.productsService.productList(
      `customers/${this.customer.id}/products`,
      params
    ).subscribe(resp => {
      const products: Product[] = resp.data.map((product: Product) => new Product(product));
      this.productsConfigs = products.map(pd => pd.cardButton(this.customer, pd.parent_type[0]));
    });
  }

  socialMediaConnected(sm: string): boolean {
    switch (sm) {
      case 'facebook':
        return true;
      default:
        return false;
    }
  }

}
