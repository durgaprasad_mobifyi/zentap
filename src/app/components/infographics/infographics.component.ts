import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { Content } from 'src/app/vos/content/content';
import { InfographicsService } from 'src/app/services/infographics/infographics.service';
import { Infographic } from 'src/app/vos/infographic/infographic';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';

interface PageInfo {
  total_entries: number;
  per_page: number;
  current_page: number;
}
@Component({
  selector: 'app-infographics',
  templateUrl: './infographics.component.html',
  styleUrls: [ './infographics.component.scss' ]
})
export class InfographicsComponent implements OnInit {
  // @ViewChild('categorySelector') categorySelector: MatSelect;
  customer: Customer;
  infographics: Infographic[];
  pageInfo: any;
  content: Content[] = [];
  selectedCategory = '';
  categories = [];
  currentPage = 1;
  selectedCategories: string[];
  // categorySelectionOutput: Observable<MatOptionSelectionChange>;
  constructor(
    private authService: AuthenticationService,
    private infoService: InfographicsService,
    private breadcrumService: BreadCrumService
  ) { }

  ngOnInit() {
    this.customer = this.authService.currentCustomerValue;
    this.loadTags();
    this.loadGallery(this.currentPage);
    this.breadcrumService.set_breadcrum();
  }
  loadTags() {
    this.infoService.tags().subscribe( resp => {
      this.categories = resp.tags[0].children;
    });
  }
  loadGallery(page) {
    this.infoService.gallery(this.selectedCategory, page).subscribe(resp => {
      this.infographics = resp.data;
      this.pageInfo = {total_entries: resp.total_entries, per_page: resp.per_page, current_page: resp.current_page};
      this.filterContent();
    });
  }

  filterContent() {
    this.content = this.infographics.map(i => this.contentFor(i));
  }

  contentFor(i: Infographic): Content {
    return new Content({
      id: i.id,
      url: i.url,
      status: 'ready',
      caption: i.caption,
      category: 'IF',
      style: '',
      thumbnail: i.thumbnail,
      media_type: i.media_type,
      contentable_id: this.customer.id,
      contentable_type: 'Customer'
    });
  }
  handleChange(checked: boolean, category: string): void {
    this.selectedCategory = checked ? category : '';
    this.loadGallery(this.currentPage);
  }
}
