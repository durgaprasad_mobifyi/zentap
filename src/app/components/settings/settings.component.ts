import {Component, OnInit, ViewChild, TemplateRef, Input} from '@angular/core';
import {NzNotificationService} from 'ng-zorro-antd';
import {Customer} from '../../vos/customer/customer';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {SettingsService} from '../../services/settings/settings.service';
import {CustomersService} from '../../services/customers/customers.service';
import {Setting} from '../../vos/setting/setting';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {ConnectLinkedinService} from '../../services/connect-linkedin/connect-linkedin.service';
import {SocialPostDialogComponent} from '../../shared/social-post-dialog/social-post-dialog.component';
import {NzModalService} from 'ng-zorro-antd/modal';
import {Router} from '@angular/router';
import {UpgradeDialogComponent} from '../../shared/upgrade-dialog/upgrade-dialog.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: [ './settings.component.scss' ]
})

export class SettingsComponent implements OnInit {
  @ViewChild('statusTmp') template: TemplateRef<any>;
  loading = false;
  settings: any;
  digest_settings: any;
  @Input()
   likedinConnectedSuccess;
  customer: Customer;
  integration;
  linkedinStatus = false;
  linkedinAutopost = false;
  integrationdId;
  paidTier = false;
  constructor(
    private subscriptionService: SettingsService,
    private authService: AuthenticationService,
    private customerService: CustomersService,
    private message: NzNotificationService,
    private breadcrumService: BreadCrumService,
    private linkedInService: ConnectLinkedinService,
    private modalService: NzModalService,
    private router: Router
  ) {}
  ngOnInit() {
    this.fetch_digest_subscriptions();
    this.fetch_subscriptions();
    this.fetch_content_posting_settings();
    this.fetch_linkedin_autopost_settings();
    this.fetch_tier();
    this.breadcrumService.set_breadcrum();
  }
  fetch_subscriptions() {
    this.subscriptionService.subscriptionsList().subscribe(setting => {
      this.settings = setting.subscriptions;
    });
  }
  fetch_digest_subscriptions() {
    this.subscriptionService.settingList().subscribe(setting => {
      this.digest_settings = setting.settings;
    });
  }
  fetch_content_posting_settings() {
    this.authService.currentCustomer.subscribe((c) => {
      if (c && c.settings['show_price'] === undefined) {
        c.settings['show_price'] = true;
      }
      this.customer = c;
    });
  }
  fetch_linkedin_autopost_settings() {
    this.authService.refresh().subscribe((c) => {
      const integration = c.integrations_attributes.filter((i) => i.provider === 'linkedin');
      if (integration.length > 0) {
        this.linkedinStatus = true;
        this.linkedInService.getAutopost().subscribe( int => {
          this.integration = int.data[0];
          this.linkedinAutopost = this.integration.auto_posting;
        });
      }
    });
  }
  subscription_title(title: string) {
    const s_title = title.split('.');
    const sub_title = s_title.slice(Math.max(s_title.length - 2, 1));
    return     sub_title.join(' ');
  }
  get_title(title: string) {
    const s_title = title.split('.');
    return     s_title[0];
  }
  toggleNotificationSettings(setting: Setting, type?: string) {
    let endpoint;
    let subscribe;
    if (type === 'email') {
      endpoint = setting.subscribing_to_email ? 'subscribe_to_email' : 'unsubscribe_to_email';
      subscribe = setting.subscribing_to_email;
      this.updateSubscriptions(setting.id, endpoint);
    }
    if (type === 'sms') {
      endpoint = setting.optional_targets.subscribing_to_sms ? 'subscribe_to_optional_target' : 'unsubscribe_to_optional_target';
      subscribe = setting.optional_targets.subscribing_to_sms;
      this.updateSubscriptions(setting.id, endpoint);
    }
    if (type === 'subscription') {
      endpoint = setting.subscribing ? 'subscribe' : 'unsubscribe';
      subscribe = setting.subscribing;
      this.updateSubscriptions(setting.id, endpoint);
      // childern subscriptions email/sms
      const index = this.settings.map(function(e) { return e.id; }).indexOf(setting.id);
      this.settings[index].subscribing_to_email = setting.subscribing;
      // email subscription
      const endpointemail = setting.subscribing ? 'subscribe_to_email' : 'unsubscribe_to_email';
      this.updateSubscriptions(setting.id, endpointemail);
      // sms subscription
      if (Object.keys(setting.optional_targets).length > 0) {
        this.settings[index].optional_targets.subscribing_to_sms = setting.subscribing;
        const endpointsms = setting.optional_targets.subscribing_to_sms ? 'subscribe_to_optional_target' : 'unsubscribe_to_optional_target';
        this.updateSubscriptions(setting.id, endpointsms);
      }
    }
    const message = subscribe ? 'Subscribed' : 'Unsubscribed';
    this.notify_message(message);
  }
  updateSubscriptions(setting_id, endpoint) {
    this.subscriptionService.update_subscription(setting_id, endpoint).subscribe(resp => {});
  }
  toggleLinkedInSetting(event?: any) {
    const message =  event ? 'Subscribed' : 'Unsubscribed';
    const params = {
      integration: {
        auto_posting: event
      }
    };
    this.linkedInService.setAutopost(params, this.integration).subscribe( integration => {
      this.notify_message(message);
    });
  }
  toggleSetting(setting?: string, event?: any) {
    const message =  event ? 'Subscribed' : 'Unsubscribed';
    this.customerService.update(this.customer).subscribe((c) => {
      this.customer = c;
      this.notify_message(message);
    });
  }
  toggleDigestSetting(setting: Setting) {
    const endpoint = setting.status ? 'subscribe' : 'unsubscribe';
    const message =  setting.status ? 'Subscribed' : 'Unsubscribed';
    this.subscriptionService.update_digest_subscription(setting.id, endpoint).subscribe(resp => {
      this.notify_message(message);
    });
  }
  notify_message(message?) {
    const notificationMSG = { message: message};
    this.message.template(
      this.template,
      {
        nzData: notificationMSG,
        nzPauseOnHover: true
      }
    );
  }
  connectLinkedin() {
    const modal = this.modalService.create({
      nzContent: SocialPostDialogComponent,
      nzComponentParams: {
        hideFacebook: false,
        hidePosting: true
      },
      nzFooter: null
    });
    modal.afterClose.subscribe(response => {
      this.fetch_linkedin_autopost_settings();
    });
  }
  upgradePlan() {
    const modal = this.modalService.create({
      nzContent: UpgradeDialogComponent,
      nzFooter: null
    });
  }
  fetch_tier() {
    this.authService.currentCustomer.subscribe(customer => {
      this.paidTier = customer.tier === 'ultra';
    });
  }
}
