import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products/product-api.service';
import { Tier } from 'src/app/vos/tier/tier';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: [ './plans.component.scss' ]
})
export class PlansComponent implements OnInit {
  tiers: Tier[] = [];
  selectedTier: string;
  constructor(private productsService: ProductsService) {
    this.productsService.tiers()
      .subscribe(tiers => this.tiers = tiers);
  }

  ngOnInit() {
  }
  selectTier(tier: Tier) {

    this.selectedTier = tier.id;

  }
}
