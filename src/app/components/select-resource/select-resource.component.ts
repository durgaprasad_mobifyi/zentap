import { Component, OnInit } from '@angular/core';
import { CardButtonConfig } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { ListingsService } from 'src/app/services/listings/listings.service';
import { Listing } from 'src/app/vos/listing/listing';
import { MarketReportsService } from 'src/app/services/market-reports/market-reports.service';
import { MarketReport } from 'src/app/vos/market-report/market-report';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {GlobalsService} from '../../services/globals/globals.service';

@Component({
  selector: 'app-reach-people',
  templateUrl: './select-resource.component.html',
  styleUrls: ['./select-resource.component.scss']
})
export class SelectResourceComponent implements OnInit {
  loading = true;
  // allListings: Listing[] = [];
  listings: Listing[] = [];
  marketreports: MarketReport[] = [];
  customer: Customer;
  productName: string;
  productTitle: string;
  productType: string;
  productTypeTitle: string;
  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private listingService: ListingsService,
    private breadcrumService: BreadCrumService,
    private marketreportService: MarketReportsService,
    private globalsService: GlobalsService
  ) { }
  pageSizeOptions = [10];
  listingPagination = {
    limit: 10,
    total: 10,
    page: 1,
    totalPages: 10
  };
  ConfigListingTitle;
  newCardButton: CardButtonConfig;
  newMarketBtnConfig: CardButtonConfig;
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.productName = params['name'];
      this.productType = params['type'];
      this.productTitle = this.globalsService.normalizeTitle(params['name']);
      this.productTypeTitle = this.globalsService.normalizeTitle(params['type']);
      if (this.productType === 'listing') {
        this.fetchContent();
      } else {
        this.fetchMarketReports();
      }
      this.newCardButton = this.globalsService.getNewCard(this.productType, this.productName);
      this.ConfigListingTitle = {
        title: this.productTitle,
        class: 'section-title',
        icon: 'home',
        colors: '#30d289',
      };
    });
    this.customer = this.authService.currentCustomerValue;
    // const customerId = this.customer.id;
    this.authService.currentCustomer.subscribe(c => (this.customer = c));
    this.breadcrumService.set_breadcrum();
    this.loading = false;
  }
  didPage(pageIndex) {
    this.listingPagination.page = pageIndex;
    this.loading = true;
    this.fetchContent();
  }

  configFor(listing: Listing): CardButtonConfig {
    return {
      id: listing.id,
      title: '',
      actionText: '',
      centerText: listing.address,
      createdAt: listing.created_at,
      link: `${this.productType}/${listing.id}/${this.productName}/new`,
      colors: {
        bg: '#30d289',
        textSelected: 'white'
      }
    };
  }

  configForMarket(marketReport: MarketReport): CardButtonConfig {
    return {
      id: marketReport.id,
      title: 'Select',
      actionText: marketReport.zips.join(', '),
      topText: marketReport.region_name,
      antIcon: 'area-chart',
      link: `${this.productType}/${marketReport.id}/${this.productName}/new`,
      createdAt: marketReport.created_at,
      colors: {
        bg: '#ffaf47',
        textSelected: 'white'
      }
    };
  }
  fetchContent() {
    this.loading = true;
    this.listingService.listingsList(this.productName, `?page=${this.listingPagination.page}`).subscribe( response => {
      this.loading = false;
      this.listingPagination.limit = response['per_page'];
      this.listingPagination.total = response['total_entries'];
      const limit = this.listingPagination.limit;
      const total_items = this.listingPagination.total;
      this.listingPagination.totalPages = Math.ceil(total_items / limit);
      this.listings = response.data;
    });
  }
  fetchMarketReports() {
    this.marketreportService.list().subscribe(response => {
      this.loading = false;
      this.marketreports = response.data;
    });
  }
}
