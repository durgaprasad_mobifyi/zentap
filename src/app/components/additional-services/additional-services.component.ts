import { ActivatedRoute } from '@angular/router';
import {Component, Input, OnInit} from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {ProductsService} from '../../services/products/product-api.service';

@Component({
  selector: 'app-additional-services',
  templateUrl: './additional-services.component.html',
  styleUrls: ['./additional-services.component.scss']
})

export class AdditionalServicesComponent implements OnInit {
  loading = true;
  additionalServices = [];
  isVisible = false;
  constructor(
    private route: ActivatedRoute,
    private breadcrumService: BreadCrumService,
    private modalService: NzModalService,
    private productsService: ProductsService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.breadcrumService.set_breadcrum();

    const params: Record<string, any> = {
      'q[additional_service_true]': 'true',
      'q[s][]': ['rank asc', 'available desc']
    };

    this.productsService.productList('products', params).subscribe(res => {
      this.additionalServices = res.data;
      this.additionalServices.map(service => {
        service.name = service.name.replace(/_/g, ' ');
        service.name = service.name.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
      });
      this.loading = false;
    });
  }
  openInfo() {
    this.isVisible = true;
  }
  closeInfo() {
    this.isVisible = false;
  }
}
