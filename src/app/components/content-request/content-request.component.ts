import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Content } from 'src/app/vos/content/content';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { ProductsService } from 'src/app/services/products/product-api.service';
import { Product, ProductStyle } from 'src/app/vos/product/product';
import { Observable, of } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ResourceService } from 'src/app/services/customer-resource.service';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { ListingsService } from 'src/app/services/listings/listings.service';
import { MarketReportsService } from 'src/app/services/market-reports/market-reports.service';
import { map, mergeMap } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Location } from '@angular/common';
import { Listing } from 'src/app/vos/listing/listing';
import { Customer } from 'src/app/vos/customer/customer';
import { WebsiteAttributes } from 'src/app/shared/interfaces/website-attributes';
import * as moment from 'moment';
import { ContentFormComponent } from '../../shared/content-form/content-form.component';
import { CustomerFormComponent } from '../../shared/customer-form/customer-form.component';
import { ListingFormComponent } from '../../shared/listing-form/listing-form.component';
import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';
import { ContentReceiptDialogComponent } from '../../shared/content-receipt-dialog/content-receipt-dialog.component';
import { GlobalsService } from 'src/app/services/globals/globals.service';

@Component({
  selector: 'app-content-request',
  templateUrl: './content-request.component.html',
  styleUrls: ['./content-request.component.scss']
})
export class ContentRequestComponent implements OnInit, AfterViewInit {

  form: string;
  type: string;
  product: Product;
  dateFormat = moment;
  @ViewChild('contentForm') contentForm: ContentFormComponent;
  @ViewChild('rForm') resourceForm:
    | CustomerFormComponent
    | ListingFormComponent;
  content = new Content();
  isNew = true;
  isSingleSnapShot = false;
  currentPage = 1;
  pagination = {
    limit: 10,
    total: 100,
    page: 1,
    totalPages: 10
  };
  contentSubject: Customer | Listing;
  clonedContentSubject: Customer | Listing;
  resourceService: ResourceService<Listing | Customer | any>;
  loading = false;
  error: string;
  contentCompleted = false;
  resourceCompleted = false;
  styles: ProductStyle[] = [];
  allReqFields = [];
  stepToMove;
  contents: Content[] = [];
  current = 0;
  customer: Customer;
  get showFields(): string[] {
    const fields = this.product && this.product.shownFields ? [...this.product.shownFields] : [];
    return fields;
  }
  get requiredFields(): string[] {
    return this.allReqFields;
  }
  updateRequiredFields($event) {
    this.allReqFields = $event;
  }
  constructor(
    private route: ActivatedRoute,
    private contentService: ContentsService,
    private productService: ProductsService,
    private authService: AuthenticationService,
    private customerService: CustomersService,
    private listingService: ListingsService,
    private marketReportsService: MarketReportsService,
    private message: NzMessageService,
    private router: Router,
    protected _location: Location,
    private breadcrumService: BreadCrumService,
    private cdRef: ChangeDetectorRef,
    private modalService: NzModalService,
    private globalService: GlobalsService
  ) { }

  ngAfterViewInit() {
    if (this.stepToMove === '2') {
      this.cdRef.detectChanges();
      setTimeout(() => {
        this.saveResource();
      }, 1000);
    }
  }

  ngOnInit() {
    const content: Content = this.route.snapshot.data['content'];
    if (content) {
      this.isNew = false;
      this.content = content;
    }
    const contentSubject: { object: Customer | Listing; type: string } = this
      .route.snapshot.data['data'];
    if (contentSubject) {
      this.contentSubject = contentSubject.object;
      this.type = contentSubject.type;
      this.content.contentable_id = contentSubject.object.id;
      this.content.contentable_type = contentSubject.type;
      this.resourceService =
        contentSubject.type === 'Customer'
          ? this.customerService
          : this.listingService;
      if (contentSubject.type === 'Content') {
        this.resourceService = this.marketReportsService;
        this.content.contentable_type = 'MarketReport';
      }
    } else {
      this.contentSubject = this.authService.currentCustomerValue;
      this.type = 'Customer';
      this.resourceService = this.customerService;
    }
    this.route.params.subscribe(params => {
      if (params['form']) {
        this.customer = this.authService.currentCustomerValue;
        const endpoint = `customers/${this.customer.id}/products`;
        const search_params = { 'q[name_eq]': params['form'] };
        this.productService.productList(endpoint, search_params).subscribe(p => {
          this.contentSubject.products = p.data;
          const products = this.contentSubject.products.map((product: Product) => new Product(product));
          this.product = products.find((prod) => prod.name === params['form']);
          this.isSingleSnapShot = this.product.name === 'single_data_snapshot';
          if (this.isSingleSnapShot) {
            this.content.extra_attributes = { data_points: [] };
          }
          this.allReqFields = this.product ? [...this.product.requiredFields] : [];
          this.cdRef.detectChanges();
          this.content.category = this.product.abbreviation;
          if (this.product.parent_type.includes('customer')) {
            this.content.contentable_type = 'Customer';
            this.content.contentable_id = this.authService.currentCustomerValue.id;
          }
          if (
            this.product.category === 'website' &&
            this.product.styles.length <= 1
          ) {
            this.save();
          }
          let product_name = '';
          if (product_name) {
            product_name = this.product.normalizedName;
          }
          this.breadcrumService.push_breadcrum({ name: (this.isNew ? 'New ' : 'Update ') + (product_name) });

        });
      }
    });
    this.clonedContentSubject = JSON.parse(JSON.stringify(this.contentSubject));
    this.route.queryParams
      .subscribe(params => {
        this.stepToMove = params['step'];
      });
  }
  cancel() {
    this._location.back();
  }
  pathFor(form: string): string {
    return this.form !== undefined ? '../' + form : form;
  }

  onChangeSnapType(snapData) {
    this.content.extra_attributes = { data_points: [snapData] };
  }

  save() {
    if (this.product.category === 'content') {
      this.saveContent();
    } else if (this.product.category === 'website') {
      this.saveWebsite().subscribe(w => {
        window.open(w.view_url, '_blank');
      });
    }
  }

  saveSnapShot() {
    this.delayedNext();
  }

  saveResource() {
    this.resourceForm.checkForm();
    if (
      this.resourceForm && !this.resourceForm.valid
    ) {
      this.message.create('error', 'You have invalid fields.');
      return;
    }
    this.loading = true;
    this.styles = [];
    if (this.isContentChanged) {
      this.resourceService
        .update(this.contentSubject)
        .subscribe(res => {
          if (this.content.contentable_type === 'Customer') {
            this.contentSubject['team_members_attributes'] =
              this.contentSubject['team_members_attributes'].filter(tm => tm._destroy !== 1);
            const Images = this.contentSubject['images_attributes'];
            if (Images[Images.length - 1].order) {
              const imgIndex = this.contentSubject['images_attributes'].findIndex(im => im.order === Images[Images.length - 1].order);
              if (imgIndex !== Images.length - 1) {
                this.contentSubject['images_attributes'] = this.contentSubject['images_attributes'].splice(imgIndex, 1);
              }
            }
          }
          this.fetchStyles();
        }, err => {
          this.loading = false;
          this.message.create('error', err.message || err);
        });
    } else {
      this.fetchStyles();
    }
  }

  fetchStyles() {
    this.productService.styles(this.product, this.contentSubject, this.currentPage)
      .subscribe(
        res => {
          if (res) {
            this.styles = this.styles.concat(res.data);
            const limit = this.pagination.limit = res.per_page;
            const total_items = this.pagination.total = res.total_entries;
            this.pagination.totalPages = Math.ceil(total_items / limit);
            if (this.contentSubject['team_members_attributes']) {
              const tm = this.contentSubject['team_members_attributes'].filter(
                member => member._destroy !== 1
              );
              this.styles = tm.length >= 3 ? this.styles.filter(styl => styl.name === 'Vanilla') : this.styles;
            }
            this.resourceCompleted = true;
            this.delayedNext();
            this.loading = false;
          }
        },
        err => {
          this.loading = false;
          this.error = err.message;
          this.message.create('error', err.message);
        }
      );
  }
  submit() {
    this.contentForm.checkForm();
    if (
      !this.contentForm.valid
    ) {
      this.message.create('error', 'You have invalid fields.');
      return;
    }
    this.loading = true;
    let contentRequest: Observable<Content>;
    if (this.isNew) {
      contentRequest = this.contentService.create(this.content);
    } else {
      contentRequest = this.contentService.update(this.content);
    }

    contentRequest.subscribe(
      res => {
        if (res) {
          this.contentCompleted = true;
          this.delayedNext();
          const route = this.product.path.split('/');
          const config = {
            type: this.product.name,
            link: this.product.name === 'banner_videos' ? 'branding/banner_videos/new' : `${route[0]}s/` + this.contentSubject.id
          };
          if (this.product.parent_type.includes('listing')) {
            config.link = `content_type/listings/${this.content.contentable_id}`;
            this.receipt(config, '/listings/' + this.content.contentable_id);
          } else {
            this.router.navigateByUrl('/');
            this.receipt(config, '/');
          }
        }
      },
      err => {
        this.loading = false;
        this.error = err.error['tier'][0] || err.message;
        this.message.create('error', this.error);
      });

  }
  gotoStep(stepNumber) {
    if (this.current > stepNumber) {
      this.current = stepNumber;
    }
  }
  delayedNext() {
    setTimeout(() => {
      this.current += 1;
    });
  }
  pre(): void {
    this.current -= 1;
  }
  saveContent() {
    this.contentForm.checkForm();
    this.resourceForm.checkForm();

    if (
      !this.contentForm.valid ||
      (this.resourceForm && !this.resourceForm.valid)
    ) {
      this.message.create('error', 'You have invalid fields.');
      return;
    }
    this.loading = true;
    let contentRequest: Observable<Content>;
    if (this.isNew) {
      contentRequest = this.contentService.create(this.content);
    } else {
      contentRequest = this.contentService.update(this.content);
    }
    this.resourceService
      .update(this.contentSubject)
      .pipe(mergeMap(data => contentRequest))
      .subscribe(
        res => {
          if (res) {
            if (this.product.parent_type.includes('listing')) {

              this.router.navigate([
                '/listings/' + this.content.contentable_id
              ]);
            } else {

              this.router.navigateByUrl('/branding');
            }
          }
          this.loading = false;
        },
        err => {
          this.loading = false;
          this.error = err.message;
        }
      );
  }

  saveWebsite(): Observable<WebsiteAttributes> {
    if (
      this.contentSubject.websites_attributes.length > 0 &&
      this.contentSubject.websites_attributes[0].id
    ) {
      return of(this.contentSubject.websites_attributes[0]);
    } else {
      if (this.contentSubject.websites_attributes.length === 0) {
        this.contentSubject.websites_attributes.push({
          template_id: 2
        });
      }
      return this.resourceService
        .update(this.contentSubject)
        .pipe(map(r => r.websites_attributes[0]));
    }
  }

  contentFor(i: ProductStyle): Content {
    return new Content({
      caption: '',
      url: i.preview,
      category: this.content.category,
      style: i.name,
      thumbnail: i.preview,
      media_type: 'image',
      contentable_id: this.contentSubject.id,
      contentable_type: 'Customer',
      is_template: true
    });
  }

  nextPage() {
    if (this.pagination.totalPages < this.currentPage) {
      this.currentPage++;
      this.fetchStyles();
    }
  }

  receipt(config, redirectUrl) {
    this.router.navigateByUrl(redirectUrl);
    const modal = this.modalService.create({
      nzContent: ContentReceiptDialogComponent,
      nzComponentParams: {
        config: config
      },
      nzFooter: null,
      nzWidth: '40%'
    });
  }

  get diagnostic() {
    return JSON.stringify(this.content);
  }

  get isContentChanged() {
    return (JSON.stringify(this.contentSubject) !== JSON.stringify(this.clonedContentSubject));
  }
  get imageLoadEnd () {
    return this.globalService.imageLoaded();
  }
}
