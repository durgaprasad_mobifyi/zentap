import { ActivatedRoute } from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {ConnectLinkedinService} from '../../services/connect-linkedin/connect-linkedin.service';
import {CustomersService} from '../../services/customers/customers.service';
import {Customer} from '../../vos/customer/customer';

@Component({
  selector: 'app-linkedin-auth',
  templateUrl: './linkedin-auth.component.html',
  styleUrls: ['./linkedin-auth.component.scss']
})

export class LinkedinAuthComponent implements OnInit {
  code: string;
  status = false;
  linkedInAuthentication = {};
  customer: Customer;
  loading = false;
  // linkedInConnected = false;
  // integration;
  constructor(
    private route: ActivatedRoute,
    private linkedInService: ConnectLinkedinService,
    private customersService: CustomersService,
    // private authService: AuthenticationService
  ) { }

  ngOnInit() {
    const params = this.route.snapshot.queryParams;

    if (params.error) {
      this.linkedInAuthentication = {status: 'error', message: params.error};
      this.status = false;
      this.loading = true;
      this.setLinkedinAuth();
    } else if (params.code) {
        this.linkUser(params.code);
    }
    setTimeout(function () { window.close(); }, 3000);
  }
  linkUser(code) {
    const payload = {
      code: code,
      integration: {
        provider: 'linkedin',
        status: 'pending'
      }
    };
    this.customersService.linkLN(payload).subscribe( response => {
      this.linkedInAuthentication = {status: 'success', message: 'Linkedin Integrated Successfully', code: code};
      this.status = true;
      this.setLinkedinAuth();
      this.loading = true;
    }, err => {
      if (err) {
        this.linkedInAuthentication = {status: 'error', message: 'Something went wrong with linkedin login'};
        this.status = false;
        this.loading = true;
        this.setLinkedinAuth();
      }
    });
  }
  setLinkedinAuth () {
    this.linkedInService.setAuthorizationCode(this.linkedInAuthentication);
  }
}
