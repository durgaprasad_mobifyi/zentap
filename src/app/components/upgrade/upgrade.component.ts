import { Component, OnInit } from '@angular/core';
import { Tier, Plan } from 'src/app/vos/tier/tier';
import { ProductsService } from 'src/app/services/products/product-api.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { SubscriptionService } from 'src/app/services/subscriptions/subscriptions.service';
import { Router } from '@angular/router';
import {StripeCoupon, Subscription} from 'src/app/vos/subscription/subscription';
import { Observable } from 'rxjs';
import {BillingPeriod} from '../../shared/tier-selector/tier-selector.component';
import {Customer} from '../../vos/customer/customer';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {GenericDialogComponent} from '../../shared/generic-dialog/generic-dialog.component';

@Component({
  selector: 'app-upgrade',
  templateUrl: './upgrade.component.html',
  styleUrls: ['./upgrade.component.scss']
})
export class UpgradeComponent implements OnInit {
  loading = false;
  tiers: Tier[] = [];
  selectedPlan: Plan;
  subscription$: Observable<Subscription>;
  currentPlan$: Observable<string>;
  // is_paid_active$: Observable<boolean>;
  paymentSource: string;
  billingPeriod: BillingPeriod = 'mo';
  customer: Customer;
  coupon: StripeCoupon;
  isYearly: Boolean = false;
  current = 0;
  contactLinks = [
    {
      label: 'Call Us',
      href: 'tel:1-888-343-0940',
      icon: 'fas phone',
      color: '#30d289'
    },
    {
      label: 'Email Us',
      href: 'mailto:support@universalpromote.com',
      icon: 'fas envelope',
      color: '#18a0b2'
    }
  ];
  // currentPlanId$: Observable<string>;
  constructor(private productsService: ProductsService,
    private authService: AuthenticationService,
    private subscriptionService: SubscriptionService,
    private router: Router,
    private breadcrumService: BreadCrumService,
    private modalService: NzModalService,
  ) {
    this.productsService.tiers()
      .subscribe(tiers => {
        this.tiers = tiers;
      });
    this.subscription$ = this.subscriptionService.currentSubscription();
    this.currentPlan$ = this.subscriptionService.currentPlan();
    this.authService.currentCustomer.subscribe(c => this.customer = c);
    // this.currentPlanId$ = this.subscriptionService
    //   .currentSubscription().pipe(map(s => s.plan.id));
    // this.is_paid_active$ = this.subscriptionService
    //   .currentSubscription().pipe(map(s => s.is_paid_active));
    // this.authService.currentCustomer.subscribe(c => this.currentTier = c.tier)
  }

  ngOnInit() {
    this.breadcrumService.set_breadcrum();

  }
  selectPlan(plan: Plan) {
    this.selectedPlan = plan;
    this.current += 1;
    // this.stepper.next();
  }
  toggleBillingPeriod() {
    this.billingPeriod = this.billingPeriod === 'mo' ? 'yr' : 'mo';
    if (this.billingPeriod === 'yr') {
      this.isYearly = true;
    } else {
      this.isYearly = false;
    }
  }

  lineItems(): { name: string, quantity: number, price: number }[] {
    const items = this.selectedPlan ? [
      {
        name: `${this.selectedPlan.tier.capitalizeAll()} Subscription`, quantity: 1,
        price: Number(this.selectedPlan.price)
      }
    ] : [];
    if (this.coupon && this.coupon.valid) {
      items.push({
        name: this.coupon.name,
        quantity: 1,
        price: Number(this.coupon.percent_off
          ? -(this.coupon.percent_off / 100 * this.selectedPlan.price)
          : -this.coupon.amount_off / 100
        )
      });
    }
    return items;
  }

  submit() {
    this.loading = true;
    // this.stepper.next();
    this.subscriptionService.updateSubscription(
      {
        plan: { id: this.selectedPlan.id },
        cancel_at_period_end: false,
        default_source: this.paymentSource,
        coupon: this.coupon ? this.coupon.id : null
      }
    )
      .subscribe(_ => this.loading = false);
  }

  home() {
    this.router.navigateByUrl('/');
  }


  cancelAccount() {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: 'Are you sure you want to cancel your subscription?',
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null
    });

    modal.afterClose.subscribe(v => {
      if (v) {
        this.subscription$ = this.subscriptionService.updateSubscription({ cancel_at_period_end: true });
      }
    });

  }

}
