import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import {AdsService} from '../../../services/ads/ads.service';
import {GenericDialogComponent} from '../../../shared/generic-dialog/generic-dialog.component';

@Component({
  selector: 'app-ads-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class AdsshowComponent implements OnInit {
  ad: any;
  loading = false;
  ads_title: string;
  ads_caption: string;
  ads_description: string;
  media_type: string;
  media_url: string;
  constructor(
    private adsService: AdsService,
    private activatedRoute: ActivatedRoute,
    private message: NzMessageService,
    private router: Router,
    private modalService: NzModalService,
  ) { }

  ngOnInit() {
    this.loading = true;
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.get_ad(params.id);
      }
      this.loading = false;
    });
  }
  get_ad(id) {
    this.adsService.show(id).subscribe( response => {
      this.ad = response;
      this.ads_title = this.ad.post_attributes.content_attributes.headline;
      this.ads_caption = this.ad.post_attributes.content_attributes.caption;
      this.ads_description = this.ad.post_attributes.content_attributes.description;
      this.media_type = this.ad.post_attributes.content_attributes.media_type;
      this.media_url = this.ad.post_attributes.content_attributes.url;
    });
  }
  deleteAd () {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: 'Are you sure you want to delete this Ad?',
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.loading = true;
        this.adsService.destroy(this.ad).subscribe(r => {
          this.router.navigateByUrl('/ads');
        }, e => {
          this.loading = false;
          this.message.create('error',  'Error deleting this ad. Please try again');
        });
      }
    });
  }
}
