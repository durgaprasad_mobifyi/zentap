import {Component, NgZone, OnInit, ElementRef, ViewChild, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MapsAPILoader, MouseEvent} from '@agm/core';;
import {AuthResponse, FacebookService, LoginResponse} from 'ngx-facebook';
import {FacebookStepsComponent} from '../../facebook-steps/facebook-steps.component';
import { NzMessageService } from 'ng-zorro-antd/message';
import {NzModalService} from 'ng-zorro-antd';
import {Customer} from '../../../vos/customer/customer';
import {Ad} from '../../../vos/ads/ads';
import {Content} from '../../../vos/content/content';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {AdsService} from '../../../services/ads/ads.service';
import {CustomersService} from '../../../services/customers/customers.service';


interface AdsCategory {
  id: number;
  value: string;
  parent: number;
  children: [];
}
@Component({
  selector: 'app-ads-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class AdsNewComponent implements OnInit {
  customer: Customer;
  current = 0;
  loading = false;
  preview = false;
  goalList;
  goalDetails;
  contents: Ad[] = [];
  filteredContent: Ad[] = [];
  isDisabled = true;
  goal: string;
  goalType: string;
  ad: Content;
  adsLocations: Array<any> = [];
  adsCities: Array<any> = [];
  adsLocationsMarker: Array<any> = [];
  adsLocationDisable = false;
  title = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;
  city;
  zip;
  isSelected = false;
  selectedGoal: any;
  selectedGoalType: any;
  selectedAd: number;
  isSelectedAd: number;
  isAdsButtonDisabled = true;
  isLocationButtonDisabled = true;
  adsmapLocation: string;
  // pagination
  page = 1;
  contentLength = 0;
  isFbClicked = false;
  adsDetected = false;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private adsService: AdsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private render: Renderer2,
    private fbService: FacebookService,
    private customerService: CustomersService,
    private message: NzMessageService,
    private modalService: NzModalService
  ) { }

  ngOnInit() {
    this.adsDetected = this.adsService.detect();
    // this.goalList = this.adsService.getAdsGoals();
    this.loadTags();
    // this.getAds();
  }
  handleCancel(): void {
    this.adsDetected = false;
    location.reload();
  }
  loadTags() {
    this.adsService.getAdsTags().subscribe( resp => {
      this.goalList = resp[0].children;
    });
  }
  pre(): void {
    this.current -= 1;
    this.isDisabled = true;
    if (this.current === 2) {
      this.loadMapAutoComplete();
    }
  }

  next(): void {
    this.current += 1;
    if (this.current === 1) {
      this.getAds();
    }
    if (this.current === 2) {
      this.loadMapAutoComplete();
    }
    // this.isSelected = false;
  }

  back() {
    this.router.navigate([
      '/'
    ]);
  }
  isActive (goal) {
    return this.selectedGoal === goal;
  }
  isActiveType (goalType) {
    return this.selectedGoalType === goalType;
  }
  setGoal(adsgoal) {
    this.goal = adsgoal.name;
    this.goalDetails = adsgoal;
    this.selectedGoal = adsgoal.name;
    // this.selectedGoalType = adsgoal.childern[0];
    this.isSelected = true;
    this.isAdsButtonDisabled = false;
  }
  // setGoalType(adsgoaltype) {
  //   this.goalType = adsgoaltype;
  //   this.selectedGoalType = adsgoaltype;
  //   this.isSelected = true;
  // }
  setAd(ad) {
    this.ad = ad;
    this.selectedAd = ad.id;
  }
  isActiveAd(adId) {
    // this.isSelectedAd =  this.selectedAd === adId;
    this.isSelectedAd =  adId ;
    this.isAdsButtonDisabled = true ;
  }
  getAds() {
    this.loading = true;
    this.adsService.adsList(this.selectedGoal, this.page).subscribe(ads => {
        if (ads.length > 0) {
          this.contents = ads;
          this.contentLength = ads.length;
        }
      },
      error => { },
      () => {
        this.loading = false;
      }
    );
  }
  // setRange(range: []) {
  //   this.adsAgeRange = range;
  // }
  // Get Current Location Coordinates
  private setDefaultLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }
  loadMapAutoComplete () {
    this.mapsAPILoader.load().then(() => {
      if (this.adsLocations.length === 0) {
        this.setDefaultLocation();
      }
      this.geoCoder = new google.maps.Geocoder;

      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['geocode']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          // set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.adsLocationsMarker.push([this.latitude, this.longitude]);
          this.searchElementRef.nativeElement.value = '';
          this.getAddress();
          // this.adsLocations.push([this.latitude, this.longitude]);
          this.zoom = 15;
        });
      });
    });
  }
  disableLocationSearch() {
    this.adsLocationDisable = this.adsLocations.length >= 3;
    this.isLocationButtonDisabled = this.adsLocations.length === 0;
  }
  launchAd() {
    this.loading = true;
    this.checkFBAccess();
  }
  submitAd() {
    this.adsService.launchAd(this.get_campaign_type(), this.ad, this.adsLocations).subscribe(success => {
      this.router.navigate(['/ads']);
    }, err => {
      if (err.status === 500) {
        this.message.create('error', err.message);
      } else {
        this.message.create('error', err.error.message['campaign.tier'][0]);
      }
      this.loading = false;
    });
  }
  checkFBAccess() {
    this.customerService.checkFBAccess().subscribe(c => {
      // this.adsService.launchAd(this.get_campaign_type(), this.ad, this.ageRange, this.adsLocations).subscribe(success => {
      this.submitAd();
    }, err => {
      this.loading = false;
      this.selectPage();
    });
  }
  openFacebookSteps() {
    const modal = this.modalService.create({
      // nzTitle: 'Modal Title',
      nzContent: FacebookStepsComponent,
      nzFooter: null
    });
  }
  linkFB(): void {
    this.fbService.login({
      scope: 'public_profile,user_friends,email,pages_show_list',
      return_scopes: true,
      enable_profile_selector: true
    })
      .then((response: LoginResponse) => {
          this.isFbClicked = false;
          this.connectFB(response.authResponse);
        }
      )
      .catch((error: any) => {
        console.error(error);
      });
  }
  connectFB(response: AuthResponse) {
    this.customerService.linkFB(response.accessToken).subscribe(customer => {
        this.customer = customer;
        this.message.create('success',  'Connected');
        this.selectPage();
      },
      (err: any) => {
        console.error(err);
      });
  }
  selectPage() {
    if (!this.isFbClicked) {
      this.isFbClicked = true;
      this.fbService.getLoginStatus()
        .then((status) => {
          if (status.authResponse && status.status === 'connected') {
            this.fbService.api(`/v3.2/me/accounts?fields=name,id,picture`)
              .then(response => {
                // this.isFbClicked = false;
                const pages = response.data.map(p => ({ label: p.name, value: p.id, image: p.picture.data.url }));
                if (pages.length) {
                  this.openFacebookSteps();
                } else {
                  this.linkFB();
                }
              });
          } else {
            this.linkFB();
          }
        });
    }
  }
  didPage(page: number) {
    this.page = page;
    this.getAds();
  }
  get_campaign_type() {
    let campaign_type = '';
    switch (this.goal) {
      case 'sellers':
        campaign_type = 'lead_seller';
        break;
      case 'buyers':
        campaign_type = 'lead_buyer';
        break;
      default:
        campaign_type = this.goal;
        break;
    }
    return campaign_type;
  }
  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress();
      });
    }
  }

  mapClicked($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.adsLocationsMarker.push([$event.coords.lat, $event.coords.lng]);
    this.getAddress();
  }
  markerDragEnd($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.adsLocationsMarker.push([$event.coords.lat, $event.coords.lng]);
    this.getAddress();
  }

  getAddress() {
    this.geoCoder.geocode({ 'location': { lat: this.latitude, lng: this.longitude } }, (results, status) => {
      if (status === 'OK') {
        this.get_city(results);
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
  get_postal_code(results) {
    let postal_code ;
    if (results[1]) {
      // find postal code
      for (let i = 0; i < results[0].address_components.length; i++) {
        for (let b = 0; b < results[0].address_components[i].types.length; b++) {
          // there are different types that might hold a city admin_area_lvl_1
          if (results[0].address_components[i].types[b] === 'postal_code') {
            // this is the object you are looking for
            postal_code = results[0].address_components[i];
            break;
          }
        }
      }
      return this.zip = postal_code ? postal_code.short_name : '';
    } else {
      window.alert('No results found');
    }

  }
  get_city(results) {
    let i_neigh ;
    if (results[1]) {
      // find city name
      for (let i = 0; i < results[0].address_components.length; i++) {
        for (let b = 0; b < results[0].address_components[i].types.length; b++) {
          if (results[0].address_components[i].types[b] === 'neighborhood') {
            i_neigh = results[0].address_components[i];
            break;
          } else if (results[0].address_components[i].types[b] === 'locality') {
            this.city = results[0].address_components[i];
            break;
          }
        }
      }
      if (i_neigh) {
        this.city = i_neigh;
      }
      this.adsCities.push(this.city.long_name);
      // this.adsLocations.push({zip_code: this.get_postal_code(results), latitude: this.latitude, longitude: this.longitude});
      this.adsLocations.push({latitude: this.latitude, longitude: this.longitude});
      this.disableLocationSearch();
    } else {
      window.alert('No results found');
    }
  }
  onClose(city: String) {
    const cindex = this.adsCities.indexOf(city);
    this.adsLocationsMarker.splice(cindex, 1);
    this.adsLocations.splice(cindex, 1);
    this.adsCities.splice(cindex, 1);
    this.disableLocationSearch();
  }
}
