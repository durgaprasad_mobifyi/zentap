import { Component, OnInit, ViewChild } from '@angular/core';
import { CardButtonConfig } from '../../models';
import { Product } from 'src/app/vos/product/product';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { MatPaginator } from '@angular/material';
import { AdsService } from 'src/app/services/ads/ads.service';
import { Ad } from 'src/app/vos/ads/ads';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';

export interface PeriodicElement {
  name: string;
  phone: string;
  email: string;
  action: string;
}

@Component({
  selector: 'app-leads',
  templateUrl: './ads.component.html',
  styleUrls: [ './ads.component.scss' ]
})
export class AdsComponent implements OnInit {
  loading = true;
  stylesExpanded = false;
  products: CardButtonConfig[];
  ads: Ad[] = [];
  // ads: [];
  customer: Customer;
  marketingProducts: Product[] = [];
  page = 1;
  pageSize = 5;
  // pageSizeOptions = [ 10, 12, 20, 50, 100 ];
  contentLength = 0;
  adsDetected = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  newCardButton: CardButtonConfig = {
    id: 'new',
    title: 'CREATE',
    actionText: 'NEW AD',
    icon: 'fas plus',
    link: 'ads/new',
    colors: {
      bg: '#f4faff',
      text: '#326771',
      textSelected: '#f4faff'
    }
  };
  viewCardButton: CardButtonConfig = {
    id: 'view',
    title: 'VIEW',
    actionText: 'CURRENT ADS',
    icon: 'fas eye',
    link: 'ads/list',
    colors: {
      bg: '#30d289',
      text: '#f4faff',
      textSelected: '#30d289'
    }
  };
  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private adsService: AdsService,
    private breadcrumService: BreadCrumService
  ) { }

  ngOnInit() {
    this.adsDetected = this.adsService.detect();
    this.customer = this.authService.currentCustomerValue;
    const customerId = this.customer.id;
    this.authService.currentCustomer.subscribe(c => (this.customer = c));
    this.getAds();
    this.breadcrumService.set_breadcrum();
  }
  handleCancel(): void {
    this.adsDetected = false;
    location.reload();
  }
  getAds() {
    this.loading = true;
    this.adsService.getAdsList(this.page).subscribe(response => {
      this.ads = response.data.map( ads => {
        return ads;
      });
        this.contentLength = response.total_entries;
        this.pageSize = response.per_page;
        this.loading = false;
      },
      error => { },
      () => {
         this.loading = false;
      }
    );
  }
  get_campaign_title(ad: Ad) {
    return ad.post_attributes.content_attributes.caption;
  }
  get_campaign_thumbnail(ad: Ad) {
    let thumbnail = 'https://imgplaceholder.com/160x90?text=Advertisement';
    if (ad.post_attributes.content_attributes.thumbnail) {
      thumbnail = ad.post_attributes.content_attributes.thumbnail;
    }
    return thumbnail;
  }
  didPage(page: number) {

    this.loading = true;
    this.page = page;
    this.getAds();
    // this.pageSize = pageEvent.pageSize;
    // this.pagechange.emit(this.page);
    // this.updateFilter();
  }
}
