import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { trigger, transition, style, animate } from '@angular/animations';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { OnboardingService } from 'src/app/services/onboarding/onboarding.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  animations: [
    trigger('flyInOut', [
      // state('void', style({ opacity: 0 })),
      transition(':enter', [
        style({ opacity: 0 }),
        animate('0.5s')
      ]),
      transition(':leave',
        animate('0.5s', style({ opacity: 0 })))
    ])
  ]
})
/**
 * Blank view.
 */
export class ResetPasswordComponent implements OnInit {
  passForm: FormGroup;
  resetForm: FormGroup;
  state = 'set_pass';
  code: string;
  errorMessage: string;
  emailValid = true;
  emailExists = false;
  email: string;
  loading = false;
  showSignupLink = false;
  hide = false;
  resendWait = 0;
  passwordVisible = false;

  /**
   * Component constructor and DI injection point.
   */
  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private onboardingService: OnboardingService
  ) {
  }
  PasswordMatch: ValidatorFn = (fg: FormGroup) => {
    const valid = fg.controls['password'].value === fg.controls['password_confirmation'].value;
    return valid
      ? null
      : { mismatch: true };
  }
  EmailExists: ValidatorFn = (fg: FormGroup) => {
    const valid = this.emailValid;
    return valid
      ? null
      : { missing: true };
  }
  /**
 * Called part of the component lifecycle. Best first
 * place to start adding your code.
 */
  ngOnInit() {
    const d = { ...this.route.snapshot.data, ...this.route.snapshot.params, ...this.route.snapshot.queryParams };
    this.emailExists = false;
    this.setFormsWith(d);
    if (d.user_logout) {
      this.authService.logout();
      this.router.navigateByUrl('/login');
    } else {
      this.code = d.code;
      this.email = d.email;
      this.resetForm.controls.email.setValue(d.email || '');
      this.setState(d.state ? d.state : 'login');
    }
  }

  setFormsWith({ state: _, ...obj }: any = {}) {
    this.passForm = this.fb.group({
      ...obj,
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      password_confirmation: new FormControl('', [
        Validators.required
      ])
    },
      { validator: this.PasswordMatch });
    this.resetForm = this.fb.group({
      ...obj,
      email: new FormControl('', [
        Validators.required
      ])
    },
      {
        validator: this.EmailExists
      });
  }

  setState(s: string) {
    this.state = s;
    window.history.pushState({}, '', `/${s}`);
  }

  changePass() {
    const req = this.passForm.value;
    req['reset_password_token'] = this.code;
    this.loading = true;
    this.authService.resetPassword(req).subscribe(customer => {
      this.loading = false;
      if (customer) {
        this.router.navigateByUrl('/');
      }
    }, err => {
      if (err.status === 422) {
        this.errorMessage = 'Token is timed out';
      }
    });
  }

  showForgotPass() {

    this.resetForm.controls.email.setValue(this.email);
    this.setState('forgot_pass');
    this.errorMessage = '';
    // this.router.navigate([ '/forgot_password' ],
    //   {
    //     queryParams: { email: this.authForm.controls.email.value },
    //     queryParamsHandling: 'merge'
    //   });
  }

  forgotPass() {
    this.email = this.resetForm.controls.email.value;
    this.loading = true;
    this.authService.requestResetPassword(this.email).subscribe(success => {
      this.emailValid = success;
      this.loading = false;
      if (success) {
        this.state = 'reset_pass';
        this.resendWait = 30;
        const timer = setInterval(() => {
          this.resendWait -= 1;
          if (this.resendWait === 0) {
            clearInterval(timer);
          }
        }, 1000);
      }
    }, err => {
      this.loading = false;
      this.errorMessage = 'No account found for this email, Please enter valid email and try again.';
      });
  }

  // checkOnboardingSteps(cId?) {
  //   this.onboardingService.loadOnboardingSteps(cId).subscribe(steps => {
  //     if (this.onboardingService.hasIncompleteStep()) {
  //       this.router.navigateByUrl('/onboarding');
  //       this.hide = false;
  //     } else {
  //       this.router.navigateByUrl('/');
  //       this.hide = false;
  //     }
  //   });
  // }
}
