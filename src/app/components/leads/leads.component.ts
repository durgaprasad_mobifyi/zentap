import { Component, OnInit } from '@angular/core';
import { CardButtonConfig } from '../../models';
import { Product } from 'src/app/vos/product/product';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { LeadsService } from 'src/app/services/leads/leads.service';
import { AdsService } from 'src/app/services/ads/ads.service';
import { Ad } from 'src/app/vos/ads/ads';
import { DataResponse } from 'src/app/models/data-response/data-response';
import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';

export interface PeriodicElement {
  name: string;
  phone: string;
  email: string;
  action: string;
}



@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})

export class LeadsComponent implements OnInit {
  loading = true;
  stylesExpanded = false;
  products: CardButtonConfig[];
  contents: Ad[] = [];
  customer: Customer;
  marketingProducts: Product[] = [];
  page = 1;
  pageInfo =  { total_entries: 0, per_page: 10, current_page: 1 };
  displayedColumns: string[] = ['name', 'phone', 'email', 'action', 'transaction_in', 'address'];
  leadsDataSource = []; // new MatTableDataSource<PeriodicElement>();
  paginatedLeads = [];


  // @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private leadService: LeadsService,
    private adsService: AdsService,
    private breadcrumService: BreadCrumService
  ) { }

  ngOnInit() {
    this.customer = this.authService.currentCustomerValue;
    const customerId = this.customer.id;
    this.authService.currentCustomer.subscribe(c => (this.customer = c));

    this.getLeads();
    this.breadcrumService.set_breadcrum();
  }
  exportLeads() {

    this.leadService.exportLeads().subscribe(
      res => {
        const options = { type: `text/csv;charset=utf-8;` };
        this.downloadCsv(res, options, 'contacts.csv');
      }
    );
  }
  downloadCsv(body, options, filename) {
    const blob = new Blob([body], options);
    if (navigator.msSaveBlob) {
      // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      const link = document.createElement('a');
      // Browsers that support HTML5 download attribute
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
  getLeads() {
    this.loading = true;
    const customerId = this.customer.id;
    const page = `?page=${this.page}`;
    this.leadService.list(page).subscribe((res: any) => {
      if (res.data.length > 0) {
        this.leadsDataSource = res.data;
        this.pageInfo = { total_entries: res.total_entries, per_page: res.per_page, current_page: res.current_page };
        // this.leadsDataSource.paginator = this.paginator;
      }
      this.loading = false;
    });
  }

  didPage(page: number) {
    this.page = page;
    this.getLeads();
  }

  getAds() {
    this.loading = true;
    const customerId = this.customer.id;
    const visibleStatus = ['active', 'paused'];
    this.adsService.list().subscribe(
      (res: DataResponse<Ad[]>) => {
        if (res.data.length > 0) {
          this.contents = res.data.filter(
            a => visibleStatus.includes(a.status.toLowerCase())
          );
        }
      },
      error => { },
      () => {
        this.loading = false;
      }
    );
  }
}
