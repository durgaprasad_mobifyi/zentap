import { Component, OnInit } from '@angular/core';
import { CardButtonConfig } from '../../models';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Content } from 'src/app/vos/content/content';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';
import { ContentsService } from '../../services/contents/contents.service';
import { DownloaderService } from 'src/app/services/downloader.service';
import { GlobalsService } from '../../services/globals/globals.service';
import { ContentPreviewDialogComponent } from '../../shared/content-preview-dialog/content-preview-dialog.component';
import { ProductsService } from '../../services/products/product-api.service';
import { SocialPostDialogComponent } from '../../shared/social-post-dialog/social-post-dialog.component';
import {GenericDialogComponent} from '../../shared/generic-dialog/generic-dialog.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  selectedIndex = 0;
  tabs = [];
  loading = true;
  products: CardButtonConfig[];
  contents: Content[];
  customer: Customer;
  pageSizeOptions = [10];
  searchText: string;
  contentPagination = {
    limit: 10,
    total: 100,
    page: 1,
    totalPages: 10
  };
  public searchModelChanged: Subject<string> = new Subject<string>();
  public searchModelChangeSubscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private breadcrumService: BreadCrumService,
    private contentService: ContentsService,
    private productService: ProductsService,
    private globalsService: GlobalsService,
    private downloader: DownloaderService,
    private modalService: NzModalService,
    private message: NzMessageService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.tabs = this.globalsService.getContentTabs();
    this.customer = this.authService.currentCustomerValue;
    this.breadcrumService.set_breadcrum();
    this.fetchContent();
    this.searchModelChangeSubscription = this.searchModelChanged
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(newText => {
        this.searchText = newText;
        if (newText) {
          this.fetchContent(`&q[contentable_of_${this.tabs[this.selectedIndex].type}_type_address_cont]=${newText}`);
        } else {
          this.fetchContent();
        }
      });
  }

  contentDeleted(content: Content) {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: `Are you sure you want to delete content?`,
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.loading = true;
        this.contentService.destroy(content)
          .subscribe(res => {
            this.contents = this.contents.filter(c => c.id !== content.id);
            this.message.create('success', `Content has been successfully deleted.`);
            this.fetchContent();
          }, e => {
            this.loading = false;
            this.message.create('error', 'Error deleting the content. Please try again');
          });
      }
    });
  }

  didPage(pageIndex) {
    this.contentPagination.page = pageIndex;
    this.fetchContent();
  }

  fetchContent(searchQuery?) {
    this.loading = true;
    const contentType = this.tabs[this.selectedIndex].type || 'Listing';
    let queryParams = `?page=${this.contentPagination.page}&q[contentable_type_eq]=${contentType}`;
    if (searchQuery) {
      queryParams = queryParams + searchQuery;
    }
    this.contentService.list(queryParams)
      .pipe(
        map((res) => {
          this.contentPagination.limit = res['per_page'];
          this.contentPagination.total = res['total_entries'];
          const limit = this.contentPagination.limit;
          const total_items = this.contentPagination.total;
          this.contentPagination.totalPages = Math.ceil(total_items / limit);
          return res.data;
        })
      ).subscribe(contents => {
        this.contents = contents;
        this.loading = false;
      });
  }

  log(args: any[]): void {
    this.searchText = '';
    this.fetchContent();
  }

  download(content) {
    let url = content.url;
    if (!url.includes('https')) {
      url = url.replace('http', 'https');
    }
    this.downloader.save(url, content.filename);
  }
  post(content) {
    if (content.status === 'ready') {
      const modal = this.modalService.create({
        nzContent: SocialPostDialogComponent,
        nzComponentParams: {
          content
        },
        nzFooter: null
      });
      // modal.afterClose.subscribe(response => {
      // });
    }
  }

  sort(sort: { key: string; value: string }) {
    const sortName = sort.key || 'descend';
    this.contents.sort((a, b) =>
      sort.value === 'ascend'
        ? a[sortName!] > b[sortName!]
          ? 1
          : -1
        : b[sortName!] > a[sortName!]
          ? 1
          : -1
    );
  }
  showPreview(content) {
    const modal = this.modalService.create({
      nzContent: ContentPreviewDialogComponent,
      nzComponentParams: {
        config: {
          content
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
    });
  }

  gotToEdit(content) {
    this.router.navigate([`listings/${content.contentable_id}/edit`]);
  }
}
