import { Component, OnInit } from '@angular/core';
import {Template} from '../../vos/template/template';
import {Listing} from '../../vos/listing/listing';
import {MarketReport} from '../../vos/market-report/market-report';
import {CardButtonConfig} from '../../models/interfaces';
import {ListingsService} from '../../services/listings/listings.service';
import {MarketReportsService} from '../../services/market-reports/market-reports.service';
import {TemplatesService} from '../../services/templates/templates.service';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';

@Component({
  selector: 'app-blast-email',
  templateUrl: './blast-email.component.html',
  styleUrls: ['./blast-email.component.scss']
})
export class BlastEmailComponent implements OnInit {
  s: number[] = [];
  current = 0;
  isAllSelected = false;
  template: Template;
  templates: Template[] = [];
  loading = false;
  preview = false;
  resourceId: number;
  resourceType: string;
  selectedTemplate: Template;
  customerTemplate: Template;
  selectedTempalateType: string;
  selectedListing: { id: any };
  listings: Listing[] = [];
  selectedMarketReport: { id: any };
  marketReports: MarketReport[] = [];
  isDisabled = true;
  newListingCardButton: CardButtonConfig = {
    id: 'new',
    title: 'CREATE',
    actionText: 'NEW LISTING',
    icon: 'fas plus',
    link: `listings/new`,
    colors: {
      bg: '#f4faff',
      text: '#326771',
      textSelected: '#f4faff'
    },
  };
  newMarketBtnConfig: CardButtonConfig = {
    id: 'new',
    title: 'CREATE',
    actionText: 'NEW MARKET REPORT',
    icon: 'fas plus',
    link: 'market_reports/new',
    colors: {
      bg: '#f4faff',
      text: '#326771',
      textSelected: '#f4faff'
    }
  };
  marketingButton: CardButtonConfig = {
    id: 'new',
    title: 'Market Report',
    actionText: 'Email Campaign',
    icon: 'fas envelope',
    link: '',
    colors: {
      bg: '#30d289',
      text: '#ffffff',
      textSelected: '#f4faff'
    }
  };
  listingButton: CardButtonConfig = {
    id: 'new',
    title: 'Listing',
    actionText: 'Email Campaign',
    icon: 'fas envelope-open-text',
    link: '',
    colors: {
      bg: '#30d289',
      text: '#ffffff',
      textSelected: '#f4faff'
    }
  };
  get previewURL(): string {
    if (this.selectedTemplate) {
      return this.templateService.previewURLFor(this.selectedTemplate, this.getSelectedResource().id);
    } else {
      return '';
    }
  }
  get isResourceSelected(): boolean {
    if (this.getSelectedResource()) { return true; } else { return false; }
  }
  previewURLForCustomerVersion() {
    if (this.customerTemplate) {
      return this.templateService.previewURLFor(this.customerTemplate, this.getSelectedResource().id);
    } else {
      return '';
    }
  }
  getSelectedResource() {
    return this.selectedTempalateType === 'listing' ? this.selectedListing : this.selectedMarketReport;
  }
  constructor(
    private listingService: ListingsService,
    private marketReportService: MarketReportsService,
    private templateService: TemplatesService,
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private breadcrumService: BreadCrumService
  ) { }

  ngOnInit() {
    this.resourceId = parseInt(this.route.snapshot.paramMap.get('resourceId'), 10);
    this.resourceType = this.route.snapshot.paramMap.get('resourceType');

    if (this.resourceType && this.resourceType.length > 0 && this.resourceId) {
      this.selectedTempalateType = this.resourceType;
      if (this.resourceType === 'listings' || this.resourceType === 'listing') {
        this.selectedListing = { id: this.resourceId };
        this.setTemplateType('listing');
      } else if (this.resourceType === 'market_reports') {
        this.selectedMarketReport = { id: this.resourceId };
        this.setTemplateType('market_report');
      }
      this.loadTemplates(this.selectedTempalateType);
      this.current = 2;
    }

    this.listingService.list().subscribe(response => {
      this.loading = false;
      this.listings = response.data;
    });
    this.marketReportService.list().subscribe(response => {
      this.loading = false;
      this.marketReports = response.data;
    });
    this.breadcrumService.set_breadcrum();
  }
  selChange(event) {
    console.log(event);
  }
  pre(): void {
    this.current -= 1;
    this.isDisabled = true;
  }

  next(): void {
    this.current += 1;
  }

  send(): void {
    if (this.s.length > 0 || this.isAllSelected) {
      this.loading = true;
      this.next();
      this.templateService.blast(this.customerTemplate.id, this.s, this.getSelectedResource().id, this.isAllSelected).subscribe(success => {
        this.loading = false;
      });
    }
  }
  SelectContactsAndsend(): void {
    this.loading = true;
    this.customerTemplate = this.selectedTemplate;
    this.next();
    this.loading = false;
  }

  setTemplateType(type) {
    this.loading = true;
    this.selectedTempalateType = type;
    this.loadTemplates(this.selectedTempalateType);
    this.loading = false;
    this.next();
  }
  loadTemplates(selectedTempalateType) {
    const templateEnum = selectedTempalateType === 'listing' ? 0 : 1;
    this.templateService.allTemplates(templateEnum, 'email').subscribe(t => {
      this.templates = t.data;
      this.selectedTemplate = this.templates[0];
    });
  }
  setSelectedListing(listing) {
    this.selectedListing = listing;
    this.next();
  }

  setSelectedMarketReport(marketReport) {
    this.selectedMarketReport = marketReport;
    this.next();
  }
  back() {
    this.router.navigate([
      '/'
    ]);
  }
  configFor(listing: Listing): CardButtonConfig {
    return {
      id: listing.id,
      title: '',
      actionText: listing.listing_status,
      centerText: listing.address,
      link: '',
      colors: {
        bg: '#ffaf47',
        textSelected: 'white'
      }
    };
  }
  configForMarketReport(marketReport: MarketReport): CardButtonConfig {
    return {
      id: marketReport.id,
      title: '',
      actionText: '',
      centerText: marketReport.region_name,
      link: '',
      colors: {
        bg: '#ffaf47',
        textSelected: 'white'
      }
    };
  }

  changePropertyType(event) {
    this.loading = true;
    this.isDisabled = true;
    this.selectedTemplate = event;
    this.loading = false;
  }
  createCustomerTemplate() {
    this.loading = true;
    if (this.selectedTemplate) {
      this.selectedTemplate.customer_id = this.authService.currentCustomerValue.id;
      this.templateService.create(this.selectedTemplate).subscribe(template => {
        this.customerTemplate = template;
        this.loading = false;
      });
    }
    this.next();
  }
  onChangePreview(val) {
    if (val.checked === true) {
      this.preview = true;
    } else {
      this.preview = false;
    }
  }

  updateSelected(selected) {
    this.s = selected;
  }
  updateAllSelected(allSelected) {
    this.isAllSelected = allSelected;
  }
}
