import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { CustomersService } from 'src/app/services/customers/customers.service';

import { Customer } from 'src/app/vos/customer/customer';
import { GlobalsService } from 'src/app/services/globals/globals.service';

@Component({
  selector: 'app-registration-form.',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})

export class RegistrationFormComponent implements OnInit {
  tabs = [
    { name: 'Personal Details', icon: 'user' },
    { name: 'Branding', icon: 'highlight' }
  ];
  submitted = false;
  titles = Array<string>();
  credentials = Array<string>();
  currentTab = 0;
  model: any;
  colors = Array<string>();
  brandColor = {
    text_color: '#222222',
    bg_color: '#FFFFFF'
  };
  password;
  confirmPassword;
  constructor(
    private authService: AuthenticationService,
    private globalsService: GlobalsService,
    private customerService: CustomersService,
    private router: Router
  ) { }
  ngOnInit() {
    this.credentials = this.globalsService.getCredentials();
    this.titles = this.globalsService.getTitles();
    this.colors = this.globalsService.getColors();
    this.authService.currentCustomer.subscribe((c) => {
      this.model = c;

      if (this.model.needs_password_reset) {
        this.tabs.push({ name: 'Password', icon: 'key' });
      }

      if (!this.model.settings.text_color) {
        this.model.settings.text_color = '#222222';
      }

      if (!this.model.settings.bg_color) {
        this.model.settings.bg_color = '#FFFFFF';
      }
    });
  }

  submitCustomer() {
    this.submitted = true;
    this.customerService.update(this.model).subscribe(c => {
      if (c) {
        this.submitted = false;
      this.router.navigateByUrl('/');
      }
    });
  }

  updateColor(key) {
    this.model.settings[key] = this.brandColor[key];
  }

  nextTab() {
    this.currentTab++;
  }

  setLogo(object) {
    this.model.logo.large = object.image.logo;
  }
  setHeadshot(object) {
    this.model.image.large = object.image.headshot;
  }

  submitBranding() {
    if (this.model.needs_password_reset) {
      this.nextTab();
    } else {
      this.submitCustomer();
    }
  }

  changeIndex(nextTab) {
    this.currentTab = nextTab;
  }

  get isBrandingFormValid() {
    return this.model.settings.bg_color &&
      this.model.settings.text_color &&
      this.model.image.large &&
      this.model.logo.large;
  }

  get isPersonalDetailsValid() {
    return this.model.display_name &&
      this.model.brokerage &&
      this.model.preferred_title &&
      this.model.display_email &&
      this.model.display_phone &&
      this.model.license_number;
  }

  get isCustomerDetailsValid() {
    return this.isBrandingFormValid &&
      this.isPersonalDetailsValid;
  }

  get headshotConfig() {
    const target = {
      customer_id: this.model.id
    };
    const config = {
      id: this.model.id,
      imageableType: 'Customer',
      type: 'headshot',
      aspectRatio: 1,
      minWidth: 50,
      minHeight: 50,
    };
    return {
      configMeta: config,
      image: this.model ? this.model.image.large : '',
      isNew: !!this.model.image.large,
      channel: 'HeadshotChannel',
      target: target
    };
  }
  get logoConfig() {
    const target = {
      customer_id: this.model.id
    };
    const config = {
      id: this.model.id,
      imageableType: 'Customer',
      type: 'logo',
      aspectRatio: 1,
      minWidth: 50,
      minHeight: 50,
      maintainRatio: false
    };
    return {
      configMeta: config,
      image: this.model ? this.model.logo.large : '',
      isNew: !!this.model.logo.large,
      channel: 'LogoChannel',
      target: target
    };
  }
}
