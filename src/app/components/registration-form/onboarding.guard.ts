import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { JwtService } from 'src/app/services/jwt/jwt.service';

@Injectable()
/**
 * Blank view guard for view access and data preloading.
 */
export class OnboardingGuard implements CanActivate, CanActivateChild {

  /**
   * Method to determine if we can activate the view based on custom logic.
   * @param {ActivatedRouteSnapshot} next
   * @param {RouterStateSnapshot} state
   * @returns {Observable<boolean> | Promise<boolean> | boolean}
   */
  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private jwtService: JwtService
  ) { }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (window.scrollY > 0) {
      window.scrollTo(0, 0);
    }
    return this.checkCustomer();
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkCustomer();
  }

  checkCustomer(): Observable<boolean> | boolean {
    if (!this.authService.currentCustomerValue || !this.jwtService.getToken()) {
      this.router.navigateByUrl('/login');
      return false;
    }
    return this.authService.refresh().pipe(
      map(c => {
        if (c && !c.onboarded) {
          return true;
        } else if (c) {
          this.router.navigateByUrl('/');
          return false;
        } else {
          this.router.navigateByUrl('/login');
          return false;
        }
      }),
      catchError(e => {
        this.router.navigateByUrl('/login');
        return of(false);
      }));
  }
}
