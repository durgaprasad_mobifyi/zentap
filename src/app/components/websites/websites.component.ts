import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { map } from 'rxjs/operators';

import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';
import { ListingsService } from '../../services/listings/listings.service';
import { WebsiteService } from 'src/app/services/websites/website.service';
import { WebsiteAttributes } from '../../shared/interfaces/website-attributes';

@Component({
  selector: 'app-websites',
  templateUrl: './websites.component.html',
  styleUrls: ['./websites.component.scss']
})
export class WebsitesComponent implements OnInit {
  websites: WebsiteAttributes[] = [];
  website_name;
  selectedIndex = 0;
  loading = false;
  pageSizeOptions = [10];
  mapOfSort: { [key: string]: string | null } = {
    websiteable_type: null
  };
  selectedSort = {
    by: 'websiteable_type',
    order: 'asc'
  };
  websitePagination = {
    limit: 10,
    total: 100,
    page: 1,
    totalPages: 10
  };
  tabs = [{ name: 'Listings', type: 'Listing' }, { name: 'Agents', type: 'Customer' }];
  constructor(
    private ListingService: ListingsService,
    private route: ActivatedRoute,
    private router: Router,
    private breadcrumservce: BreadCrumService,
    private modalService: NzModalService,
    private websiteService: WebsiteService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.breadcrumservce.set_breadcrum();
    this.getWebsites();
  }


  log(args: any[]): void {
    this.websitePagination.page = 1;
    this.websitePagination.page = 1;
    this.loading = true;
    this.getWebsites();
  }

  getWebsites(sort?) {
    this.loading = true;
    let queryParams = `?page=${this.websitePagination.page}`;
    if (sort) {
      queryParams = `${queryParams}&${sort}`;
    }
    this.websiteService.list(queryParams)
    .pipe(
      map((res) => {
        this.websitePagination.limit = res['per_page'];
        this.websitePagination.total = res['total_entries'];
        const limit = this.websitePagination.limit;
        const total_items = this.websitePagination.total;
        this.websitePagination.totalPages = Math.ceil(total_items / limit);
        return res.data;
      })
    ).subscribe((websites: WebsiteAttributes[]) => {
      this.websites = websites;
      this.loading = false;
    });
  }

  didPage(pageIndex) {
    this.websitePagination.page = pageIndex;
    this.getWebsites();
  }

  newSite() {
    this.router.navigateByUrl('/websites/new');
  }

  sort(sort: { key: string; value: string }): void {
    for (const key in this.mapOfSort) {
      this.mapOfSort[key] = key === sort.key ? sort.value : null;
    }
    const valuesMap = {
      descend: 'desc',
      ascend: 'asc'
    };
    this.getWebsites(`q[s]=${sort.key} ${valuesMap[sort.value]}`);
  }
}
