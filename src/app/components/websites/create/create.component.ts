import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { NzModalService } from 'ng-zorro-antd/modal';
import { CardButtonConfig } from 'src/app/models';
import { SwiperOptions } from 'swiper';
import { Content } from 'src/app/vos/content/content';

import { TemplatesService } from '../../../services/templates/templates.service';
import { BreadCrumService } from '../../../services/breadcrum/bread-crum.service';
import { WebsiteService } from '../../../services/websites/website.service';
import { ContentReceiptDialogComponent } from '../../../shared/content-receipt-dialog/content-receipt-dialog.component';

@Component({
  selector: 'app-create-website',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateWebsiteComponent implements OnInit {
  selectedType = '';
  current = 0;
  tagId;
  selectedTemplate;
  selectedListing;
  isCreated = false;
  templates: Content[] = [];
  config: SwiperOptions = {
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 10,
    slidesPerView: 3,
    slidesPerGroup: 3,
    breakpoints: {
      1920: {
        slidesPerView: 4,
        spaceBetween: 10,
        slidesPerGroup: 4
      },
      1700: {
        slidesPerView: 3,
        spaceBetween: 10,
        slidesPerGroup: 3
      },
      1250: {
        slidesPerView: 2,
        spaceBetween: 10,
        slidesPerGroup: 2
      },
      940: {
        slidesPerView: 1,
        spaceBetween: 30,
        slidesPerGroup: 1
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
        slidesPerGroup: 2
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20,
        slidesPerGroup: 1
      }
    }
    // loopFillGroupWithBlank: true,
  };
  agentCardButton: CardButtonConfig = {
    id: 'new',
    title: 'Agent',
    actionText: 'NEW WEBSITE',
    link: 'websites/new',
    image: 'https://universal-promote.s3-us-west-1.amazonaws.com/icons/icon_AgentSite.svg',
    colors: {
      bg: '#30d289',
      textSelected: 'white'
    },
  };
  listCardButton: CardButtonConfig = {
    id: 'new',
    title: 'Listing',
    actionText: 'NEW WEBSITE',
    link: 'websites/new',
    image: 'https://universal-promote.s3-us-west-1.amazonaws.com/icons/icon_ListingSite.svg',
    colors: {
      bg: '#30d289',
      textSelected: 'white'
    },
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public message: NzMessageService,
    private modalService: NzModalService,
    private breadcrumservce: BreadCrumService,
    private templatesService: TemplatesService,
    private websiteService: WebsiteService
  ) { }

  ngOnInit() {
    this.breadcrumservce.set_breadcrum();
  }

  getTemplates() {
    const params = this.selectedType === 'Listing' ? 'tag_id=1' : 'tag_id=2';
    this.templatesService.getPublicUpTemplates(params)
      .subscribe(res => {
        this.templates = res.map(templ => {
          templ.url = templ.image;
          templ.caption = templ.name;
          templ.media_type = 'image';
          return templ;
        });
      });
  }

  selectList(list) {
    this.selectedListing = list;
    this.next();
  }

  done() {
    if (this.selectedType === 'Agent') {
      this.createAgentSite();
    } else {
      this.createListingSite();
    }
  }

  createAgentSite() {
    this.websiteService.create({template_id: this.selectedTemplate.id})
      .subscribe(res => {
        window.open(res.edit_url, '_blank');
        this.isCreated = true;
        this.receipt();
      });
  }

  createListingSite() {
    const url = `/listings/${this.selectedListing.id}/websites.json`;
    this.websiteService.createCustom({template_id: this.selectedTemplate.id}, url)
    .subscribe(res => {
      window.open(res.edit_url, '_blank');
      this.isCreated = true;
      this.receipt();
    });
  }

  setType(type) {
    this.selectedType = type;
    this.getTemplates();
  }

  next() {
    this.current++;
  }

  prev() {
    this.current--;
  }

  selectTemplate(c) {
    this.selectedTemplate = c;
  }

  receipt() {
    this.router.navigateByUrl('websites');
    const modal = this.modalService.create({
      nzContent: ContentReceiptDialogComponent,
      nzComponentParams: {
        config: {
            type: 'website',
            link: 'websites/new'
        }
      },
      nzFooter: null,
      nzWidth: '40%'
    });
    modal.afterClose.subscribe(response => {
      if (response === 'true') {
        this.selectedTemplate = null;
        this.selectedListing = null;
        this.isCreated = false;
      }
    });
  }
}
