import { Component, OnInit, ViewChild } from '@angular/core';
import { Listing } from 'src/app/vos/listing/listing';
import { ActivatedRoute, Router } from '@angular/router';
import { ListingsService } from 'src/app/services/listings/listings.service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
import {ListingFormComponent} from '../../../shared/listing-form/listing-form.component';
import {BreadCrumService} from '../../../services/breadcrum/bread-crum.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';

@Component({
  selector: 'app-edit-listing',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditListingComponent implements OnInit {
  listing = new Listing();
  clonnedListing: Listing;
  isNew = true;
  loading = false;
  error: string;
  queryName: string;
  breadcrum_name;
  // @Output() breadcrums = new EventEmitter<BreadCrumComponent>();
  @ViewChild('listingForm') listingForm: ListingFormComponent;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private listingService: ListingsService,
    private breadcrumService: BreadCrumService,
    private _location: Location,
    private message: NzMessageService,
    private globalService: GlobalsService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (res: { data: Listing }) => {
        if (res.data) {
          this.listing = res.data;
          this.listing.price = this.listing.price ? this.listing.price.toString() : '';
          this.listing.sq_ft = this.listing.sq_ft;
          this.listing.year_build = this.listing.year_build ? this.listing.year_build.toString() : '';
          this.clonnedListing = JSON.parse(JSON.stringify(this.listing));
          this.isNew = false;
        }
      });
    this.route.params.subscribe(params => {
      if (params.id) {
        this.breadcrum_name = this.listing.address;
      } else {
        this.breadcrum_name = 'New Listing';
      }
    });

    this.route.queryParams
      .subscribe(params => {
        this.queryName = params['name'];
      });
    this.breadcrumService.push_breadcrum({ name: this.breadcrum_name });
  }
  cancel() {
    this._location.back();
  }
  save() {
    this.listingForm.checkForm();
    if (!this.isModified) {
      return;
    }
    if (!this.listingForm.valid) {
      this.message.create('error', 'You have invalid fields.');
      return;
    }
    this.loading = true;
    this.error = null;
    let request: Observable<Listing>;
    if (this.isNew) {
      // this.listing.image_ids = [];
      // this.listing.images_attributes.forEach(image => {this.listing.image_ids.push(image.id)});
      // const newListing = {...this.listing};
      // delete newListing['images_attributes'];
      // this.listing.image_ids = images;
      request = this.listingService.create(this.listing);
    } else {
      request = this.listingService.update(this.listing);
    }
    request.subscribe((res) => {
      this.loading = false;
      //  if (res && this.queryName && res.products.length && res.products.find((prod) => prod.name === this.queryName)) {
      if (res && this.queryName) {
        this.router.navigate(['/listings', res.id, this.queryName, 'new'], { queryParams: { step: 2 } });
      } else if (res) {
        this.router.navigate(['/listings', res.id]);
      }
      // } else if (res && this.queryName && res.products.length) {
      //   this.router.navigate(['/listings', res.id, res.products[0].name, 'new'], { queryParams: { step: 2 } });
      // }

    }, err => {
      this.loading = false;
      if (err.status === 422) {
        this.error = 'Check that you have included all required fields and resubmit.';
        this.message.create('error', this.error);
      } if (err.status === 500) {
        this.message.create('error', err.message);
      } else {
        // const msgErrors = err.error;
        // this.message.create('error', msgErrors.error);
        const msgErrors = err.error;
        // tslint:disable-next-line:forin
        for (const key in msgErrors) {
          const errors = msgErrors[key];
          errors.forEach((m) => {
            this.message.create('error', m);
          });
        }
      }
    });
  }
  get diagnostic() { return JSON.stringify(this.listing); }
  get isModified() {
    return JSON.stringify(this.clonnedListing) !== this.diagnostic;
  }
  get imageLoadEnd () {
    return this.globalService.imageLoaded();
  }
}
