import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Listing } from 'src/app/vos/listing/listing';
import { CardButtonConfig } from 'src/app/models';
import { Product } from 'src/app/vos/product/product';
import { ProductsService } from 'src/app/services/products/product-api.service';
import { Content } from 'src/app/vos/content/content';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer'
import {map} from 'rxjs/operators';
import {ContentsService} from '../../../services/contents/contents.service';

@Component({
  selector: 'app-listings-view',
  templateUrl: './view.component.html',
  styleUrls: [ './view.component.scss' ]
})
export class ListingViewComponent implements OnInit {
  listing: Listing;
  products: CardButtonConfig[];
  contents: Content[];
  customer: Customer;
  pageSizeOptions = [10];
  contentPagination = {
    limit: 10,
    total: 100,
    page: 1,
    totalPages: 10
  };
  categories;
  loading = false;
  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private contentService: ContentsService,
    private productService: ProductsService,
    public router: Router
  ) { }

  ngOnInit() {
    this.customer = this.authService.currentCustomerValue;
    this.listing = this.route.snapshot.data[ 'data' ];
    this.loading = true;
    this.fetchContent();
  }

  contentDeleted(content: Content) {
    this.contents = this.contents.filter(c => c.id !== content.id);
  }
  didPage(pageIndex) {
    this.contentPagination.page = pageIndex;
    this.loading = true;
    this.fetchContent();
  }
  fetchContent() {
    this.contentService.list(`?page=${this.contentPagination.page}&q[contentable_type_eq]=Listing&q[contentable_id_eq]=${this.listing.id}`)
      .pipe(
        map((res) => {
          this.contentPagination.limit = res['per_page'];
          this.contentPagination.total = res['total_entries'];
          const limit = this.contentPagination.limit;
          const total_items = this.contentPagination.total;
          this.contentPagination.totalPages = Math.ceil(total_items / limit);
          return res.data;
        })
      ).subscribe(contents => {
      this.contents = contents;
      this.loading = false;
    });
  }

  makeContent() {
    this.router.navigate([`/content_type/listings/${this.listing.id}`]);
  }
}
