import { Component, OnInit } from '@angular/core';
import { TemplatesService } from 'src/app/services/templates/templates.service';
import { Template } from 'src/app/vos/template/template';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-promote-listings',
  templateUrl: './promote.component.html',
  styleUrls: [ './promote.component.scss' ]
})
export class PromoteListingsComponent implements OnInit {
  s: number[] = [];
  current = 0;
  template: Template;
  loading = false;
  resourceId: number;
  get previewURL(): string {

    return this.templateService.previewURLFor(this.template, this.resourceId);
  }
  constructor(
    private templateService: TemplatesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.resourceId = this.route.snapshot.params[ 'id' ];

    this.templateService.list().subscribe(t => {
      this.template = t.data[ 0 ];

    });
  }
  selChange(event) {


  }
  pre(): void {
    this.current -= 1;
  }

  next(): void {
    this.current += 1;
  }

  send(): void {
    this.loading = true;
    this.current = 2;
    this.templateService.blast(this.template.id, this.s, this.resourceId).subscribe(success => {
      if (!success) {
        this.current = 1;
      }
      this.loading = false;
    });
  }

  back() {
    this.router.navigate([
      '/listings/' + this.resourceId
    ]);
  }
}
