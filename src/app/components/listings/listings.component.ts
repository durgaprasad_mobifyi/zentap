import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ListingsService } from 'src/app/services/listings/listings.service';
import { Listing } from 'src/app/vos/listing/listing';
import { CardButtonConfig } from 'src/app/models';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {PaginationService} from '../../services/pagination/pagination.service';
import { GenericDialogComponent } from '../../shared/generic-dialog/generic-dialog.component';

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.scss']
})
export class ListingsComponent implements OnInit {
  @Output() select = new EventEmitter();

  allListings: Listing[] = [];
  @Input()
  isChild: Boolean;
  listings: Listing[] = [];
  loading = false;
  sortings = ['address', 'status'];
  searchText: string;
  mapOfSort: { [key: string]: string | null } = {
    created_at: null,
    address: null
  };
  pageSizeOptions = [10];
  listingPagination = {
    limit: 10,
    total: 10,
    page: 1,
    totalPages: 10
  };
  public searchModelChanged: Subject<string> = new Subject<string>();
  public searchModelChangeSubscription: Subscription;
  selectedSort = {
    by: 'address',
    order: 'asc'
  };

  constructor(
    private router: Router,
    private message: NzMessageService,
    private modalService: NzModalService,
    private listingService: ListingsService,
    private breadcrumService: BreadCrumService,
    private paginationService: PaginationService
  ) {
    this.searchModelChangeSubscription = this.searchModelChanged
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(newText => {
        this.searchText = newText;
        // this.listingPagination.page = 1;
        if (newText) {
          this.fetchContent(newText);
        } else {
          this.fetchContent();
        }
      });
  }

  ngOnInit() {
    this.loading = true;
    this.fetchContent();
    this.breadcrumService.set_breadcrum();
  }

  setSortOption(sortBy) {
    this.selectedSort.by = sortBy;
    this.searchText = '';
    // this.listingPagination.page = 1;
    this.fetchContent();
  }

  toggelSortOrder() {
    const cOrder = this.selectedSort.order;
    if (cOrder === 'asc') {
      this.selectedSort.order = 'desc';
    } else {
      this.selectedSort.order = 'asc';
    }
    // this.listingPagination.page = 1;
    this.fetchContent();
  }

  didPage(pageIndex) {
    this.listingPagination.page = pageIndex;
    this.loading = true;
    this.paginationService.setCurrentPage(this.listingPagination);
    this.fetchContent();
  }

  fetchContent(text?, sort?) {
    this.loading = true;
    const sortBy = this.selectedSort.by === 'status' ? 'listing_status' : this.selectedSort.by;
    // const pageNo = this.searchText ? 1 : this.listingPagination.page;
    const pageNo = this.paginationService.getCurrentPage();
    let query = `?page=${pageNo}&q[s]=${sortBy} ${this.selectedSort.order}`;
    if (text || this.searchText) {
      query = `${query}&q[address_cont]=${text || this.searchText}`;
    }
    if (sort) {
      query = `${query}&${sort}`;
    }
    this.listingService.list(query).subscribe(response => {
      this.loading = false;
      this.listingPagination.page = pageNo;
      this.listingPagination.limit = response['per_page'];
      this.listingPagination.total = response['total_entries'];
      const limit = this.listingPagination.limit;
      const total_items = this.listingPagination.total;
      this.listingPagination.totalPages = Math.ceil(total_items / limit);

      this.allListings = response.data;
      this.listings = this.allListings;
    });
  }

  sort(sort: { key: string; value: string }): void {
    for (const key in this.mapOfSort) {
      this.mapOfSort[key] = key === sort.key ? sort.value : null;
    }
    const valuesMap = {
      descend: 'desc',
      ascend: 'asc'
    };
    this.fetchContent(null, `q[s]=${sort.key} ${valuesMap[sort.value]}`);
  }

  viewDetail(id) {
    this.router.navigateByUrl(`listings/${id}`);
  }

  editListing(id) {
    this.router.navigateByUrl(`/listings/${id}/edit`);
  }

  createNewListing() {
    this.router.navigateByUrl('listings/new');
  }
  deleteListing(listing) {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: 'Are you sure you want to delete this listing?',
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.listingService.destroy(listing).subscribe(r => {
          this.fetchContent();
          this.message.create('success', 'Listing is deleted successfully!');
        }, e => {
          this.loading = false;
          this.message.create('error', 'Error deleting this listing. Please try again');
        });
      }
    });
  }
}
