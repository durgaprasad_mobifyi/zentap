import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';
import { ListingsService } from '../../services/listings/listings.service';
import { WebsiteService } from 'src/app/services/websites/website.service';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {
  constructor(
    private ListingService: ListingsService,
    private route: ActivatedRoute,
    private router: Router,
    private breadcrumservce: BreadCrumService,
    private modalService: NzModalService,
    private websiteService: WebsiteService
  ) { }

  ngOnInit() {
  }
}
