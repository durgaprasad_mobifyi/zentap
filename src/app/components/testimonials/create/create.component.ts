import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Content } from 'src/app/vos/content/content';
import { BreadCrumService } from '../../../services/breadcrum/bread-crum.service';
import { ContentReceiptDialogComponent } from '../../../shared/content-receipt-dialog/content-receipt-dialog.component';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { TestimonialAttributes } from 'src/app/shared/interfaces/testmonial-attributes';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { NgForm } from '@angular/forms';
import { ProductStyle, Product } from 'src/app/vos/product/product';
import { ProductsService } from 'src/app/services/products/product-api.service';
import { Customer } from 'src/app/vos/customer/customer';
import { ContentFormComponent } from 'src/app/shared/content-form/content-form.component';

@Component({
  selector: 'app-create-testimonial',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateTestimonialComponent implements OnInit {
  content = new Content();
  testimonial = new TestimonialAttributes();
  images = [];
  galleryImage: any;
  @ViewChild('testimonialForm') form: NgForm;
  @ViewChild('contentForm') contentForm: ContentFormComponent;
  current = 0;
  requiredFields = [
    'familyphoto',
    'FamilyName',
    'Message'
  ];
  isPhotoValid = false;
  isGalleryValid = false;
  valid = false;
  styles: ProductStyle[] = [];
  product: Product;
  customer: Customer;
  contentSubject: Customer;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public message: NzMessageService,
    private modalService: NzModalService,
    private breadcrumservce: BreadCrumService,
    private authService: AuthenticationService,
    private contentService: ContentsService,
    private productService: ProductsService
  ) { }

  ngOnInit() {
    this.contentSubject = this.authService.currentCustomerValue;
    this.route.params.subscribe(params => {
        this.customer = this.authService.currentCustomerValue;
        const endpoint = `customers/${this.customer.id}/products`;
        const search_params = { 'q[name_eq]': 'testimonial_videos' };
        this.productService.productList(endpoint, search_params).subscribe(p => {
          this.contentSubject.products = p.data;
          const products = this.contentSubject.products.map((product: Product) => new Product(product));
          this.product = products.find((prod) => prod.name === 'testimonial_videos');
          if (this.product.parent_type.includes('customer')) {
            this.content.contentable_type = 'Customer';
            this.content.contentable_id = this.authService.currentCustomerValue.id;
            this.fetchStyles();
          }
        });
    });
    this.form.valueChanges.subscribe(() => {
      this.valid = this.form.valid;
    });

    this.breadcrumservce.push_breadcrum({ name: 'Testimonial Videos' });
    this.content.category = 'TS';
  }
  create() {
    this.content.extra_attributes = this.testimonial;
    this.content.image_ids = this.images;
    this.contentService.create(this.content).subscribe(response => {
      this.router.navigate(['/']);
      this.receipt();

    }, err => {
      this.message.create('error', err.message);
    });

  }
  receipt() {
    const modal = this.modalService.create({
      nzContent: ContentReceiptDialogComponent,
      nzComponentParams: {
        config: {
          type: 'listing_video',
          link: '/content'
        }
      },
      nzFooter: null,
      nzWidth: '40%'
    });
  }
  back() {
    this.router.navigate([
      '/'
    ]);
  }

  setFamilyImage(image) {
    if (image) {
      this.images[0] = image.id;
      this.isPhotoValid =  true;
    }
  }
  previous() {
    this.current -= 1;
  }
  next() {
    this.current += 1;
    this.fetchStyles();
  }
  fetchStyles() {
    this.productService.styles(this.product, this.contentSubject)
      .subscribe(
        res => {
          if (res) {
            this.styles = res.data;
            }
        }
      );
  }
  get isImageValid() {
    return this.isPhotoValid;
  }
  get attestantPhotoConfig () {
    const uid =  this.randomString;
    const target = {
      uid: `attestant_photo_${uid}`
    };
    const config=  {
      imageableType: 'Content',
      type: 'attestant_photo',
      aspectRatio: 1,
      minWidth: 200,
      minHeight: 200,
      uid: `attestant_photo_${uid}`
    }
    return {
      configMeta: config,
      channel: 'CreateImageChannel',
      target: target
    }
  }
  randomString() {
    let result = '';
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 10; i > 0; --i) { result += chars[Math.round(Math.random() * (chars.length - 1))]; }
    return result;
  }
}
