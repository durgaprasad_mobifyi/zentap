import { Routes } from '@angular/router';
import { MarketReportsResolverService } from '../../services/market-reports/market-reports-resolver.service';
import { MarketReportsComponent } from '../../components/market-reports/market-reports.component';
import { EditMarketReportComponent } from '../../components/market-reports/edit/edit.component';
import { MarketReportsViewComponent } from '../../components/market-reports/view/view.component';
/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const MarketReportsRoutes: Routes = [
  {
    path: '',
    component: MarketReportsComponent
  },
  {
    path: 'new',
    component: EditMarketReportComponent
  },
  {
    path: ':id',
    component: MarketReportsViewComponent,
    resolve: {
      data: MarketReportsResolverService
    }
  },
  {
    path: ':id/edit',
    component: EditMarketReportComponent,
    resolve: {
      data: MarketReportsResolverService
    }
  },
];

