import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MarketReportsRoutes } from './market-reports.routing.module';
import { SharedModule } from '../../shared/shared.module';
import {MarketReportsComponent} from '../../components/market-reports/market-reports.component';
import {MarketReportsViewComponent} from '../../components/market-reports/view/view.component';
import {EditMarketReportComponent} from '../../components/market-reports/edit/edit.component';

@NgModule({
  declarations: [
    MarketReportsComponent,
    MarketReportsViewComponent,
    EditMarketReportComponent
  ],
  imports: [
    RouterModule.forChild(MarketReportsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class MarketReportsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
