import { Routes } from '@angular/router';
import { AdsshowComponent } from '../../components/ads/show/show.component';
/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const AdsRoutes: Routes = [
  // {
  //   path: '',
  //   component: AdsComponent,
  // },
  // {
  //   path: 'new',
  //   component: AdsNewComponent,
  // },
  {
    path: ':id',
    component: AdsshowComponent,
  },
];

