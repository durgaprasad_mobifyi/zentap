import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AdsRoutes } from './ads.routing.module';
import { SharedModule } from '../../shared/shared.module';

import {AdsComponent} from '../../components/ads/ads.component';
import {AdsNewComponent} from '../../components/ads/new/new.component';
import {AdsshowComponent} from '../../components/ads/show/show.component';
import {AdsCardComponent} from '../../shared/ads-card/ads-card.component';

@NgModule({
  declarations: [
    AdsComponent,
    AdsCardComponent,
    AdsNewComponent,
    AdsshowComponent,
  ],
  imports: [
    RouterModule.forChild(AdsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})
export class AdsModule {
  constructor() {
  }
}
