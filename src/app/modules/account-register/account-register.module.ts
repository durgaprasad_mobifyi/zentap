import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountRegisterRoutes } from './account-register.routing.module';

import { SharedModule } from '../../shared/shared.module';
import { RegisterComponent } from '../../components/register/register.component';


@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    RouterModule.forChild(AccountRegisterRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class AccountRegisterModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
