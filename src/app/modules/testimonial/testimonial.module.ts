import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestimonialRoutes } from './testimonial.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CreateTestimonialComponent } from '../../components/testimonials/create/create.component';
import { TestimonialsComponent } from '../../components/testimonials/testimonials.component';


@NgModule({
  declarations: [
    CreateTestimonialComponent,
    TestimonialsComponent
  ],
  imports: [
    RouterModule.forChild(TestimonialRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class TestimonialModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
