import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTestimonialComponent } from '../../components/testimonials/create/create.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const TestimonialRoutes: Routes = [
  {
    path: 'new',
    component: CreateTestimonialComponent,
  },
];

