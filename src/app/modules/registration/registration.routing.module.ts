import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  RegistrationFormComponent } from '../../components/registration-form/registration-form.component';



/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const RegistrationRoutes: Routes = [
  {
    path: '',
    component: RegistrationFormComponent,
  }
];

