import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RegistrationRoutes } from './registration.routing.module';
import { SharedModule } from '../../shared/shared.module';

import {  RegistrationFormComponent } from '../../components/registration-form/registration-form.component';

@NgModule({
  declarations: [
    RegistrationFormComponent,
  ],
  imports: [
    RouterModule.forChild(RegistrationRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class RegistrationModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
