import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from '../../components/settings/settings.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const SettingsRoutes: Routes = [
  {
    path: '',
    component: SettingsComponent,
  },
];

