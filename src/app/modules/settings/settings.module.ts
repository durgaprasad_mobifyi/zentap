import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SettingsRoutes } from './settings.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { SettingsComponent } from '../../components/settings/settings.component';


@NgModule({
  declarations: [
    SettingsComponent
  ],
  imports: [
    RouterModule.forChild(SettingsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class SettingsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
