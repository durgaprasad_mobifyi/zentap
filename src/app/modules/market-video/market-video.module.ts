import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MarketVideoRoutes } from './market-video.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { MarketVideoComponent } from '../../components/market-video/market-video.component';


@NgModule({
  declarations: [
    MarketVideoComponent
  ],
  imports: [
    RouterModule.forChild(MarketVideoRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class MarketVideoModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
