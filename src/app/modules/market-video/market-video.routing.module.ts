import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarketVideoComponent } from '../../components/market-video/market-video.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const MarketVideoRoutes: Routes = [
  {
    path: '',
    component: MarketVideoComponent,
  },
];

