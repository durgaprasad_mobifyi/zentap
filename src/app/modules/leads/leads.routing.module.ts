import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeadsComponent } from '../../components/leads/leads.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const LeadsRoutes: Routes = [
  {
    path: '',
    component: LeadsComponent,
  },
];

