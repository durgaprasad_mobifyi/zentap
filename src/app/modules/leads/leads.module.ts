import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LeadsRoutes } from './leads.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { LeadsComponent } from '../../components/leads/leads.component';

@NgModule({
  declarations: [
    LeadsComponent
  ],
  imports: [
    RouterModule.forChild(LeadsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class LeadsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
