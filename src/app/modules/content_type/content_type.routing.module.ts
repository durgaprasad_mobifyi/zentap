import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SelectListingContentComponent} from '../../components/listings/select-content/select-content.component';
import { ListingResolverService } from '../../services/listings/listing-resolver.service';
import { ContentResolverService } from '../../services/contents/content-resolver.service';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ContentTypeRoutes: Routes = [
  {
    path: 'listings/:id',
    component: SelectListingContentComponent,
    resolve: {
      data: ListingResolverService,
      content: ContentResolverService
    }
  }
];

