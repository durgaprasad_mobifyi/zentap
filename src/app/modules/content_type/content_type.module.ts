import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContentTypeRoutes } from './content_type.routing.module';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule.forChild(ContentTypeRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ContentTypeModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
