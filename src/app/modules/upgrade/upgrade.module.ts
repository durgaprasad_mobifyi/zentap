import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { UpgradeRoutes } from './upgrade.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { UpgradeComponent } from '../../components/upgrade/upgrade.component';



@NgModule({
  declarations: [
    UpgradeComponent
  ],
  imports: [
    RouterModule.forChild(UpgradeRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class UpgradeModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
