import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpgradeComponent } from '../../components/upgrade/upgrade.component';


/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const UpgradeRoutes: Routes = [
  {
    path: '',
    component: UpgradeComponent,
  },
];

