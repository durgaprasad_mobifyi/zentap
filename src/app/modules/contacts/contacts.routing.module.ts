import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsComponent } from '../../components/contacts/contacts.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ContactsRoutes: Routes = [
  {
    path: '',
    component: ContactsComponent,
  },
];

