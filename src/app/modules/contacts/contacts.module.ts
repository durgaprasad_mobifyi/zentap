import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContactsRoutes } from './contacts.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ContactsComponent } from '../../components/contacts/contacts.component';


@NgModule({
  declarations: [
    ContactsComponent
  ],
  imports: [
    RouterModule.forChild(ContactsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ContactsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
