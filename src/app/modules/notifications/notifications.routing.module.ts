import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotificationComponent } from '../../components/notification/notification.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const NotificationsRoutes: Routes = [
  {
    path: '',
    component: NotificationComponent,
  },
];

