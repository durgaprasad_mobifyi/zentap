import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NotificationsRoutes } from './notifications.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { NotificationComponent } from '../../components/notification/notification.component';


@NgModule({
  declarations: [
    NotificationComponent
  ],
  imports: [
    RouterModule.forChild(NotificationsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class NotificationsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
