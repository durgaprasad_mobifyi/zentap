import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlastEmailComponent } from '../../components/blast-email/blast-email.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const BlastEmailRoutes: Routes = [
  {
    path: '',
    component: BlastEmailComponent,
  },
];

