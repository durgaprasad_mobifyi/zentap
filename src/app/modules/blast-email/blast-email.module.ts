import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BlastEmailRoutes } from './blast-email.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { BlastEmailComponent } from '../../components/blast-email/blast-email.component';


@NgModule({
  declarations: [
    BlastEmailComponent
  ],
  imports: [
    RouterModule.forChild(BlastEmailRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class BlastEmailModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
