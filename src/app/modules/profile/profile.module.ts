import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProfileRoutes } from './profile.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ProfileComponent } from '../../components/profile/profile.component';


@NgModule({
  declarations: [
    ProfileComponent
  ],
  imports: [
    RouterModule.forChild(ProfileRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ProfileModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
