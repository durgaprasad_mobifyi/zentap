import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WelcomePackageComponent} from '../../components/welcome-package/welcome-package.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const WelcomePackageRoutes: Routes = [
  {
    path: '',
    component: WelcomePackageComponent,
  },
];

