import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WelcomePackageRoutes } from './welcome-package.routing.module';
import { SharedModule } from '../../shared/shared.module';
import {WelcomePackageComponent} from '../../components/welcome-package/welcome-package.component';


@NgModule({
  declarations: [
    WelcomePackageComponent
  ],
  imports: [
    RouterModule.forChild(WelcomePackageRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class WelcomePackageModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
