import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentComponent } from '../../components/content/content.component';
import { ContentResolverService } from '../../services/contents/content-resolver.service';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ContentRoutes: Routes = [
  {
    path: '',
    component: ContentComponent,
    resolve: {
      content: ContentResolverService
    }
  },
];

