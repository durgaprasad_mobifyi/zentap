import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContentRoutes } from './content.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ContentComponent } from '../../components/content/content.component';

@NgModule({
  declarations: [
    ContentComponent
  ],
  imports: [
    RouterModule.forChild(ContentRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ContentModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
