import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResetPasswordComponent } from '../../components/reset-password/reset-password.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ForgotPasswordRoutes: Routes = [
  {
    path: '',
    component: ResetPasswordComponent,
    data: { state: 'forgot_pass' }
  },
  {
    path: ':code',
    component: ResetPasswordComponent,
    data: { state: 'set_pass' }
  }
];

