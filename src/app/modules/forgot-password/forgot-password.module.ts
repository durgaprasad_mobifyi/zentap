import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ForgotPasswordRoutes } from './forgot-password.routing.module';

import { SharedModule } from '../../shared/shared.module';
import { ResetPasswordComponent } from '../../components/reset-password/reset-password.component';


@NgModule({
  declarations: [
    ResetPasswordComponent
  ],
  imports: [
    RouterModule.forChild(ForgotPasswordRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ForgotPasswordModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
