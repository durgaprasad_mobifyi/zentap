import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentRequestComponent } from '../../components/content-request/content-request.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ContentRequestRoutes: Routes = [
  {
    path: '',
    component: ContentRequestComponent,
  },
];

