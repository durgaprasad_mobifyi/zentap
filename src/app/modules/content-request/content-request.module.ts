import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ContentRequestRoutes } from './content-request.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ContentRequestComponent } from '../../components/content-request/content-request.component';


@NgModule({
  declarations: [
    ContentRequestComponent
  ],
  imports: [
    RouterModule.forChild(ContentRequestRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ContentRequestModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
