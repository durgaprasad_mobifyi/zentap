import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LinkedinAuthComponent } from '../../components/linkedin-auth/linkedin-auth.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const LinkedinRoutes: Routes = [
  {
    path: '',
    component: LinkedinAuthComponent,
  },
];

