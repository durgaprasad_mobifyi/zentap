import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LinkedinRoutes } from './linkedin.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { LinkedinAuthComponent } from '../../components/linkedin-auth/linkedin-auth.component';


@NgModule({
  declarations: [
    LinkedinAuthComponent
  ],
  imports: [
    RouterModule.forChild(LinkedinRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class LinkedinModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
