import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  CreateWebsiteComponent } from '../../components/websites/create/create.component';
import { WebsitesComponent } from '../../components/websites/websites.component';
import { WebsiteResolverService } from '../../services/websites/website-resolver.service';


import { ContentResolverService } from '../../services/contents/content-resolver.service';
import { EditListingComponent } from '../../components/listings/edit/edit.component';
import { ListingViewComponent } from '../../components/listings/view/view.component';
import { ListingResolverService } from '../../services/listings/listing-resolver.service';
import { PromoteListingsComponent } from '../../components/listings/promote/promote.component';
import { ListingsComponent } from '../../components/listings/listings.component';



/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const WebsitesRoutes: Routes = [
  {
    path: '',
    component: WebsitesComponent,
        resolve: {
          data: WebsiteResolverService,
        }
  },
  {
    path: 'new',
    component: CreateWebsiteComponent,
  }
];

