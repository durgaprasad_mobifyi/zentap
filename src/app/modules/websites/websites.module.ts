import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WebsitesRoutes } from './websites.routing.module';
import { SharedModule } from '../../shared/shared.module';

import {  CreateWebsiteComponent } from '../../components/websites/create/create.component';
import { WebsitesComponent } from '../../components/websites/websites.component';

@NgModule({
  declarations: [
    CreateWebsiteComponent,
    WebsitesComponent
  ],
  imports: [
    RouterModule.forChild(WebsitesRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class WebsitesModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
