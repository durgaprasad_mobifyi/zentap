import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ConfirmationRoutes } from './confirmation.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ConfirmationComponent } from '../../components/confirmation/confirmation.component';


@NgModule({
  declarations: [
    ConfirmationComponent
  ],
  imports: [
    RouterModule.forChild(ConfirmationRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ConfirmationModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
