import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmationComponent } from '../../components/confirmation/confirmation.component';


/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ConfirmationRoutes: Routes = [
  {
    path: '',
    component: ConfirmationComponent,
  },
  {
    path: ':code',
    component: ConfirmationComponent,
  }
];

