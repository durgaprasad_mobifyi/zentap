import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SelectResourceComponent } from '../../components/select-resource/select-resource.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const SelectResourceRoutes: Routes = [
  {
    path: '',
    component: SelectResourceComponent,
  },
];

