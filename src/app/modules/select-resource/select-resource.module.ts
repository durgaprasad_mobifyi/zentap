import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SelectResourceRoutes } from './select-resource.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { SelectResourceComponent } from '../../components/select-resource/select-resource.component';

@NgModule({
  declarations: [
    SelectResourceComponent
  ],
  imports: [
    RouterModule.forChild(SelectResourceRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class SelectResourceModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
