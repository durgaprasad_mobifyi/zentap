import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InfographicsRoutes } from './infographics.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { InfographicsComponent } from '../../components/infographics/infographics.component';


@NgModule({
  declarations: [
    InfographicsComponent
  ],
  imports: [
    RouterModule.forChild(InfographicsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class InfographicsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
