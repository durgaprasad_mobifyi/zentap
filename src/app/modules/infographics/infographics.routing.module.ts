import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfographicsComponent } from '../../components/infographics/infographics.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const InfographicsRoutes: Routes = [
  {
    path: '',
    component: InfographicsComponent,
  },
];

