import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ListingsRoutes } from './listings.routing.module';
import { SharedModule } from '../../shared/shared.module';

import {ListingPreviewComponent} from '../../shared/listing-preview/listing-preview.component';
import {ListingsComponent} from '../../components/listings/listings.component';
import {EditListingComponent} from '../../components/listings/edit/edit.component';
import {ListingViewComponent} from '../../components/listings/view/view.component';
import {PromoteListingsComponent} from '../../components/listings/promote/promote.component';

@NgModule({
  declarations: [
    ListingsComponent,
    EditListingComponent,
    ListingPreviewComponent,
    ListingViewComponent,
    PromoteListingsComponent,
  ],
  imports: [
    RouterModule.forChild(ListingsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class ListingsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
