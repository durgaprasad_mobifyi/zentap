import { Routes } from '@angular/router';
import { ContentResolverService } from '../../services/contents/content-resolver.service';
import { EditListingComponent } from '../../components/listings/edit/edit.component';
import { ListingViewComponent } from '../../components/listings/view/view.component';
import { ListingResolverService } from '../../services/listings/listing-resolver.service';
import { PromoteListingsComponent } from '../../components/listings/promote/promote.component';
import { ListingsComponent } from '../../components/listings/listings.component';



/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const ListingsRoutes: Routes = [
  {
    path: '',
    component: ListingsComponent
  },
  {
    path: 'new',
    component: EditListingComponent,
  },
  {
    path: ':id',
    component: ListingViewComponent,
    resolve: {
      data: ListingResolverService,
      content: ContentResolverService
    }
  },
  {
    path: ':id/edit',
    component: EditListingComponent,
    resolve: {
      data: ListingResolverService
    }
  },
  {
    path: ':id/promote',
    component: PromoteListingsComponent,
    resolve: {
      data: ListingResolverService
    }
  },
];

