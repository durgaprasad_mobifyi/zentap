import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AdditionalsRoutes } from './additionals.routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AdditionalServicesComponent } from '../../components/additional-services/additional-services.component';

@NgModule({
  declarations: [
    AdditionalServicesComponent
  ],
  imports: [
    RouterModule.forChild(AdditionalsRoutes),
    SharedModule,
  ],
  providers: [],
  entryComponents: [],
})

/**
 * Main app module. Import your submodules here.
 */
export class AdditionalsModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    // bugsnagClient.notify(new Error('Test error'));
  }
}
