import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdditionalServicesComponent } from '../../components/additional-services/additional-services.component';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
export const AdditionalsRoutes: Routes = [
  {
    path: '',
    component: AdditionalServicesComponent,
  },
];

