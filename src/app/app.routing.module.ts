import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginGuard } from './components/login/login.guard';
import { AppGuard } from './app.guard';
import { OnboardingGuard } from './components/registration-form/onboarding.guard';
import { TypeResolverService } from './services/type-resolver.service';
import { WebsitesComponent } from './components/websites/websites.component';
import { CreateWebsiteComponent } from './components/websites/create/create.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
const routes: Routes = [
  {
    'path': 'login',
    component: LoginComponent,
    canActivate: [LoginGuard]
  },
  { 'path': 'logout', canActivate: [LoginGuard], component: LoginComponent, data: { user_logout: true } },
  {
    path: 'forgot_password',
    loadChildren: './modules/forgot-password/forgot-password.module#ForgotPasswordModule'
  },
  {
    path: 'reset_password',
    loadChildren: './modules/forgot-password/forgot-password.module#ForgotPasswordModule'
  },
  {
    path: 'register',
    loadChildren: './modules/account-register/account-register.module#AccountRegisterModule'
  },
  {
    path: 'confirmation',
    loadChildren: './modules/confirmation/confirmation.module#ConfirmationModule'
  },
  {
    path: 'registration',
    canActivateChild: [OnboardingGuard],
    loadChildren: './modules/registration/registration.module#RegistrationModule'
  },
  {
    path: '',
    canActivateChild: [AppGuard],
    component: HomeComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'content_type',
        loadChildren: './modules/content_type/content_type.module#ContentTypeModule',

      },
      // lazyload websites module
      {
        path: 'websites',
        loadChildren: './modules/websites/websites.module#WebsitesModule'
      },
      {
        path: 'upgrade',
        loadChildren: './modules/upgrade/upgrade.module#UpgradeModule'
      },
      {
        path: 'plan',
        loadChildren: './modules/upgrade/upgrade.module#UpgradeModule'
      },
      {
        path: 'leads',
        loadChildren: './modules/leads/leads.module#LeadsModule'
      },
      {
        path: 'additional_services',
        loadChildren: './modules/additionals/additionals.module#AdditionalsModule'
      },
      // lazy loading ads module
      // {
      //   path: 'ads',
      //   loadChildren: './modules/ads/ads.module#AdsModule'
      // },
      {
        path: 'notifications',
        loadChildren: './modules/notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'contacts',
        loadChildren: './modules/contacts/contacts.module#ContactsModule'
      },
      {
        path: 'email_campaigns/new',
        loadChildren: './modules/blast-email/blast-email.module#BlastEmailModule'
      },
      {
        path: 'testimonial_videos',
        loadChildren: './modules/testimonial/testimonial.module#TestimonialModule'
      },
      {
        path: ':resourceType/:resourceId/email_campaigns/new',
        loadChildren: './modules/blast-email/blast-email.module#BlastEmailModule'
      },
      {
        path: 'profile',
        loadChildren: './modules/profile/profile.module#ProfileModule'
      },
      {
        path: 'content',
        loadChildren: './modules/content/content.module#ContentModule'
      },
      {
        path: 'social_media_posts/new',
        loadChildren: './modules/infographics/infographics.module#InfographicsModule'
      },
      // lazy listings ads module
      {
        path: 'listings',
        loadChildren: './modules/listings/listings.module#ListingsModule'
      },
      // lazy market_reports ads module
      {
        path: 'market_reports',
        loadChildren: './modules/market-reports/market-reports.module#MarketReportsModule'
      },
      {
        path: 'welcome',
        loadChildren: './modules/welcome-package/welcome-package.module#WelcomePackageModule'
      },
      {
        path: ':type/new',
        loadChildren: './modules/content-request/content-request.module#ContentRequestModule'
      },
      {
        path: ':type/:name',
        loadChildren: './modules/select-resource/select-resource.module#SelectResourceModule'
      },
      {
        path: ':type/marketing_videos/new',
        loadChildren: './modules/market-video/market-video.module#MarketVideoModule'
      },
      {
        path: 'marketing_videos',
        loadChildren: './modules/market-video/market-video.module#MarketVideoModule'
      },
      {
        path: ':type/:form/new',
        loadChildren: './modules/content-request/content-request.module#ContentRequestModule'
      },
      {
        path: 'linkedin',
        loadChildren: './modules/linkedin/linkedin.module#LinkedinModule'
      },
      {
        path: 'settings',
        loadChildren: './modules/settings/settings.module#SettingsModule'
      },
      {
        path: ':type/:id/:form/new',
        loadChildren: './modules/content-request/content-request.module#ContentRequestModule',
        resolve: {
          data: TypeResolverService
        }
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: !environment.production })],
  exports: [RouterModule],
  providers: [AppGuard]
})

export class AppRoutingModule {
}
