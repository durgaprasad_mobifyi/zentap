import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {AuthenticationService} from './services/authentication/authentication.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {JwtService} from './services/jwt/jwt.service';
import {WindowService} from './services/window/window.service';
import {DeviceDetectorService} from 'ngx-device-detector';
import { OverlayModule } from '@angular/cdk/overlay';
import {SubscriptionService} from './services/subscriptions/subscriptions.service';
import {NgZorroAntdModule} from 'ng-zorro-antd';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        OverlayModule,
        NgZorroAntdModule,
        ServiceWorkerModule.register('', {enabled: false})
      ],
      providers: [
        AuthenticationService,
        JwtService,
        WindowService,
        DeviceDetectorService,
        SubscriptionService
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Universal Promote'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Universal Promote');
  }));

});
