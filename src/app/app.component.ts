import {Component, Input, OnInit, AfterViewInit} from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { SwUpdate } from '@angular/service-worker';
import {interval} from 'rxjs';
import { AuthenticationService } from './services/authentication/authentication.service';
import { Customer } from './vos/customer/customer';
declare global {
  interface String {
    capitalize(): string;
    capitalizeAll(): string;
    titleize(): string;
    regEscape(): string;
  }
  interface Number {
    niceString(): string;
    asDuration(): string;
  }

}

String.prototype.regEscape = function (): string {
  return this.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};
String.prototype.capitalize = function (): string {
  return this.charAt(0).toUpperCase() + this.slice(1);
};
String.prototype.capitalizeAll = function (): string {
  return this.split(' ')
    .map(s => s.capitalize())
    .join(' ');
};
String.prototype.titleize = function (): string {
  return this
    .split('_')
    .join(' ')
    .capitalizeAll();
};
Number.prototype.niceString = function (): string {
  return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};


declare const identifyUser, pendo;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})

/**
 * Main App Component.
 */
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Universal Promote';
  customer: Customer;
  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    updates: SwUpdate,
    private authService: AuthenticationService,
  ) {
    iconRegistry.addSvgIcon(
      'uplogo',
      sanitizer.bypassSecurityTrustResourceUrl('assets/logos/up-logo.svg')
    );
    updates.available.subscribe(event => {
      updates.activateUpdate().then(() => document.location.reload());
    });
    if (updates.isEnabled) {
      updates.checkForUpdate();
      interval(6 * 60 * 60).subscribe(() => updates.checkForUpdate());
    }
  }
  /**
   * Called part of the component lifecycle. Best first
   * place to start adding your code.
   */
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.authService.currentCustomer.subscribe(customer => {
      if (customer) {
        this.customer = customer;
        this.setPendoCustomer(customer);
      }
    });
  }

  setPendoCustomer(c: Customer) {
    pendo.initialize({
      visitor: {
        id: c.id,
        email: c.email,
        first_name: c.first_name,
        last_name: c.last_name,
        crm_id: c.crm_id,
        tier: c.tier,
        sign_in_count: c.sign_in_count,
        last_sign_in_at: c.last_sign_in_at,
        confirmation_sent_at: c.confirmation_sent_at,
        confirmed_at: c.confirmed_at
      }
    });
  }

}

