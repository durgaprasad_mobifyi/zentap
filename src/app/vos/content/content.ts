import * as moment from 'moment';
import { Image } from '../image/image';
import { Resource } from '../resource/resource';
import { TestimonialAttributes } from 'src/app/shared/interfaces/testmonial-attributes';

export class Content extends Resource {
  // public static categories: { [key: string]: { label: string, mime_type: string }} = {

  // }
  /**
   * id
   */
  id?: number;

  /**
   * caption
   */
  caption: string;

  /**
   * url
   */
  url: string;

  /**
   * status
   */
  status: string;

  /**
   * posted_at
   */
  posted_at?: moment.Moment;

  /**
   * customer_id
   */
  customer_id: number;

  /**
   * created_at
   */
  created_at?: moment.Moment;

  /**
   * updated_at
   */
  updated_at?: moment.Moment;

  /**
   * category
   */
  category: string;

  /**
   * style
   */
  style?: string;

  /**
   * bg_color
   */
  bg_color = '#FFFFFF';

  /**
   * text_color
   */
  text_color = '#222222';

  /**
   * Content Reference Type
   */
  contentable_type: string;

  /**
   * Id of the referenced contentable object
   */
  contentable_id: number;

  media_type: string;

  thumbnail?: string;

  cta_link?: string;

  ad_id?: string;
  sheet_id?: string;
  headline?: string;
  description?: string;
  ad_set_id?: string;
  ad_creative_id?: string;
  is_template: boolean;
  title?: string;
  contentable?: any;
  display_name?: string;
  extra_attributes: any;
  // image_ids?: [];
  // images_attributes?: Image[];
  get display_type(): string {
    switch (this.category) {
      case 'LV':
      case 'CG':
      case 'OH':
      case 'DV':
      case 'MK':
      case 'VB':
        return 'video';
      case 'LF':
      case 'IF':
        return 'image';
      default:
        return 'unknown';
    }
  }
  get fileext(): string {
    if (!this.url) {
      return '';
    }
    const urlEls = this.url.split('.');
    return urlEls[ urlEls.length - 1 ];
  }
  get filename(): string {
    return `${this.category}_${this.contentable_type}_${moment().format('MMDDYYYY')}.${this.fileext}`;
  }
}
