import * as moment from 'moment';
import { Resource } from '../resource/resource';
import { Content } from '../content/content';
import {Campaign} from '../campaign/campaign';

export class Post extends Resource {
  // public static categories: { [key: string]: { label: string, mime_type: string }} = {

  // }
  /**
   * id
   */
  id: number;
  content_id: number;
  posted_at: moment.Moment;
  post_id: number;
  posted_on: string;
  content_attributes: Content;
  /**
   * created_at
   */
  created_at?: moment.Moment;
  /**
   * updated_at
   */
  updated_at?: moment.Moment;
}
