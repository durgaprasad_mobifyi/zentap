import * as moment from 'moment';
export class DashboardSetting {

  id: number;
  name: string;
  value: any;
  created_at: moment.Moment;
  updated_at: moment.Moment;
  value_type: string;

  constructor(vals: any = {}) {
    Object.assign(this, vals);
    this.created_at = moment(this.created_at);
    this.updated_at = moment(this.updated_at);
  }
}
