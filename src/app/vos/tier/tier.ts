import { Product } from '../product/product';
import { Resource } from '../resource/resource';
import {BillingPeriod} from '../../shared/tier-selector/tier-selector.component';

export interface Plan {
  id: string;
  tier: string;
  type: string;
  price: number;
}
export class Tier extends Resource {

  id: string;
  name: string;
  icon: string;
  description: string;
  trial_period: number;
  products: Product[];
  monthly_price: number;
  yearly_price: number;
  plans: Plan[];
  get uniqueProducts(): Product[] {
    const prods: Product[] = [];
    const prodNames = new Set<string>();
    this.products.forEach(p => {
      if (!prodNames.has(p.name)) {
        prods.push(p);
        prodNames.add(p.name);
      }
    });
    return prods;
  }
  price(period: BillingPeriod): number {
    switch (period) {
      case 'mo':
        return this.plans.find(p => p.type === 'monthly').price;
      case 'yr':
        return this.plans.find(p => p.type === 'yearly').price;
    }
  }

  planFor(period: BillingPeriod): Plan {
    switch (period) {
      case 'mo':
        return this.plans.find(p => p.type === 'monthly');
      case 'yr':
        return this.plans.find(p => p.type === 'yearly');
    }
  }

  constructor(vals: any) {
    super(vals);
    this.products = this.products.map(p => new Product(p));
  }

}
