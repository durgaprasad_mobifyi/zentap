import * as moment from 'moment';
import {Resource} from '../resource/resource';
import {Product} from '../product/product';

export class MarketReport extends Resource {

    /**
     * id
     */
    id?: number;

    /**
     * region_name
     */
    region_name: string;

    /**
     * zips
     */
    zips: string[];

    products: Product[]

    /**
     * last_made
     */
    // last_made: moment.Moment;

    /**
     * active
     */
    // active: boolean;

    /**
     * customer_id
     */
    customer_id: number;

    /**
     * created_at
     */
    created_at?: moment.Moment;

    /**
     * updated_at
     */
    // updated_at: moment.Moment;

}
