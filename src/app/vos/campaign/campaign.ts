import * as moment from 'moment';
import { Resource } from '../resource/resource';
import { Content } from '../content/content';

export class Campaign extends Resource {
  // public static categories: { [key: string]: { label: string, mime_type: string }} = {

  // }
  /**
   * id
   */
  id: number;
  customer_id: number;
  campaign_type: string;
  age_max: number;
  age_min: number;
  budget: number;
  location_attributes: Location;
  /**
   * created_at
   */
  created_at?: moment.Moment;
  /**
   * updated_at
   */
  updated_at?: moment.Moment;
}
