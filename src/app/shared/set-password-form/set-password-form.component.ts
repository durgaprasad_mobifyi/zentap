import {
  Component,
  OnInit,
  ViewChild, ElementRef
} from '@angular/core';
import { Customer } from 'src/app/vos/customer/customer';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { NzModalRef, NzMessageService } from 'ng-zorro-antd';
import { PropmixService } from 'src/app/services/propmix/propmix.service';
import { CustomerFormComponent } from '../customer-form/customer-form.component';

export interface OnboardingConfig {
  customer: Customer;
}

@Component({
  selector: 'app-set-password-form',
  templateUrl: './set-password-form.component.html',
  styleUrls: ['./set-password-form.component.scss']
})
export class SetPasswordFormComponent implements OnInit {
  hide = true;
  password: string;
  loading = false;
  submitted = false;
  @ViewChild('passwordField') passwordField: ElementRef;
  config = {customer: null };

  constructor(private propmixService: PropmixService,
    private message: NzMessageService, private customerService: CustomersService,
    public authService: AuthenticationService,
    private modal: NzModalRef
  ) {
  }

  ngOnInit() {
    this.config.customer = this.authService.currentCustomerValue;
  }
  submit(check?: CustomerFormComponent) {
    this.submitted = true;
    if (!this.checkValidity(check)) {
      this.message.create('error', 'You have invalid fields.\nPlease check them and try again.');
      return;
    } else {
      // this.errorMessage = null;
    }
    this.loading = true;
    if (this.password) {
      this.config.customer.password = this.password;
      this.config.customer.password_confirmation = this.password;
      this.config.customer.tracking.needs_password_reset = '0';

    }
    this.customerService.update(this.config.customer).subscribe(c => {
      if (c) {
        this.loading = false;
        this.modal.destroy();
      }
    });
  }
  cancel() {
    this.modal.destroy();
  }
  checkValidity(check?: CustomerFormComponent | any): boolean {

    if (check instanceof CustomerFormComponent) {
      check.checkForm();
      return check.valid;
    } else if (check) {
      if (check.value && check.value.length > 0) {
        return true;
      } else {
        check.focus();
      }
    } else {
      return true;
    }
    // if (this.customerForm) {
    //   this.customerForm.checkForm();

    //   return true;
    //   return this.customerForm.valid;
    // } else if (this.passwordField) {
    //   return true;
    //   return this.passwordField.value && this.passwordField.value.length > 0;
    // } else {
    //   return true;
    // }
  }
}
