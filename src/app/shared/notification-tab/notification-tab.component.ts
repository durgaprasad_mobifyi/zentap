import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subscription } from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import { Notification } from '../../vos/notification/notification';
import {NotificationService} from '../../services/notification/notification.service';
import {ActionCableService, Channel} from 'angular2-actioncable';
import {NzMessageService, NzNotificationService} from 'ng-zorro-antd';
import {environment} from '../../../environments/environment';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Customer} from '../../vos/customer/customer';
import {ListingsService} from '../../services/listings/listings.service';
import {SocialPostDialogComponent} from '../social-post-dialog/social-post-dialog.component';
import {Content} from '../../vos/content/content';
import {NzModalService} from 'ng-zorro-antd/modal';
import {ContentsService} from '../../services/contents/contents.service';

@Component({
  selector: 'app-notification-tab',
  templateUrl: './notification-tab.component.html',
  styleUrls: [ './notification-tab.component.scss' ]
})

export class NotificationTabComponent implements OnInit {
  @ViewChild('notifyTmp') template: TemplateRef<any>;
  loading = false;
  visible = false;
  notifications: Notification[];
  allNotifications: Notification[];
  subscription: Subscription;
  Dot = true;
  customer: Customer;
  content: Content = new Content();
  // Notifications
  notification_channel = 'ActivityNotification::NotificationChannel';
  notification_target = 'Customer';
  newCount = 0;
  mark_all_clear: false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private cableService: ActionCableService,
    private pushNotification: NzMessageService,
    private authService: AuthenticationService,
    private notificationsService: NzNotificationService,
    private listingService: ListingsService,
    private modalService: NzModalService,
    private contentService: ContentsService
  ) {}
  ngOnInit() {
    this.fetch_notifications();
    this.push_notification();
    // this.clearAll();
    this.refreshNotification();
    this.observeListingNotification();
  }
  push_notification() {
    this.customer = this.authService.currentCustomerValue;
    const customer_id = this.customer.id;
    const target = {
      target_type: this.notification_target,
      target_id: customer_id
    };
    const channel: Channel = this.cableService.cable(environment.wss_url).channel(this.notification_channel, target);
    // Subscribe to incoming messages
    this.subscription = channel.received().subscribe(message => {
      this.push_notification_popup(message);
      this.fetch_notifications();
    });
  }
  push_notification_popup(message: Notification) {
    this.notificationsService.template(
      this.template,
      {
        nzPauseOnHover: true,
        nzData: message
      });
  }
  mark_read(notification, event?) {
    if (!notification.opened) {
      if (event) {
        event.target.closest('nz-list-item').classList.remove('un-read');
      }
      document.getElementById('notification-item-' + notification.id);
      this.notificationService.mark_read(notification.id).subscribe();
      this.newCount > 0 ? this.newCount -= 1 : this.newCount = 0;
    }
    this.visible = false;
    if (notification.key === 'content.linkedin.expired') {
      this.connectLinkedin(notification.notifiable_id);
    }
  }
  check_unread (notifications) {
    let notificationCount;
    // this.Dot = notifications.filter(e => e.opened === false).length > 0;
    notificationCount = notifications.filter(e =>  e.opened === false ? e : '');
    this.newCount = notificationCount.length;
  }
  // clearAll() {
  //   this.subscription = this.notificationService.clear_notification().subscribe(data => {
  //     if (!data.mark_single) {
  //       this.mark_all_clear = data.status;
  //       this.newCount = 0;
  //     }
  //   });
  // }
  refreshNotification() {
    this.subscription = this.notificationService.clear_notification().subscribe(data => {
      if (data.mark_single) {
        const index = this.notifications.map(e => e.id).indexOf(data.id);
        // if (index) {
        this.notifications[index].opened = 'true';
        // }
        this.check_unread(this.notifications);
      } else {
        this.mark_all_clear = data.status;
        this.newCount = 0;
      }
    });
  }
  fetch_notifications () {
    this.notificationService.list().subscribe(n => {
      this.notifications = n.data.slice(0, 5);
      this.allNotifications = n.data;
      this.check_unread(this.allNotifications);
    });
  }
  checkNotificationLink(notification) {
    return notification.group_member_count === 0 && notification.type === 'Website';
  }
  observeListingNotification() {
    this.subscription = this.listingService.clearListingNotification().subscribe(data => {
      const index = this.notifications.map(e =>  e.notifiable_path ).indexOf('listings/' + data.id);
      if (index !== -1) {
        this.allNotifications.splice(index, 1);
        this.notifications = this.allNotifications.slice(0, 5);
      }
    });
  }
  connectLinkedin(contentId) {
    this.fetchContent(contentId);
    const modal = this.modalService.create({
      nzContent: SocialPostDialogComponent,
      nzComponentParams: {
        content: this.content,
        hideFacebook: false
      },
      nzFooter: null
    });
    modal.afterClose.subscribe(response => {
    });
  }
  fetchContent(contentId) {
    this.contentService.show(contentId).subscribe(content => {
      this.content = content.data;
    });
  }
}
