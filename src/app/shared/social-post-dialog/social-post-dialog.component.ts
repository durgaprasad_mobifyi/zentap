import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import {GlobalsService} from '../../services/globals/globals.service';
import {Customer} from '../../vos/customer/customer';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {ConnectLinkedinService} from '../../services/connect-linkedin/connect-linkedin.service';
import {NzMessageService} from 'ng-zorro-antd/message';
import {ActionCableService} from 'angular2-actioncable';
import {ConnectFacebookService} from '../../services/connect-facebook/connect-facebook.service';
import {ContentsService} from '../../services/contents/contents.service';
import {Subscription} from 'rxjs';
import {Content} from '../../vos/content/content';
import {UpgradeDialogComponent} from '../upgrade-dialog/upgrade-dialog.component';

@Component({
  selector: 'app-social-post-dialog',
  templateUrl: './social-post-dialog.component.html',
  styleUrls: [ './social-post-dialog.component.scss' ],
  changeDetection: ChangeDetectionStrategy.Default
})

export class SocialPostDialogComponent implements OnInit {
  get getLinkedInCode() {
    const payload = this.linkedInService.getAuthorizationCode();
    if (payload) {
      this.linkedinConnected = (payload && payload.status === 'success');
      // this.cdr.detectChanges();
    }
    return payload;
  }
  subscription: Subscription;
  @Input()
  content: Content;
  @Input()
  hideFacebook = true;
  @Input()
  hidePosting = false;
  @Input()
  config: any;
  all = false;
  facebook = false;
  linkedin = false;
  customer: Customer;
  linkedinConnected = false;
  loading = false;
  status = false;
  @Output()
  posted = new EventEmitter<Content>();
  PaidTier = false;
  constructor(
    private globalsService: GlobalsService,
    private modal: NzModalRef,
    private authService: AuthenticationService,
    private linkedInService: ConnectLinkedinService,
    private message: NzMessageService,
    private cableService: ActionCableService,
    private connectFacebookService: ConnectFacebookService,
    private contentService: ContentsService,
    private modalService: NzModalService,
  ) {}
  ngOnInit() {
    this.authService.currentCustomer.subscribe((c) => {
      this.customer = c;
      this.PaidTier = c.tier === 'ultra';
    });
    this.checkLinkedin();
  }
  connectLinkedIn() {
    this.linkedInService.authorizeLinkedIn();
  }
  checkLinkedin() {
    localStorage.removeItem('linkedInCode');
    this.authService.refresh().subscribe((c) => {
      const integrations = c.integrations_attributes.filter((i) => i.provider === 'linkedin');
      this.linkedinConnected = (integrations.length > 0 && !integrations[0].expired);
    });
  }

  changeAll() {
    this.facebook = this.all;
    this.linkedin = this.all;
  }
  post () {
    if (this.facebook) {
      this.postFB();
    }
    if (this.linkedin) {
      this.postLN();
    }
  }
  postLN () {
    const uid = `${this.content.id}`;
    const request = this.contentService.createPost({
      'uid': uid,
      'post': {
        'content_id': this.content.id,
        'posted_on': 'linkedin' }
    }, 'posts');

    request.subscribe(c => {
      const caption = this.content.caption;
      this.content = c['content_attributes'];
      this.content.caption = caption;
      // const channel: Channel = this.cableService.cable('wss://dev-api.universalpromote.com/cable').channel(`PostsChannel`, { uid: uid });
      // Subscribe to incoming messages
      // this.subscription = channel.received().subscribe(message => {
      //   if (message.status === 'failed')  {
      //     this.message.create('error', 'your post has been failed');
      //     this.status = false;
      //   } else {
      this.message.create('success', 'Your linkedin post has been posted Sucessfully');
      this.status = true;
      // }
      this.content.status = 'posted';
      // });
      this.status = true;
      this.posted.emit(c);
    }, (error) => {
          this.message.create('error', 'Your linkedin post has been failed');
          this.status = false;
    });
  }
  postFB() {
    const uid = `${this.content.id}`;
    const request = this.contentService.createPost({
      'uid': uid,
      'post': { 'content_id': this.content.id,
        'caption': this.content.caption,
        'posted_on': 'facebook' }
    }, 'posts');
    request.subscribe(c => {
      const caption = this.content.caption;
      this.loading = false;
      this.content = c['content_attributes'];
      this.content.caption = caption;
      // const channel: Channel = this.cableService.cable('wss://dev-api.universalpromote.com/cable').channel(`PostsChannel`, { uid: uid });
      // this.subscription = channel.received().subscribe(message => {
      //   if (message.status === 'failed')  {
      //     this.message.create('error', 'your post has been failed');
      //     this.status = false;
      //   } else {
          this.message.create('success', 'Your facebook post has posted Sucessfully');
          this.status = true;
        // }
        this.content.status = 'posted';
      // });
      // this.message.create('success', 'successfully requested your posting, we will notify when its posted!');
      this.status = true;
      this.posted.emit(c);
    }, (error) => {
      this.message.create('error', 'Your facebook post has failed');
      this.status = false;
    });
  }
  removeDialog () {
    this.modal.close();
  }
  upgradePlan() {
    this.modal.close();
    const modal = this.modalService.create({
      nzContent: UpgradeDialogComponent,
      nzFooter: null
    });
  }
}
