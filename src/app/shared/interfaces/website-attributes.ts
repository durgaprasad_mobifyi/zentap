export interface WebsiteAttributes {
  id?: number;
  template_id: number;
  view_url?: string;
  edit_url?: string;
  websiteable_type?: string;
  websiteable_id?: string;
}
