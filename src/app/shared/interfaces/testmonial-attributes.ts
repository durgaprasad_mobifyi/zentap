import {Image} from '../../vos/image/image';

export class TestimonialAttributes {
  Message: string;
  photo0?: Image;
  FamilyName: string;
  familyPhoto?: Image;
}
