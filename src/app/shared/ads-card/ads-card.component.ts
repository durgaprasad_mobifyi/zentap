import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Content } from 'src/app/vos/content/content';
import { Customer } from 'src/app/vos/customer/customer';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
  selector: 'app-ads-card',
  templateUrl: './ads-card.component.html',
  styleUrls: [ './ads-card.component.scss' ]
})
export class AdsCardComponent implements OnInit {
  @Input()
  content: Content;
  // filteredContent: Content;
  customer: Customer;

  // @Input()
  // set content(c: Content) {
  //   this._content = c;
  //   this.contentLength = 10;
  //   this.updateFilter();
  // }
  @Input()
  width = 'auto';

  @Input()
  horizontalMargin = '25px';

  @Input()
  verticalMargin = '25px';

  @Input()
  caption_editable = false;

  @Input()
  disableDeselect = false;

  @Input()
  selectable = false;
  @Output()
  setAdBind = new EventEmitter<Content>();
  @Output()
  isActiveAdBind = new EventEmitter();
  editing = false;
  loading = false;
  initialWidth: string;
  canPost = false;
  elem: ElementRef;
  @Input()
  isActiveAd = false;

  @Output() cardClicked = new EventEmitter();
  emitAdEvent(content) {
    this.setAdBind.emit(content);
    this.isActiveAdBind.emit(this.content.id);
  }
  // emitIsActiveAd(contentId) {
  //
  // }

  get height(): string {
    if (this.width === 'auto') {
      return 'auto';
    } else {
      return '' + (parseInt(this.width.replace(/\D/g, ''), 10) * 296) / 282 + 'px';
    }
  }
  constructor(private authService: AuthenticationService, element: ElementRef) {
    this.elem = element;
  }

  ngOnInit() {
    this.authService.currentCustomer.subscribe(c => (this.customer = c));
  }

  // isActiveAd(adId) {
  //   this.isSelectedAd =  this.selectedAd === adId;
  // }
  showPreview() {
    window.open(this.content.url, 'nil');
  }
}
