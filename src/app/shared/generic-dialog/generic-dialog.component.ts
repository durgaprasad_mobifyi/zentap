import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormControl, Validators, FormGroup } from '@angular/forms';

export interface FormFieldConfig {
  type: string;
  key: string;
  value: any;
  label;
  placeholder: string;
  options?: { value: any, key: string }[];
  required: boolean;
}
export interface GenericDialogConfig {
  title?: string;
  message?: string;
  links?: {
    label: string,
    href: string,
    icon?: string,
    color?: string
  }[];
  buttonLabel?: string;
  content?: {
    type: string,
    url: string,
    autoplay?: boolean
  };
  hideButtons?: boolean;
  extraButtons?: [
    {
      label: string,
      value: any,
      color?: string
    }
  ];
  formFields?: FormFieldConfig[];
  singleSelect?: { value: any, label: string, image: string }[];
}

@Component({
  selector: 'app-generic-dialog',
  templateUrl: './generic-dialog.component.html',
  styleUrls: ['./generic-dialog.component.scss']
})
export class GenericDialogComponent implements OnInit {
  @Input()
  config: GenericDialogConfig;
  get buttonText(): string {
    return this.config.buttonLabel ? this.config.buttonLabel : 'Ok';
  }
  form: FormGroup;
  constructor(
    private modal: NzModalRef
  ) { }

  ngOnInit() {
    if (this.config.formFields) {
      this.form = this.toFormGroup(this.config.formFields);
    }
  }

  toFormGroup(fields: FormFieldConfig[]) {
    const group: any = {};

    fields.forEach(question => {
      group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
        : new FormControl(question.value || '');
    });
    return new FormGroup(group);
  }

  selectOption(val?) {
    // this.dialogRef.close(val);
    this.modal.destroy(val);
  }
}
