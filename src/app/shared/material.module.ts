import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
/**
 *
 *
 * @export
 * @class MaterialModule
 **/

@NgModule({
  imports: [
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
  ],
  exports: [
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
  ],
  declarations: [],
  providers: [],
})
export class MaterialModule {
}
