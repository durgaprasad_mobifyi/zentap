import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { BillingsService } from 'src/app/services/billings/billings.service';
import { BillingSource } from 'src/app/vos/billing/billing';
import { StripeCardCollectorComponent } from '../stripe-card-collector/stripe-card-collector.component';

declare const Stripe;
@Component({
  selector: 'app-payment-selector',
  templateUrl: './payment-selector.component.html',
  styleUrls: ['./payment-selector.component.scss']
})
export class PaymentSelectorComponent implements OnInit {
  // sources: BillingSource[];
  @Input()
  selectedSource: string;
  @Output()
  selectedSourceChange = new EventEmitter();
  sources = [];
  paginatedCards = [];
  page = 0;
  showingAdd = false;
  loading = true;
  displayedColumns = [
    'select',
    'brand',
    'name',
    'last4',
    'expiration'
  ];
  constructor(
    private billingService: BillingsService,
    private modalService: NzModalService,
  ) { }

  ngOnInit() {
    this.loading = true;
    this.billingService.list().subscribe(s => {
      this.sources = s.data;
      this.setPageData();
      this.loading = false;
    });
  }
  setPageData(page?) {
    this.page = page ? page : this.page;
    const pageData = this.sources.slice(this.page * 10, (this.page + 1) * 10);
    this.paginatedCards = pageData.length ? pageData : this.paginatedCards;
  }

  // addSource(source: string) {

  // }
  setSource(source: BillingSource) {
    this.selectedSource = source.id;
    this.selectedSourceChange.emit(this.selectedSource);
  }
  addCard() {
    const modal = this.modalService.create({
      nzContent: StripeCardCollectorComponent,
      nzFooter: null
    });

    // Return a result when closed
    modal.afterClose.subscribe(s => {
      if (s) {
        this.sources = [...this.sources, s];
        this.setPageData();
        this.setSource(s);
      }
    });
  }
}
