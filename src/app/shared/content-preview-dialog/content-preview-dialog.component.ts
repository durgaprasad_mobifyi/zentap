import { Component, OnInit, Input} from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Router } from '@angular/router';
import { DownloaderService } from 'src/app/services/downloader.service';
import { GlobalsService } from '../../services/globals/globals.service';
import { SocialPostDialogComponent } from '../social-post-dialog/social-post-dialog.component';

@Component({
  selector: 'app-content-preview-dialog',
  templateUrl: './content-preview-dialog.component.html',
  styleUrls: ['./content-preview-dialog.component.scss']
})

export class ContentPreviewDialogComponent implements OnInit {
  @Input()
  config: any;
  constructor(
    private globalsService: GlobalsService,
    private modal: NzModalRef,
    private downloader: DownloaderService,
    private modalService: NzModalService,
    private router: Router,
  ) { }
  ngOnInit() {
  }

  download() {
    const content = this.config.content;
    let url = content.url;
    if (url && !url.includes('https')) {
      url = url.replace('http', 'https');
    }
    this.downloader.save(url, content.filename);
  }

  post() {
    if (this.config.content.status === 'ready') {
      const modal = this.modalService.create({
        nzContent: SocialPostDialogComponent,
        nzComponentParams: {
          config: this.config
        },
        nzFooter: null
      });
      modal.afterClose.subscribe(response => {
      });
    }
  }

  gotToEdit() {
    this.modal.close();
    this.router.navigate([`listings/${this.config.content.contentable_id}/edit`]);
  }
}
