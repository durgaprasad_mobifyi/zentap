import { Component, OnInit, Input, Output } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import { ImageGalleryComponent } from '../image-gallery/image-gallery.component';
import { ImageCropperDialogComponent } from '../image-cropper-dialog/image-cropper-dialog.component';
import { ActionCableService, Channel } from 'angular2-actioncable';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { GlobalsService } from '../../services/globals/globals.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-image-handler',
  templateUrl: './image-handler.component.html',
  styleUrls: ['./image-handler.component.scss']
})
export class ImageHandlerComponent implements OnInit {
  @Input() imageable_type;
  @Input() content: any;
  @Input() config: any;
  @Output() imageObject = new EventEmitter();
  subscription: Subscription;
  isNew = false;
  image;
  newImageType;
  updatedImage: string;
  updateImage = false;
  newImage;
  // config: any;
  constructor(
    private modalService: NzModalService,
    private cableService: ActionCableService,
    private globalsService: GlobalsService
  ) { }

  ngOnInit() {
    if (this.content && this.config) {
      this.image = this.config.image;
      this.isNew = this.config.isNew;
    }
  }
  selectFromGallery() {
    const configs = {
      nzFooter: null,
      nzComponentParams: {
        config: {
          aspectRatio: '2:1',
          cols: 3,
          gutterSize: '10px',
          instantSelection: true
        }
      },
      nzWidth: '75%'
    };
    configs['nzContent'] = ImageGalleryComponent;
    const modal = this.modalService.create(configs);

    modal.afterClose.subscribe(image => {
      if (image) {
        this.imageObject.emit({ 'id': image.id, 'type': 'gallery' });
        this.newImage = image.thumb ? image.thumb : image.original;
        this.isNew = true;
      }
    });
  }
  openImageDialog(type) {
    this.setImageLoadingEnd(true);
    this.newImageType = type;
    const modal = this.modalService.create({
      nzContent: ImageCropperDialogComponent,
      nzComponentParams: {
        config: this.config.configMeta
      },
      nzFooter: null
    });
    modal.afterClose.subscribe(image => {
      if (image) {
        this.updatedImage = image;
        this.ImageListener();
      } else {
        this.setImageLoadingEnd(false);
      }
    });
  }
  ImageListener() {
    this.updateImage = true
    const channel: Channel = this.cableService.cable(environment.wss_url).channel(this.config.channel, this.config.target);
    // Subscribe to incoming messages
    this.subscription = channel.received().subscribe(response => {
      if (response) {
        // NEED TO UPDATE
        if (this.newImageType === 'attestant_photo') {
          const image = JSON.parse(response.data);
          this.imageObject.emit({ 'id': image.id, 'image': image });
          this.newImage = image.original;
        } else {
          this.imageObject.emit({'image': response.data});
          this.newImage = this.updatedImage;
        }
        this.isNew = true;
      }
      this.setImageLoadingEnd(false);
      this.updateImage = false;
    });
  }
  setImageLoadingEnd (imageLoading) {
    this.globalsService.imageLoadEnd(imageLoading);
  }
}
