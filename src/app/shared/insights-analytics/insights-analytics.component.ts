import { Component, OnInit, Input, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Customer } from 'src/app/vos/customer/customer';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { InsightsService } from '../../services/insights/insights.service';
import { ActionCableService, Channel } from 'angular2-actioncable';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { ConnectFacebookService } from '../../services/connect-facebook/connect-facebook.service';
import { AdsService } from '../../services/ads/ads.service';
import { CustomersService } from 'src/app/services/customers/customers.service';

@Component({
  selector: 'app-insights-analytics',
  templateUrl: './insights-analytics.component.html',
  styleUrls: ['./insights-analytics.component.scss']
})
export class InsightsAnalyticsComponent implements OnInit, AfterViewInit {
  @Input()
  customer: Customer = new Customer();
  loading = true;
  selectedValue = '';
  insightsDates;
  subscription: Subscription;
  insights = [{display_name: 'Page Views', description: 'Total views on your Facebook Page.', value: ''},
    {display_name: 'Post Engagements', description: 'Number of times people have ' +
    'engaged with your posts through like, comments and shares and more.', value: ''},
    {display_name: 'Reach', description: 'Number of people who saw your ads at least once.', value: ''},
    {display_name: 'Leads', description: 'Number of leads you\'ve acquired from your ads.', value: ''}];
  hasAds = false;
  isVisible = false;
  isfbConnected = true;
  showButton = false;
  constructor(
    private authService: AuthenticationService,
    private insightsService: InsightsService,
    private cableService: ActionCableService,
    private facebookService: ConnectFacebookService,
    private cdRef: ChangeDetectorRef,
    private adsService: AdsService,
    private customerService: CustomersService
  ) { }

  ngOnInit() {
    this.facebookService.getReloadAnalytics().subscribe(response => {
      this.fetchInsightsAnalyticsDates();
    });
  }
  ngAfterViewInit() {
    this.customer = this.authService.currentCustomerValue;
    if (!!this.customer.fb_page_id) {
      this.checkFacebookPageAccess();
      this.fetchInsightsAnalyticsDates();
      this.getAds();
    } else {
      this.isfbConnected = false;
      this.showButton = true;
    }
    // this.loading = false;
    this.cdRef.detectChanges();
  }
  checkFacebookPageAccess() {
    this.customerService.checkFBAccess().subscribe(c => {
      this.isfbConnected = true;
    }, err => {
      this.isfbConnected = false;
      this.showButton = true;
      this.cdRef.detectChanges();
    });
  }
  // Update Current Analytics Date
  updateInsights(event) {
    this.loading = true;
    this.selectedValue = event;
    this.fetchAllInsights();
  }
  fetchInsightsAnalyticsDates() {
    this.insightsService.fetchInsightsDates().subscribe(d => {
      this.insightsDates = d;
      this.selectedValue = this.insightsDates.slice(-2)[0];
      this.fetchAllInsights();
    });
  }
  fetchAllInsights() {
    this.loading = true;
    this.insightsService.fetchInsights(this.selectedValue).subscribe(ins => {
      this.fetchInsightsAnalytics();
    });
  }

  fetchInsightsAnalytics() {
    this.loading = true;
    const customer_id = this.customer.id;
    const channel: Channel = this.cableService.cable(environment.wss_url).channel('InsightsChannel',
      { uid: customer_id });
    // Subscribe to incoming insights
    this.subscription = channel.received().subscribe(insights => {
      if (insights && insights.data) {
        this.loading = false;
        this.showButton = false;
        this.insights = insights.data;
        this.cdRef.detectChanges();
        this.destroySubscription(channel);
      }
    });
  }
  destroySubscription(channel) {
    if (this.insights) {
      channel.unsubscribe();
    }
  }
  connectToFacebook() {
    this.facebookService.selectPage();
  }
  getAds() {
    this.adsService.getAdsList(1).subscribe(response => {
      this.hasAds = response.total_entries > 0;
    }
    );
  }
  openInfo(): void {
    this.isVisible = true;
  }
  closeInfo() {
    this.isVisible = false;
  }
  requireAds(insight) {
    return (insight === 'Leads' || insight === 'Reach') ? this.hasAds : true;
  }
}
