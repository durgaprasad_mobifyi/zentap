import {
  Component, OnInit, Input, ViewChild,
  ElementRef, Output, EventEmitter, OnDestroy
} from '@angular/core';
import { Content } from 'src/app/vos/content/content';
import { Customer } from 'src/app/vos/customer/customer';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { ConnectFacebookService } from 'src/app/services/connect-facebook/connect-facebook.service';

// import { WebsocketService } from 'src/app/services/sockets/websocket.service';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { ActionCableService, Channel } from 'angular2-actioncable';

import * as FileSaver from 'file-saver';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DownloaderService } from 'src/app/services/downloader.service';
import { GenericDialogComponent } from '../generic-dialog/generic-dialog.component';
import {SocialPostDialogComponent} from '../social-post-dialog/social-post-dialog.component';

@Component({
  selector: 'app-content-card',
  templateUrl: './content-card.component.html',
  styleUrls: ['./content-card.component.scss']
})
export class ContentCardComponent implements OnInit, OnDestroy {
  @ViewChild('captioninput') captioninput: ElementRef;
  subscription: Subscription;
  @Input()
  content: Content = new Content();

  customer: Customer;

  @Input()
  width = 'auto';

  @Input()
  horizontalMargin = '25px';

  @Input()
  verticalMargin = '25px';

  @Input()
  caption_editable = false;

  @Input()
  showMoreMenu = false;

  @Output()
  posted = new EventEmitter<Content>();

  editing = false;
  loading = false;
  initialWidth: string;
  canPost = false;
  elem: ElementRef;
  visible: boolean;
  isSocialMedia: Boolean = false;
  isMarketingVideo: Boolean = false;

  @Output()
  editingChange = new EventEmitter();

  @Output()
  deleted = new EventEmitter<Content>();
  // get canPost(): boolean {
  //   return !this.customer.settings.auto_posting && [ 'ready', 'posting', 'posted' ].includes(this.content.status);
  // }

  get height(): string {
    if (this.width === 'auto') {
      return 'auto';
    } else {
      return '' + parseInt(this.width.replace(/\D/g, ''), 10) * 296 / 282 + 'px';
    }
  }
  constructor(
    public router: Router,
    private authService: AuthenticationService,
    private cableService: ActionCableService,
    private contentService: ContentsService,
    private downloader: DownloaderService,
    private modalService: NzModalService,
    private message: NzMessageService,
    element: ElementRef,
    private connectFacebookService: ConnectFacebookService
  ) {
    this.elem = element;
  }

  ngOnInit() {
    this.content.caption = '';
    this.isSocialMedia = this.router.url.includes('social_media_posts');
    this.isMarketingVideo = this.router.url.includes('marketing_videos');
    this.authService.currentCustomer.subscribe((c) => {
      this.customer = c;
      this.canPost = this.customer ?
        !this.customer.settings.auto_posting && ['ready', 'posting', 'posted', 'promoted'].includes(this.content.status) : false;
      this.initialWidth = this.width;
    });
  }
  showPreview() {
    window.open(this.content.url, 'nil');
  }

  postMarketingVideo() {
    this.loading = true;
    this.editing = false;
    const request = this.contentService.create({
      category: this.content.category,
      caption: this.content.caption,
      style: this.content.style,
      contentable_type: this.content.contentable_type,
      contentable_id: this.content.contentable_id
    });
    request.subscribe(c => {
      this.loading = false;
      const is_template = this.content.is_template;
      // this.content.status = 'rendering';
      // this.caption_editable = false;
      this.message.create('success', 'successfully created, we will notify when its created!');
    });
  }
  post() {
    const modal = this.modalService.create({
      nzContent: SocialPostDialogComponent,
      nzComponentParams: {
        content: this.content
      },
      nzFooter: null
    });
    modal.afterClose.subscribe(response => {
    });
  }
  // post1() {
  //   if (this.isSocialMedia && !this.customer.fb_page_id) {
  //     this.message.create('error', 'Please connect your facebook!');
  //     this.connectFacebookService.linkFB();
  //   } else {
  //     this.loading = true;
  //     this.editing = false;
  //     // const uid = `${this.content.id}_${this.customer.first_name}`;
  //     const uid = `${this.content.id}`;
  //     // if (this.content.id) {
  //     const request = this.contentService.createPost({
  //       'uid': uid,
  //       'post': { 'content_id': this.content.id, 'caption': this.content.caption }
  //     }, 'posts');
  //     request.subscribe(c => {
  //       const caption = this.content.caption;
  //       this.loading = false;
  //       const is_template = this.content.is_template;
  //       this.content = c['content_attributes'];
  //       this.content.caption = caption;
  //       // this.content.status = 'rendering';
  //       this.caption_editable = false;
  //       const channel: Channel = this.cableService.cable('wss://dev-api.universalpromote.com/cable').channel(`PostsChannel`, { uid: uid });
  //       // Subscribe to incoming messages
  //       this.subscription = channel.received().subscribe(message => {
  //         message.status === 'failed' ?
  //           this.message.create('error', 'your post has been failed') :
  //           this.message.create('success', 'your post has been posted Sucessfully');
  //         this.content.status = 'posted';
  //       });
  //       this.message.create('success', 'successfully requested your posting, we will notify when its posted!');
  //       this.posted.emit(c);
  //     });
  //   }
  // }

  create() {
    this.loading = true;
    const request = this.contentService.create({
      category: this.content.category,
      caption: this.content.caption,
      style: this.content.style,
      contentable_type: this.content.contentable_type,
      contentable_id: this.content.contentable_id
    });
    request.subscribe(c => {
      this.loading = false;
      const is_template = this.content.is_template;
      this.content.status = 'rendering';
      this.message.create('success', 'successfully created, we will notify when its created!');
    });
  }


  download() {
    let url = this.content.url;
    if (!url.includes('https')) {
      url = url.replace('http', 'https');
    }
    this.downloader.save(url, this.content.filename);
    // FileSaver.saveAs(url, this.content.filename);
  }

  setEditing(editing: boolean) {
    this.editing = editing;
    setTimeout(() => {
      this.captioninput.nativeElement.focus();
      if (window.scrollY > 0) {
        window.scrollTo(0, 0);
      }
      // this.elem.nativeElement.focus();
    }, 0);
    // this.width = this.editing ? '100%' : this.initialWidth;
    // this.elem.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
    // if (!editing) {
    //   this.updateCaption();
    // }
    // this.editingChange.emit(this.elem);
  }
  // updateCaption() {
  //   this.contentService.update(this.content).subscribe();
  // }
  delete() {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: 'Are you sure you want to delete this content?',
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.loading = true;
        this.contentService.destroy(this.content).subscribe(r => {
          this.deleted.emit(this.content);
          this.loading = false;
        }, e => {
          this.loading = false;
          this.message.create('error', 'Error deleting this content. Please try again');
        });
      }
    });
  }

  ngOnDestroy(): void {
  }

}
