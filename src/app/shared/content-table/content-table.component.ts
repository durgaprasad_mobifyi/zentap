import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CardButtonConfig } from '../../models';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Content } from 'src/app/vos/content/content';
import { Customer } from 'src/app/vos/customer/customer';
import { ContentsService } from '../../services/contents/contents.service';
import { DownloaderService } from 'src/app/services/downloader.service';
import { ContentPreviewDialogComponent } from '../../shared/content-preview-dialog/content-preview-dialog.component';
import { SocialPostDialogComponent } from '../../shared/social-post-dialog/social-post-dialog.component';
import {GenericDialogComponent} from '../../shared/generic-dialog/generic-dialog.component';

@Component({
  selector: 'app-content-table',
  templateUrl: './content-table.component.html',
  styleUrls: ['./content-table.component.scss']
})
export class ContentTableComponent implements OnInit {
  @Input()
  contents: Content[];
  @Input()
  contentType = 'listing';
  @Output()
  deleted = new EventEmitter<Content>();
  loading = true;

  customer: Customer;

  constructor(
    private contentService: ContentsService,
    private downloader: DownloaderService,
    private modalService: NzModalService,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
  }

  contentDeleted(content: Content) {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: `Are you sure you want to delete content?`,
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.loading = true;
        this.contentService.destroy(content)
          .subscribe(res => {
            this.contents = this.contents.filter(c => c.id !== content.id);
            this.message.create('success', `Content has been successfully deleted.`);
            this.deleted.emit(content);
          }, e => {
            this.loading = false;
            this.message.create('error', 'Error deleting the content. Please try again');
          });
      }
    });
  }


  download(content) {
    let url = content.url;
    if (!url.includes('https')) {
      url = url.replace('http', 'https');
    }
    this.downloader.save(url, content.filename);
  }
  post(content) {
    if (content.status === 'ready') {
      const modal = this.modalService.create({
        nzContent: SocialPostDialogComponent,
        nzComponentParams: {
          content
        },
        nzFooter: null
      });
      // modal.afterClose.subscribe(response => {
      // });
    }
  }

  sort(sort: { key: string; value: string }) {
    const sortName = sort.key || 'descend';
    this.contents.sort((a, b) =>
      sort.value === 'ascend'
        ? a[sortName!] > b[sortName!]
          ? 1
          : -1
        : b[sortName!] > a[sortName!]
          ? 1
          : -1
    );
  }
  showPreview(content) {
    const modal = this.modalService.create({
      nzContent: ContentPreviewDialogComponent,
      nzComponentParams: {
        config: {
          content
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      console.log(response);
    });
  }
}
