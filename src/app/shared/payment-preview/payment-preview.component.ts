import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { StripeCoupon } from '../../vos/subscription/subscription';
import { SubscriptionService } from '../../services/subscriptions/subscriptions.service';

@Component({
  selector: 'app-payment-preview',
  templateUrl: './payment-preview.component.html',
  styleUrls: [ './payment-preview.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentPreviewComponent implements OnInit {
  @Input()
  lineItems: { name: string, quantity: number, price: number }[];
  @Input()
  paymentSource: string;
  @Output()
  checkout = new EventEmitter();

  @Input()
  coupon: StripeCoupon;
  @Output()
  couponChange = new EventEmitter<StripeCoupon>();
  couponError: string;
  couponLoading = false;
  constructor(
    private subscriptionService: SubscriptionService
  ) { }

  ngOnInit() {
  }

  total(): number {
    return this.lineItems.reduce((t, i) => {
      return t + (i.quantity * i.price);
    }, 0);
  }
  triggerCheckout() {
    this.checkout.emit(this.total);
  }

  applyCoupon(input: any) {
    const code = input.value;
    if (!code) {
      return;
    }
    this.couponLoading = true;
    this.couponError = null;
    this.subscriptionService.getCoupon(code).subscribe(c => {
      if (c && c.valid) {
        this.coupon = c;
        this.couponChange.emit(c);
        input.value = null;
        this.couponLoading = false;
      }
    }, errorResponse => {
      this.couponError = errorResponse.error.message;
      this.couponLoading = false;
    });
  }

}
