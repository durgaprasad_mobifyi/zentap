import { Component, OnInit, Input } from '@angular/core';
import { Listing } from 'src/app/vos/listing/listing';
import { GenericDialogComponent } from '../generic-dialog/generic-dialog.component';
import { ListingsService } from 'src/app/services/listings/listings.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';
import { BreadCrumService } from '../../services/breadcrum/bread-crum.service';

@Component({
  selector: 'app-listing-preview',
  templateUrl: './listing-preview.component.html',
  styleUrls: ['./listing-preview.component.scss']
})
export class ListingPreviewComponent implements OnInit {
  carouselIndex = 0;
  @Input()
  listing: Listing;
  loading = false;
  previewFields: {
    field: string;
    icon: string;
    fieldType?: string;
    prefix?: string;
    suffix?: string;
  }[] = [
      {
        field: 'price',
        icon: 'fas dollar-sign',
        fieldType: 'currency'
      },
      {
        field: 'listing_type',
        icon: 'fas home',
        fieldType: 'string'
      },
      {
        field: 'listing_status',
        icon: 'fas bullhorn',
        fieldType: 'string'
      },
      {
        field: 'beds',
        icon: 'fas bed',
        fieldType: 'number'
      },
      {
        field: 'baths',
        icon: 'fas bath',
        fieldType: 'number'
      },
      {
        field: 'sq_ft',
        icon: 'fas ruler-combined',
        fieldType: 'number',
        suffix: 'sqft'
      },
      {
        field: 'year_build',
        icon: 'far calendar-alt',
        fieldType: 'string'
      }
    ];
  get editLink(): string {
    return `/listings/${this.listing.id}/edit`;
  }
  constructor(
    private listingService: ListingsService,
    private router: Router,
    private message: NzMessageService,
    private breadcrumService: BreadCrumService,
    private modalService: NzModalService,
  ) { }

  ngOnInit() {
    this.breadcrumService.push_breadcrum({ name: this.listing.address });

  }

  carouselIndexChanged(event) { }

  deleteListing() {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          title: 'Confirm',
          message: 'Are you sure you want to delete this listing?',
          buttonLabel: 'Cancel',
          extraButtons: [
            {
              label: 'Confirm',
              value: true,
              color: 'warn'
            }
          ]
        }
      },
      nzFooter: null,
      nzWidth: '60%'
    });
    modal.afterClose.subscribe(response => {
      if (response === true) {
        this.loading = true;
        this.listingService.destroy(this.listing).subscribe(r => {
          this.router.navigateByUrl('/listings');
          this.listingService.sendClearListingNotification(this.listing);
        }, e => {
          this.loading = false;
          this.message.create('error', 'Error deleting this listing. Please try again');
        });
      }
    });
  }
  isNumber(n) {
    return !isNaN(parseFloat(n)) && !isNaN(n - 0);
  }
}
