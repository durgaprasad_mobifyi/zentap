import {ChangeDetectionStrategy, Component, OnInit, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {NzModalRef} from 'ng-zorro-antd/modal';
declare  const Calendly: any;
@Component({
  selector: 'app-upgrade-dialog',
  templateUrl: './upgrade-dialog.component.html',
  styleUrls: [ './upgrade-dialog.component.scss' ],
  changeDetection: ChangeDetectionStrategy.Default
})

export class UpgradeDialogComponent implements OnInit {
  @ViewChild('container') container: ElementRef;
  constructor(
    private modal: NzModalRef,
  ) {}
  ngOnInit() {
    Calendly.initInlineWidget({
      url: 'https://calendly.com/client-success-/support',
      parentElement: this.container.nativeElement
    });
  }
  removeDialog () {
    this.modal.close();
  }
}
