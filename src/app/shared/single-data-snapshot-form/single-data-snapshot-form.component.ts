import { Component, Input, Output, OnInit } from '@angular/core';
import { ViewChild,EventEmitter } from '@angular/core';
import { ChangeDetectionStrategy,ChangeDetectorRef } from '@angular/core'; 
import { MarketReport } from '../../vos/market-report/market-report';
import { NgForm,ValidationErrors } from '@angular/forms';
import { AbstractControl, FormControl }  from '@angular/forms';
import { FormGroup, FormArray } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { MarketReportsService } from '../../services/market-reports/market-reports.service';

@Component({
  selector: 'app-single-data-snapshot-form',
  templateUrl: './single-data-snapshot-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./single-data-snapshot-form.component.scss']
})

export class SingleDataSnapShotFormComponent implements OnInit {
  @ViewChild('snapShotForm') form: NgForm;
  
  // Output
  @Output() validChange = new EventEmitter<any>();
  @Output() change = new EventEmitter<any>();
  @Output() errors = new EventEmitter<ValidationErrors>();

  // Input
  @Input() valid = false;
  @Input() limitShow = false;
  @Input() model = new MarketReport();
  @Input() contentData = {};
  @Input() extraFields = [];
  @Input() errorStateMatcher: ErrorStateMatcher;
  @Input() lockedFields = [];

  selectedType;
  highlightInvalid = false;
  dataSnapPoints = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  disabled = false;

  constructor(
    private marketReportService: MarketReportsService,
    private notification: NzNotificationService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.fetchDataPoints();
    this.form.valueChanges.subscribe(() => {
      this.validChange.emit(this.selectedType);
      this.valid = this.form.valid;
    });
    this.limitShow = true;
    if (this.contentData['extra_attributes'] && this.contentData['extra_attributes'].data_points.length) {
     this.selectedType = this.contentData['extra_attributes'].data_points[0];
    }
  }

  fetchDataPoints() {
    this.marketReportService.dataSnapPoints().subscribe(res => {
      this.dataSnapPoints = res;
      this.cdRef.detectChanges();
    });
  }
  checkControls(controls: AbstractControl[]) {
    controls.forEach(c => {
      if (c instanceof FormControl) {
        c.markAsTouched();
        c.updateValueAndValidity();
      } else if (c instanceof FormGroup) {
        this.checkControls(Object.keys(c.controls).map(k => c.controls[k]));
      } else if (c instanceof FormArray) {
        this.checkControls(c.controls);
      }
    });
  }
  checkForm() {
    this.highlightInvalid = true;
    this.valid = this.form.valid;
    this.checkControls(Object.keys(this.form.controls).map(k => this.form.controls[k]));
  }
  add(event): void {
    this.change.emit(event);
  }

}
