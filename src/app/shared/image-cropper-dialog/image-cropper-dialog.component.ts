import { Component, Input, OnInit } from '@angular/core';
import { Dimensions, ImageCroppedEvent, ImageTransform } from 'ngx-image-cropper';
import { NzModalRef } from 'ng-zorro-antd';
import { ImagesService } from '../../services/images/images.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { GlobalsService } from 'src/app/services/globals/globals.service';

export class ImageConfig {
  id?: number;
  uid?: number;
  imageableType?: string;
  type?: string;
  order?: number;
  // url?: string;
  // params?: string;
  aspectRatio?: number;
  minWidth?: number;
  minHeight?: number;
  idealWidth?: number;
  idealHeight?: number;
  isNew?: boolean;
  maintainRatio?: boolean;
}
@Component({
  selector: 'app-image-cropper-dialog',
  templateUrl: './image-cropper-dialog.component.html',
  styleUrls: ['./image-cropper-dialog.component.scss']
})
export class ImageCropperDialogComponent implements OnInit {
  @Input() config: ImageConfig;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  canvasRotation = 0;
  aspectRatio = 500;
  maintainAspectRatio = true;
  croppedWidth;
  croppedHeight;
  rotation = 0;
  scale = 1;
  showCropper = false;
  containWithinAspectRatio = false;
  transform: ImageTransform = {};
  btnSelected = false;
  customerId: number;
  imageWidth: number = 100;
  imageHeight: number = 100;
  validImage = true;
  loading = false;
  wrongFile = false;
  fileFormat = 'jpeg';
  uploadIcon = ['fas', 'file-upload'];
  // get maintainAspectRatio () {
  //   return (this.config && this.config.maintainRatio ? this.config.maintainRatio : true);
  // }
  constructor(
    private modal: NzModalRef,
    private imagesService: ImagesService,
    private authService: AuthenticationService,
    private globalService: GlobalsService
  ) { }
  ngOnInit() {
    this.setAspectRatio();
    this.authService.currentCustomer.subscribe(customer => {
      this.customerId = customer.id;
    });
  }
  fileChangeEvent(event: any): void {
    const allowedFiles = ['image/x-png', 'image/png', 'image/jpg', 'image/jpeg']
    const allowed = allowedFiles.indexOf(event.target.files[0].type)
    if (allowed !== -1) {
    this.loading = true;
    this.imageChangedEvent = event;
    this.btnSelected = true;
    const image: any = event.target.files[0];
      const file = new FileReader;
      file.onload = () => { // when file has loaded
        const img = new Image();

        img.onload = () => {
          this.imageWidth = img.width;
          this.imageHeight = img.height;
        };

        const file_string = file.result.toString();
        const file_mime = file_string.split(',')[0].replace(';base64', '');
        const file_extension = file_mime.split('/')[1];

        if (file_extension) { this.fileFormat = file_extension; }

        img.src = file.result as string; // This is the data URL
      };
      file.readAsDataURL(image);
    } else {
      console.log('error');
      this.wrongFile = true;
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
    this.validateImage();
    if (this.validImage) {
      this.showCropper = true;
      this.btnSelected = true;
    } else {
      this.btnSelected = false;
    }
    this.loading = false;
  }

  cropperReady(sourceImageDimensions: Dimensions) {
  }
  validateImage() {
    return this.validImage = this.imageWidth >= this.config.minWidth && this.imageHeight >= this.config.minHeight;
  }

  loadImageFailed() {
    // console.log('Load failed');
  }
  saveImage() {
    this.modal.destroy(this.croppedImage);
    const photoParams = this.imagesService.getPhotoParams(this.config, this.croppedImage);
    if (photoParams.isNew) {
      this.imagesService.createImage(photoParams.params, this.config.type, photoParams.url).subscribe();
    } else {
      this.imagesService.updateImage(photoParams.params, photoParams.url).subscribe();
    }
  }
  cancel() {
    this.modal.close();
  }

  toggleContainWithinAspectRatio() {
    this.containWithinAspectRatio = !this.containWithinAspectRatio;
  }
  setAspectRatio() {
    this.maintainAspectRatio = this.config.maintainRatio !== undefined ? this.config.maintainRatio : true
    this.aspectRatio = this.config.aspectRatio;
    this.croppedWidth = this.config.minWidth;
    this.croppedHeight = this.config.minHeight;
  }
}
