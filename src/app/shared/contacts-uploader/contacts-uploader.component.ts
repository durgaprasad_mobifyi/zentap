import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as Papa from 'papaparse';
import { Observable, Observer } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Contact } from 'src/app/vos/contact/contact';
import { NzMessageService, UploadFile, UploadFilter, NzModalRef } from 'ng-zorro-antd';
import { ContactsService } from 'src/app/services/contacts/contacts.service';
enum UploadState {
  Single = 1,
  Multi = 2,
  ParseHeader = 3,
  Preview = 4
}
@Component({
  selector: 'app-contacts-uploader',
  templateUrl: './contacts-uploader.component.html',
  styleUrls: ['./contacts-uploader.component.scss']
})
export class ContactsUploaderComponent implements OnInit {
  contactPreviewSource: Contact[];
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  loading = false;
  page = 1;
  parseResult: Papa.ParseResult;
  parseHeader: string[];
  parseError: Papa.ParseError;
  submitError: Error;
  csvFile: File;
  uploadState: UploadState;
  contactForm: FormGroup;
  parsedContacts: Contact[];
  filters: UploadFilter[] = [
    {
      name: 'type',
      fn: (fileList: UploadFile[]) => {
        const fileType = fileList[0].name.split('.');
        const fileTypeExt = fileType[fileType.length - 1];
        const filterFiles = fileList.filter(w => w.type.indexOf('text/csv') > -1 || fileTypeExt === 'csv');
        if (filterFiles.length !== fileList.length) {
          this.msg.error(`Invalid file type, you can upload only 'csv' format, please convert your file to csv format before uploading!`);
          return filterFiles;
        }
        return fileList;
      }
    },
    {
      name: 'async',
      fn: (fileList: UploadFile[]) => {
        return new Observable((observer: Observer<UploadFile[]>) => {
          // doing
          observer.next(fileList);
          observer.complete();
        });
      }
    }
  ];

  headerMap = {
    first_name: null,
    last_name: null,
    email: null,
    phone: null,
    company: null,
    address: null,
  };
  formError: Error;
  contactFields = [
    'first_name',
    'last_name',
    'email',
    'phone',
    'company',
    'address',
  ];
  contactFieldsRequired = [
    'first_name',
    'last_name',
    'email',
  ];
  constructor(
    private modal: NzModalRef,
    private fb: FormBuilder,
    private contactsService: ContactsService,
    private cd: ChangeDetectorRef,
    private msg: NzMessageService,
  ) { }

  ngOnInit() {
    this.contactForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      first_name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      phone: [null],
      company: [null],
      address: [null]
    });
  }

  parseCsv = (file): boolean => {
    this.parseError = null;

    Papa.parse(file, {
      dynamicTyping: true,
      complete: (results, f) => {
        this.csvFile = f;
        this.parseHeader = results.data[0];
        this.uploadState = this.parseHeader && !this.parseError ? UploadState.ParseHeader : UploadState.Multi;
      },
      error: (error, _) => {
        console.log(error);

        this.parseError = error;
      },
    });
    return false;
  }

  submitContact() {
    this.loading = true;
    this.contactsService.create(this.contactForm.value).subscribe(c => {
      this.modal.destroy(c);
    },
      (error) => {
        this.formError = error;
        this.loading = false;
      });

  }

  mapHeaders() {
    this.loading = true;
    Papa.parse(this.csvFile, {
      dynamicTyping: true,
      header: true,
      skipEmptyLines: true,
      complete: (results, file) => {
        this.loading = false;
        this.parsedContacts = results.data.map(d => {
          const newContact = {};
          this.contactFields.forEach(f => {
            newContact[f] = d[this.headerMap[f]];
          });
          return new Contact(newContact);
        });
        // this.contactPreviewSource = this.parsedContacts;
        this.setPageData();
        // this.contactPreviewSource.data.length = 5;
        this.uploadState = UploadState.Preview;
        this.cd.detectChanges();
      },
      error: (error, file) => {
        this.loading = false;

        this.parseError = error;
      },
    });
    return false;
  }

  setPageData() {
    const page = this.page - 1;
    this.contactPreviewSource = this.parsedContacts.slice(page * 5, (page + 1) * 5);
  }

  didPage(page: number) {
    this.page = page;
    this.setPageData();
  }
  submitContacts() {
    this.loading = true;
    this.contactsService.createMultiple(this.parsedContacts).subscribe(c => {
      this.modal.destroy(c);
    }, error => {
      this.loading = false;
      this.formError = error;
    });
  }
}
