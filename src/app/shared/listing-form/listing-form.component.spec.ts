import { TestBed, async, fakeAsync, flush, inject, tick, ComponentFixture, } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatIconModule } from '@angular/material';
import { ListingFormComponent } from './listing-form.component';
import { ImagesReorderComponent } from '../images-reorder/images-reorder.component';
import { ImageHandlerComponent } from '../image-handler/image-handler.component';
import { NumberInputFormatterPipe } from '../../pipes/formatting/number-input-formatter.pipe';
import { dispatchKeyboardEvent, dispatchMouseEvent, typeInElement } from 'ng-zorro-antd/core';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ListingsService } from '../../services/listings/listings.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthenticationService } from '../../services/authentication/authentication.service';
import { ContentsService } from '../../services/contents/contents.service';
import { ActionCableService, Channel } from 'angular2-actioncable';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AppComponent } from '../../app.component';
import { DownloaderService } from '../../services/downloader.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DeviceDetectorService } from 'ngx-device-detector';
import { OverlayModule } from '@angular/cdk/overlay';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { JwtService } from '../../services/jwt/jwt.service';
import { WindowService } from '../../services/window/window.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { isSameDay } from 'date-fns';
import * as moment from 'moment';

describe('ListingFormComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListingFormComponent,
        AppComponent,
        ImagesReorderComponent,
        ImageHandlerComponent
      ],
      imports: [
        BrowserAnimationsModule,
        MatIconModule,
        RouterTestingModule,
        HttpClientTestingModule,
        OverlayModule,
        NgZorroAntdModule,
        BrowserModule, FormsModule,
        ServiceWorkerModule.register('', { enabled: false })
      ],
      providers: [
        WindowService,
        JwtService,
        AuthenticationService,
        ListingsService,
        ActionCableService,
        ContentsService,
        DownloaderService,
        NumberInputFormatterPipe,
        NzModalService,
        NzMessageService,
        { provide: NZ_I18N, useValue: en_US }
      ]
    }).compileComponents();
  }));

  it('should create the Listing Form Component', async(() => {
    const fixture = TestBed.createComponent(ListingFormComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  // it(`Open house date shuold be matched with the selection'`, fakeAsync(() => {
  //   const fixture = TestBed.createComponent(ListingFormComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   fixture.detectChanges();
  //   app.showFields = ['open_house'];
  //   fixture.detectChanges();
  //   const el = fixture.debugElement.query(By.css('nz-picker .ant-calendar-picker')).nativeElement as HTMLInputElement;
  //   dispatchMouseEvent(el, 'click');
  //   fixture.detectChanges();
  //   tick(500);
  //   fixture.detectChanges();

  //   // Click to set  today date
  //   const nzOnChange = spyOn(fixture.componentInstance, 'validChange');
  //   const todayPick = fixture.debugElement.query(By.css('.ant-calendar-today-btn')).nativeElement as HTMLInputElement;
  //   dispatchMouseEvent(todayPick, 'click');
  //   tick(500);

  //   // console.log(app.model.oh_from_date, new Date());
  //   const selectedDate = new Date(app.model.oh_from_date);
  //   const today = new Date();
  //   expect(isSameDay(selectedDate, today)).toBeTruthy();
  // }));

  // it(`Open house start time shuold be matched PM with the selection'`, fakeAsync(() => {
  //   const dateFormat = moment;
  //   const fixture = TestBed.createComponent(ListingFormComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   const startTime = '7:15 PM';
  //   fixture.detectChanges();
  //   app.showFields = ['open_house'];
  //   fixture.detectChanges();
  //   const el = fixture.debugElement.query(By.css('#start_time')).nativeElement as HTMLInputElement;
  //   app.model.oh_start_time = startTime;
  //   fixture.detectChanges();
  //   tick(500);
  //   fixture.detectChanges();
  //   app.model.oh_start_time = dateFormat(app.model.oh_start_time , 'h:mm a').format('LT');
  //   fixture.detectChanges();
  //   expect((app.model.oh_start_time)).toEqual(startTime);
  // }));

  // it(`Open house start time shuold be matched AM with the selection'`, fakeAsync(() => {
  //   const dateFormat = moment;
  //   const fixture = TestBed.createComponent(ListingFormComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   const startTime = '7:15 AM';
  //   fixture.detectChanges();
  //   app.showFields = ['open_house'];
  //   fixture.detectChanges();
  //   const el = fixture.debugElement.query(By.css('#start_time')).nativeElement as HTMLInputElement;
  //   app.model.oh_start_time = startTime;
  //   fixture.detectChanges();
  //   tick(500);
  //   fixture.detectChanges();
  //   app.model.oh_start_time = dateFormat(app.model.oh_start_time , 'h:mm a').format('LT');
  //   fixture.detectChanges();
  //   expect((app.model.oh_start_time)).toEqual(startTime);
  // }));

  // it(`Open house end time shuold be matched PM with the selection'`, fakeAsync(() => {
  //   const dateFormat = moment;
  //   const fixture = TestBed.createComponent(ListingFormComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   const endTime = '10:15 PM';
  //   fixture.detectChanges();
  //   app.showFields = ['open_house'];
  //   fixture.detectChanges();
  //   const el = fixture.debugElement.query(By.css('#start_time')).nativeElement as HTMLInputElement;
  //   app.model.oh_end_time = endTime;
  //   fixture.detectChanges();
  //   tick(500);
  //   fixture.detectChanges();
  //   app.model.oh_end_time = dateFormat(app.model.oh_end_time , 'h:mm a').format('LT');
  //   fixture.detectChanges();
  //   expect((app.model.oh_end_time)).toEqual(endTime);
  // }));

  // it(`Open house end time shuold be matched AM with the selection'`, fakeAsync(() => {
  //   const dateFormat = moment;
  //   const fixture = TestBed.createComponent(ListingFormComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   const endTime = '10:15 AM';
  //   fixture.detectChanges();
  //   app.showFields = ['open_house'];
  //   fixture.detectChanges();
  //   const el = fixture.debugElement.query(By.css('#start_time')).nativeElement as HTMLInputElement;
  //   app.model.oh_end_time = endTime;
  //   fixture.detectChanges();
  //   tick(500);
  //   fixture.detectChanges();
  //   app.model.oh_end_time = dateFormat(app.model.oh_end_time , 'h:mm a').format('LT');
  //   fixture.detectChanges();
  //   expect((app.model.oh_end_time)).toEqual(endTime);
  // }));
});
