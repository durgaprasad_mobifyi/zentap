import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  AfterViewChecked
} from '@angular/core';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import { Router, ActivatedRoute } from '@angular/router';
import { Listing } from '../../vos/listing/listing';
import { ListingsService } from 'src/app/services/listings/listings.service';
import {
  NgForm,
  ValidationErrors,
  AbstractControl,
  FormControl,
  FormGroup,
  FormArray
} from '@angular/forms';
import * as moment from 'moment';
import { NumberInputFormatterPipe } from 'src/app/pipes/formatting/number-input-formatter.pipe';
@Component({
  selector: 'app-listing-form',
  templateUrl: './listing-form.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
  styleUrls: ['./listing-form.component.scss']
})
export class ListingFormComponent implements OnInit, AfterViewChecked {
  isOpenHouse = false;
  isCongrats = false;
  isTestimonial = false;
  @ViewChild('listingForm') form: NgForm;
  @Output() validChange = new EventEmitter<boolean>();
  @Input() valid = false;
  @Output() errors = new EventEmitter<ValidationErrors>();
  @Output() requiredChange = new EventEmitter<Array<any>>();
  @Input() requiredFields = [
    'listing_type',
    'transaction_type',
    'address',
    'listing_status',
    'price',
    'beds',
    'baths',
    'images',
    'oh_from_date',
  ];
  @Input() showFields = [
    'address',
    'listing_status',
    'price',
    'sq_ft',
    'beds',
    'baths',
    'images'
  ];
  @Input() limitShow = false;
  @Input() model = new Listing();
  @Input() extraFields = [];

  @Input() lockedFields = [];
  // @Input()
  // disableDynamic = false;
  submitted = false;
  status = [
    'Now Available',
    'Pending',
    'Sold',
    'Just Sold',
    'Off-Market',
    'Under Contract',
    'Coming Soon',
    'Accepting Backups',
    'For Rent',
    'Rented',
    'Just Listed',
    'Still Available',
    'Reduced Price',
    'Purchase Made',
    'LOT Available',
    'Auction',
    'Active',
    'Cancel/Withdrawn',
    'Contingent',
    'Delisted',
    'Pending',
    'Sold',
    'Unknown',
    'Unlisted',
  ];
  property_types = [
    'Custom-Built Home',
    'Single Family Home',
    'Apartment',
    'Townhome',
    'Condominium',
    'Lot',
    'Land',
    'Office',
    'Retail',
    'Industrial',
    'Multi Family',
    'Agriculture',
    'Ranch',
    'Cabin',
    'Stock Cooperative',
    'Duplex',
    'Quadruplex',
    'Timeshare',
    'Triplex',
    'Farmhouse',
  ];

  unitTypes = [
    'acres',
    'sqft'
  ];
  highlightInvalid = false;
  loading = false;
  get minDate(): moment.Moment {
    return moment().add(1, 'd');
  }
  get listingImages(): any[] {
    if (!this.model || (this.model && !this.model.images_attributes)) { return []; }
    const IMAGES = [];
    this.model.images_attributes.forEach((image, i) => {
      if (image) {
        IMAGES[i] = image;
      }
    });
    return IMAGES;
  }
  constructor(
    private listingService: ListingsService,
    private numberPipe: NumberInputFormatterPipe,
    private cdr: ChangeDetectorRef,
    private message: NzMessageService,
    public router: Router,
    private route: ActivatedRoute,
  ) { }
  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
  ngOnInit() {
    this.route.params
      .subscribe(param => {
        this.isOpenHouse = param.form === 'open_house_videos' || param.form === 'open_house_flyers';
        this.isCongrats = param.form === 'congrats_videos';
        this.isTestimonial = param.form === 'testimonial_videos';
      });
    this.form.valueChanges.subscribe(() => {
      this.validChange.emit(this.form.valid);
      this.valid = this.form.valid && this.imagesValid('images');
    });
    this.limitShow = true;
    this.updateVisibleFields();
    this.updateRequiredfields();
  }
  setModelValue(attr: string, value, decimalPlaces) {
    this.model[attr] = this.numberPipe.parse(value, decimalPlaces);
  }
  imageUploaded(event, index) {
    if (index === 'family_photo') {
      this.model.remote_family_photo_url = event['cdnUrl'];
      return;
    }

    if (!this.model.images_attributes) {
      this.model.images_attributes = [];
      this.model.images_attributes.length = 5;
    }
    const imageURL = event['cdnUrl'];
    const imageId = this.model.images[index]
      ? this.model.images[index].id
      : null;
    this.model.images_attributes[index] = {
      id: imageId,
      remote_image_url: imageURL
    };
  }
  checkControls(controls: AbstractControl[]) {
    controls.forEach(c => {
      if (c instanceof FormControl) {
        c.markAsTouched();
        c.updateValueAndValidity();
      } else if (c instanceof FormGroup) {
        this.checkControls(Object.keys(c.controls).map(k => c.controls[k]));
      } else if (c instanceof FormArray) {
        this.checkControls(c.controls);
      }
    });
  }
  checkForm() {
    this.submitted = true;
    this.highlightInvalid = true;
    this.valid = this.form.valid && this.imagesValid('images');
    this.checkNumberInputs();
    // this.form.control.updateValueAndValidity();
    this.checkControls(Object.keys(this.form.controls).map(k => this.form.controls[k]));
    // this.form.controls.forEach(c => {})
  }
  imagesValid(imageType: string): boolean {
    switch (imageType) {
      case 'images':
        return (
          !this.requiredFields.includes('images') ||
          this.listingImages.filter(i=> i && i.id).length >= 5
        );
    }
  }

  changePropertyType(event) {
    this.model.listing_type = event;
    this.updateVisibleFields();
    this.updateRequiredfields();
  }

  changeTransactionType(event) {
    this.model.transaction_type = event;
    this.updateVisibleFields();
  }

  updateRequiredfields() {
    // remove beds and baths for land
    const bathIndex = this.requiredFields.indexOf('baths');
    const bedIndex = this.requiredFields.indexOf('beds');
    if (this.model.listing_type === 'Land' && bathIndex > -1 && bedIndex > -1) {
      this.requiredFields = this.requiredFields.filter(reqFiled => reqFiled !== 'baths');
      this.requiredFields = this.requiredFields.filter(reqFiled => reqFiled !== 'beds');
    } else {
      this.requiredFields.push('beds');
      this.requiredFields.push('baths');
    }
    this.cdr.detectChanges();
    this.requiredChange.emit(this.requiredFields);
  }

  updateVisibleFields() {
    let baseFields = [
      'address',
      'listing_status',
      'listing_type',
      'price',
      'sq_ft',
      'images',
      'lot_size',
      'year_build'
    ];
    let residentialFields = [
      'beds',
      'baths',
      'description'
    ];

    if (this.model.listing_type === 'Land' || this.model.listing_type === 'Lot') {
      residentialFields = ['description'];
      this.model.lot_size_type = 'acres';
      baseFields = [
        'address',
        'listing_status',
        'listing_type',
        'price',
        'images',
        'lot_size',
        'year_build'
      ];
    }

    const commercialFields = [
      'transaction_type',
      'zoning',
      'occupancy',
      'docks'
    ];
    const multiFamilyFields = [
      'units',
      'unit_mix',
      'price_per_unit',
      'grm',
      'pro_forma_cap_rate'
    ];
    const saleFields = [
      'cap_rate',
      'building_class'
    ];
    const leaseFields = [
      'lease_type',
      'lease_rate'
    ];
    if (this.model.listing_type === 'Multi Family') {
      this.lockedFields.push('transaction_type');
      this.model.transaction_type = 'Sale';
    } else {
      this.lockedFields = [];
    }
    let fields = [...baseFields, ...this.extraFields];
    if (this.model.isCommercial) {
      fields = [...fields, ...commercialFields];
      if (this.model.transaction_type === 'Sale') {
        fields = [...fields, ...saleFields];
      } else {
        fields = [...fields, ...leaseFields];
      }
      if (this.model.listing_type === 'Multi Family') {
        fields = [...fields, ...multiFamilyFields];
      }
    } else {
      fields = [...fields, ...residentialFields];
    }
    this.showFields = fields;
    if (this.isOpenHouse) {
      this.showFields.push('open_house');
    }

    if (this.isCongrats) {
      this.showFields.push('congrats');
      this.showFields.push('transaction_type');
    }
  }

  checkNumberInputs() {
    if (this.model.price && this.showFields.indexOf('price') > -1 && isNaN(+this.model.price)) {
      this.message.create('error', 'Price input accepts number only.');
    }
    if (this.model.beds && this.showFields.indexOf('beds') > -1 && isNaN(+this.model.beds)) {
      this.message.create('error', 'Beds input accepts number only.');
    }
    if (this.model.baths && this.showFields.indexOf('baths') > -1 && isNaN(+this.model.baths)) {
      this.message.create('error', 'Baths input accepts number only.');
    }
    if (this.model.year_build && this.showFields.indexOf('year_build') > -1 && isNaN(+this.model.year_build)) {
      this.message.create('error', 'Year build input accepts number only.');
    }
  }
  onKey($event, modelKey) {
    this.model[modelKey] = this.model[modelKey] ? this.model[modelKey].toString() : '';
  }
  get familyPhotoConfig() {
    const target = {
      listing_id: this.model.id
    };
    const config =  {
      id: this.model.id,
      type: 'family_photo',
      aspectRatio: 16 / 9,
      minWidth: 275,
      minHeight: 105,
      idealWidth: 1280,
      idealHeight: 720
    }
    return {
      configMeta: config,
      channel: 'FamilyPhotoChannel',
      target: target,
      image: this.model.family_photo.original,
      isNew: !!this.model.family_photo.original,
    }
  }
}
