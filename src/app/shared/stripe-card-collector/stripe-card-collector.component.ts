import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { BillingsService } from '../../services/billings/billings.service';

declare var Stripe; // : stripe.StripeStatic;
@Component({
  selector: 'app-stripe-card-collector',
  templateUrl: './stripe-card-collector.component.html',
  styleUrls: [ './stripe-card-collector.component.scss' ]
})
export class StripeCardCollectorComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private fb: FormBuilder,
    private billingService: BillingsService,
    private modal: NzModalRef
  ) { }
  @Output()
  createdCard = new EventEmitter<string>();
  @ViewChild('cardElement') cardElement: ElementRef;
  form: FormGroup;
  stripe; // : stripe.Stripe;
  card;
  cardErrors;
  loading = false;
  confirmation;


  ngOnInit() {
    this.stripe = Stripe(environment.stripe_key);
    const elements = this.stripe.elements();

    this.card = elements.create('card', {
      hidePostalCode: true,
      style: {
        base: {
          color: '#303238',
          fontSize: '16px',
          fontFamily: '"Open Sans", sans-serif',
          fontSmoothing: 'antialiased',
          '::placeholder': {
            color: '#CFD7DF',
          },
        },
        invalid: {
          color: '#e5424d',
          ':focus': {
            color: '#303238',
          },
        },
      }
    });
    this.card.mount(this.cardElement.nativeElement);

    this.card.addEventListener('change', ({ error }) => {
      this.cardErrors = error && error.message;
    });
    this.setupForm();
  }

  setupForm() {
    const c = this.authService.currentCustomerValue;
    this.form = this.fb.group({
      name: [ `${c.first_name} ${c.last_name}`, [ Validators.required ] ],
      address_line1: [ '', [ Validators.required ] ],
      address_line2: [ '', [] ],
      address_city: [ c.primary_city, [ Validators.required ] ],
      address_state: [ c.primary_state, [ Validators.required ] ],
      address_zip: [ c.primary_zip, [ Validators.required ] ],
    });
  }
  checkForm() {
    // tslint:disable-next-line: forin
    for (const i in this.form.controls) {
      this.form.controls[ i ].markAsDirty();
      this.form.controls[ i ].updateValueAndValidity();
    }
  }

  async handleForm(e) {
    this.loading = true;
    e.preventDefault();
    this.checkForm();
    if (!this.form.valid) {
      return;
    }
    const value = this.form.value;
    const { token, error } = await this.stripe.createToken(this.card, value);
    if (error) {
      // Inform the customer that there was an error.
      console.log(error);

      this.cardErrors = error.message;
    } else {
      // Send the token to your server.
      this.addCard(token.id);

    }
  }

  addCard(source) {
    this.billingService.createWithoutObjectKey({ source }).subscribe(s => {
      this.modal.destroy(s);
    },
      error => {
        this.loading = false;
        this.cardErrors = error.error.message;
      });
  }

}
