import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Content } from 'src/app/vos/content/content';
import { Customer } from 'src/app/vos/customer/customer';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-select-image-card',
  templateUrl: './select-image-card.component.html',
  styleUrls: [ './select-image-card.component.scss' ]
})
export class SelectImageCardComponent implements OnInit {

  @Input()
  content: Content;

  customer: Customer;

  @Input()
  width = 'auto';

  @Input()
  horizontalMargin = '25px';

  @Input()
  verticalMargin = '25px';

  @Input()
  caption_editable = false;

  @Input()
  disableDeselect = false;

  editing = false;
  loading = false;
  initialWidth: string;
  canPost = false;
  elem: ElementRef;

  @Output() cardClicked = new EventEmitter();

  get height(): string {
    if (this.width === 'auto') {
      return 'auto';
    } else {
      return '' + parseInt(this.width.replace(/\D/g, ''), 10) * 296 / 282 + 'px';
    }
  }
  constructor(
    private authService: AuthenticationService,
    element: ElementRef
  ) {
    this.elem = element;
  }

  ngOnInit() {
    this.authService.currentCustomer.subscribe((c) => this.customer = c);
    // this.canPost = !this.customer.settings.auto_posting && ['ready', 'posting', 'posted'].includes(this.content.status);
    // this.initialWidth = this.width;
  }

  showPreview() {
    window.open(this.content.url, 'nil');
  }

  setContectCard(e: any, item: any) {
    if (e.target && e.target.className && !e.target.className.match(/noselect/g)) {
      if ($(e.target).closest('nz-card.content-card').hasClass('active') && !this.disableDeselect) {
        $('.content-card').removeClass('active');
        $(e.target).closest('nz-card.content-card').removeClass('active');
      } else {
        $('.content-card').removeClass('active');
        $(e.target).closest('nz-card.content-card').addClass('active');
      }
    }

    this.content = item;
    this.cardClicked.emit(item);
  }

}
