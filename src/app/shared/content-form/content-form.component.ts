import { Component, OnInit, Input, ChangeDetectionStrategy, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Content } from 'src/app/vos/content/content';
import { ContentsService } from 'src/app/services/contents/contents.service';
import { NgForm, ValidationErrors, AbstractControl, FormControl, FormGroup, FormArray } from '@angular/forms';
import { ProductStyle } from 'src/app/vos/product/product';
import { GlobalsService } from 'src/app/services/globals/globals.service';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-content-form',
  templateUrl: './content-form.component.html',
  styleUrls: ['./content-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContentFormComponent implements OnInit {
  @ViewChild('contentForm') form: NgForm;
  @Output()
  validChange = new EventEmitter<boolean>();
  @Input()
  valid = false;
  @Output()
  errors = new EventEmitter<ValidationErrors>();
  @Output()
  nextPage = new EventEmitter<any>();
  @Input()
  requiredFields = ['address', 'status', 'photos'];
  @Input()
  onlyRequired = false;
  @Input()
  model: Content = new Content();
  @Input()
  showCaption = true;
  @Input()
  showColors = true;
  submitted = false;
  @Input()
  styles: ProductStyle[] = [];
  // get previewForStyle(): string {
  //   const s = this.styles.find((s) => s.name === this.model.style);
  //   return s ? s.preview : '';
  // }

  stylesExpanded = false;
  contentStyles: Content[];

  colors: string[];
  config: SwiperOptions = {
    // pagination: { el: '.swiper-pagination', clickable: true },
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 10,
    slidesPerView: 3,
    slidesPerGroup: 3,
    // slidesPerView: Math.floor(window.innerWidth / 300),
    // slidesPerGroup: Math.floor(window.innerWidth / 300),
    // loop: true,
    // Responsive breakpoints
    breakpoints: {
      1920: {
        slidesPerView: 4,
        spaceBetween: 10,
        slidesPerGroup: 4
      },
      1700: {
        slidesPerView: 3,
        spaceBetween: 10,
        slidesPerGroup: 3
      },
      1250: {
        slidesPerView: 2,
        spaceBetween: 10,
        slidesPerGroup: 2
      },
      // 1024: {
      //   slidesPerView: 2,
      //   spaceBetween: 10,
      //   slidesPerGroup: 2
      // },
      940: {
        slidesPerView: 1,
        spaceBetween: 30,
        slidesPerGroup: 1
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
        slidesPerGroup: 2
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20,
        slidesPerGroup: 1
      }
    }
    // loopFillGroupWithBlank: true,
  };
  constructor(
    private contentService: ContentsService,
    private cd: ChangeDetectorRef,
    private globalsService: GlobalsService
  ) { }

  ngOnInit() {
    this.colors = this.globalsService.getColors();
    this.form.valueChanges.subscribe(() => {
      this.validChange.emit(this.form.valid);
      this.valid = this.form.valid;
    });
    if (!this.model.style && this.styles.length > 0) {
      this.model.style = this.styles[0].name;
    }
    this.cd.detectChanges();
    this.contentStyles = this.styles.map(s => {
      const url_parts = s.preview.split('.');
      const extension = url_parts[url_parts.length - 1];
      console.log(extension);
      return new Content({
        url: s.preview,
        style: s.name,
        caption: s.name,
        media_type: extension === 'mp4' ? 'video' : 'image',
        is_style: true
      });
    });
  }
  gifloaded() {
    this.cd.detectChanges();
  }
  checkControls(controls: AbstractControl[]) {
    controls.forEach(c => {
      if (c instanceof FormControl) {
        c.markAsTouched();
        c.updateValueAndValidity();
      } else if (c instanceof FormGroup) {
        this.checkControls(Object.keys(c.controls).map(k => c.controls[k]));
      } else if (c instanceof FormArray) {
        this.checkControls(c.controls);
      }
    });
  }
  checkForm() {
    this.valid = this.form.valid;
    // this.form.control.updateValueAndValidity();
    this.checkControls(Object.keys(this.form.controls).map(k => this.form.controls[k]));
    // this.form.controls.forEach(c => {})
  }
  // showPreview() {
  //   window.open(this.previewForStyle, 'nil');
  // }

  selectStyle(content: Content) {
    this.model.style = content.style;
  }

  nextSlide() {
    this.nextPage.emit();
  }
}
