import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {AgmCoreModule} from '@agm/core';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ListingFormComponent} from './listing-form/listing-form.component';
import {ImagesReorderComponent} from './images-reorder/images-reorder.component';
import {CardButtonComponent} from './card-button/card-button.component';
import {ContentCardComponent} from './content-card/content-card.component';
import {ContactsSelectorComponent} from './contacts-selector/contacts-selector.component';
import {MarketReportFormComponent} from './market-report-form/market-report-form.component';
import {ListingsSelectorComponent} from '../shared/listings-selector/listings-selector.component';
import {SingleDataSnapShotFormComponent} from './single-data-snapshot-form/single-data-snapshot-form.component';
import {SelectListingContentComponent} from '../components/listings/select-content/select-content.component';
import { ContentTableComponent } from '../shared/content-table/content-table.component';
import {ListingsComponent} from '../components/listings/listings.component';
import {SupportPopupComponent} from '../shared/support-popup/support-popup.component';

import { ListingsService } from '../services/listings/listings.service';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { ContentsService } from '../services/contents/contents.service';
import { MaterialModule } from './material.module';
import { CustomersService } from '../services/customers/customers.service';
import { MarketReportsService } from '../services/market-reports/market-reports.service';
import { MccColorPickerModule } from 'material-community-components';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { ProductsService } from '../services/products/product-api.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';
import { ContentStatusPipe } from '../pipes/content/content-status.pipe';
import { ContentCreatedPipe } from '../pipes/content/content-created.pipe';
import { ContentTypePipe } from '../pipes/content/content-type.pipe';
import { ContentParentPipe } from '../pipes/content/content-parent.pipe';
import { NumberInputFormatterPipe } from '../pipes/formatting/number-input-formatter.pipe';
import { InfographicsService } from '../services/infographics/infographics.service';
import { LeadsService } from '../services/leads/leads.service';
import { IntegrationsService } from '../services/integrations/integrations.service';
import { AdsService } from '../services/ads/ads.service';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { ContactsService } from '../services/contacts/contacts.service';
import { TemplatesService } from '../services/templates/templates.service';
import { InsightsService } from '../services/insights/insights.service';
import { OnboardingService } from '../services/onboarding/onboarding.service';
import { WelcomePackageService } from '../services/welcome-package/welcome-package.service';
import { SafePipe } from '../pipes/sanitize/sanitize.pipe';
import { PropmixService } from '../services/propmix/propmix.service';
import { BillingsService } from '../services/billings/billings.service';
import { SubscriptionService } from '../services/subscriptions/subscriptions.service';
import { DurationPipe } from '../pipes/time/duration.pipe';
import { ActionCableService } from 'angular2-actioncable';
import { NotificationService } from '../services/notification/notification.service';
import {SettingsService} from '../services/settings/settings.service';
import {PaginationService} from '../services/pagination/pagination.service';
import {TierSelectorComponent} from './tier-selector/tier-selector.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {PaymentSelectorComponent} from './payment-selector/payment-selector.component';
import {StripeCardCollectorComponent} from './stripe-card-collector/stripe-card-collector.component';
import {PaymentPreviewComponent} from './payment-preview/payment-preview.component';
import { WebsiteService } from '../services/websites/website.service';
import {ImagesService} from '../services/images/images.service';
import {ImageCropperDialogComponent} from './image-cropper-dialog/image-cropper-dialog.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import { ImageHandlerComponent } from './image-handler/image-handler.component';
import {ContentFormComponent} from '../shared/content-form/content-form.component';
import {SelectImageCardComponent} from '../shared/select-image-card/select-image-card.component';
import {CustomerFormComponent} from '../shared/customer-form/customer-form.component';
import {SelectContentComponent} from '../shared/select-content/select-content.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MccColorPickerModule,
    NgxHmCarouselModule,
    FontAwesomeModule,
    NgZorroAntdModule,
    NgxUsefulSwiperModule,
    FlexLayoutModule,
    ScrollingModule,
    InfiniteScrollModule,
    ImageCropperModule,
    DragDropModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAzC1SkyTLkuTk0d6Ye7L58Dxbdm2pnpy0',
      libraries: ['places']
    }),
  ],
  declarations: [
    ContentCreatedPipe,
    ContentStatusPipe,
    ContentTypePipe,
    ContentParentPipe,
    NumberInputFormatterPipe,
    SafePipe,
    DurationPipe,
    NumberInputFormatterPipe,
    TierSelectorComponent,
    CheckoutComponent,
    PaymentSelectorComponent,
    StripeCardCollectorComponent,
    PaymentPreviewComponent,
    ListingFormComponent,
    ImagesReorderComponent,
    ImageHandlerComponent,
    CardButtonComponent,
    ContentCardComponent,
    ContactsSelectorComponent,
    MarketReportFormComponent,
    ListingsSelectorComponent,
    // ListingsComponent,
    SingleDataSnapShotFormComponent,
    ImageCropperDialogComponent,
    ContentTableComponent,
    // ListingsComponent,
    ImageCropperDialogComponent,
    SelectImageCardComponent,
    SelectListingContentComponent,
    SelectImageCardComponent,
    SupportPopupComponent,
    ContentFormComponent,
    CustomerFormComponent,
    SelectContentComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MccColorPickerModule,
    NgxHmCarouselModule,
    FontAwesomeModule,
    ContentCreatedPipe,
    ContentStatusPipe,
    ContentTypePipe,
    ContentParentPipe,
    DurationPipe,
    NumberInputFormatterPipe,
    NgZorroAntdModule,
    SafePipe,
    TierSelectorComponent,
    PaymentSelectorComponent,
    StripeCardCollectorComponent,
    PaymentPreviewComponent,
    DragDropModule,
    NgxUsefulSwiperModule,
    FlexLayoutModule,
    ScrollingModule,
    InfiniteScrollModule,
    ListingFormComponent,
    ImagesReorderComponent,
    ImageHandlerComponent,
    CardButtonComponent,
    ContentCardComponent,
    ContactsSelectorComponent,
    MarketReportFormComponent,
    AgmCoreModule,
    ListingsSelectorComponent,
    // ListingsComponent,
    ImageCropperModule,
    ImageCropperDialogComponent,
    SingleDataSnapShotFormComponent,
    ContentTableComponent,
    SelectImageCardComponent,
    SelectListingContentComponent,
    SelectImageCardComponent,
    SupportPopupComponent,
    ContentFormComponent,
    CustomerFormComponent,
    SelectContentComponent
  ],
  providers: [
    AuthenticationService,
    ListingsService,
    ContentsService,
    CustomersService,
    ContentsService,
    MarketReportsService,
    ProductsService,
    InfographicsService,
    LeadsService,
    IntegrationsService,
    AdsService,
    ContactsService,
    TemplatesService,
    PropmixService,
    BillingsService,
    SubscriptionService,
    InsightsService,
    OnboardingService,
    WelcomePackageService,
    ActionCableService,
    NotificationService,
    SettingsService,
    PaginationService,
    WebsiteService,
    ImagesService,
    { provide: NZ_I18N, useValue: en_US }
  ]
})

/**
 * Shares common components, directives, and pipes that
 * may not be as application specific.
 *
 * @see https://angular.io/guide/ngmodule#shared-modules
 */
export class SharedModule {
}
