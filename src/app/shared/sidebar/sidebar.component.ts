import { Component, OnInit, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, NavigationStart } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Customer } from 'src/app/vos/customer/customer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { CustomersService } from 'src/app/services/customers/customers.service';
import { GenericDialogComponent } from '../generic-dialog/generic-dialog.component';
import { OnboardingService } from 'src/app/services/onboarding/onboarding.service';
import { FacebookService } from 'ngx-facebook';
import {Observable} from 'rxjs';
import {Subscription} from '../../vos/subscription/subscription';
import {SubscriptionService} from '../../services/subscriptions/subscriptions.service';
declare const $zopim;


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({ width: '*' })),
      state('false', style({ width: '61px' })),
      transition('false => true', animate('.5s ease-in')),
      transition('true => false', animate('.5s ease-out')),
    ])
  ]
})
export class SidebarComponent implements OnInit, OnDestroy {
  opened = true;
  animating = false;
  isCollapsed = false;
  mobileQuery: MediaQueryList;
  customer: Customer;
  loading = false;
  visible: boolean;
  subscription$: Observable<Subscription>;
  timerStyle = {
    color: 'white',
    fontSize: '18px',
    fontFamily: 'Montserrat'
  };
  private _mobileQueryListener: () => void;
  navItems = [
    {
      title: 'Home',
      path: '/',
      icon: 'home',
      color: '#30d289'
    },
    {
      title: 'Gallery',
      path: '/content',
      icon: 'crown',
      color: '#30d289'
    },
    {
      title: 'Social Media Posts',
      path: '/social_media_posts/new',
      icon: 'like',
      color: '#30d289'
    },
    {
      title: 'Websites',
      path: '/websites',
      icon: 'layout',
      color: '#18a0b2'
    },
    {
      title: 'Promote My Listings',
      path: '/listings',
      icon: 'flag',
      color: '#18a0b2'
    },
    {
      title: 'Local Market Content',
      path: '/market_reports',
      icon: 'area-chart',
      color: '#18a0b2'
    },
    {
      title: 'Contacts',
      path: '/contacts',
      icon: 'solution',
    },
    {
      title: 'Leads',
      path: '/leads',
      icon: 'team',
    }
  ];
  profileMenuItems: { text: string, action: (() => void) | string, icon: string }[] = [
    { text: 'Settings', action: '/settings', icon: 'setting' },
    { text: 'Profile', action: '/profile', icon: 'user' },
    { text: 'Need Help?', action: (() => this.goToLink('https://universalpromote.zendesk.com/hc/en-us')), icon: 'question-circle' },
    { text: 'Upgrade', action: '/upgrade', icon: 'arrow-up' },
    // { text: 'Plan', action: '/plan', icon: 'logout'  },
    { text: 'Logout', action: (() => this.logout()),  icon: 'logout'  }
  ];

  config = {
    paddingAtStart: true,
    interfaceWithRoute: true,
    classname: 'mnav-dropdown',
    listBackgroundColor: '#28484D',
    fontColor: `#FFFFFF`,
    backgroundColor: '#28484D',
    selectedListFontColor: `#FFFFFF`,
    highlightOnSelect: true,
    collapseOnSelect: true,
    rtlLayout: false
  };
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public router: Router,
    private authService: AuthenticationService,
    private customerService: CustomersService,
    private fbService: FacebookService,
    private onboardingService: OnboardingService,
    private modalService: NzModalService,
    private subscriptionsService: SubscriptionService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    // tslint:disable-next-line: deprecation
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.opened = !this.mobileQuery.matches;
    this.fbService.init({
      appId: '210551162312621',
      xfbml: true,
      version: 'v2.8'
    });
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.opened = !this.mobileQuery.matches;
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() {
    // this.getOnbaording();
    this.authService.currentCustomer.subscribe((c) => {
      if (c && c.settings['show_price'] === undefined) {
        c.settings['show_price'] = true;
      }
      this.customer = c;
      if (c) {
        this.subscription$ = this.subscriptionsService.currentSubscription();
      }
    });
    this.routeEvent(this.router);
  }

  getOnbaording() {
    this.onboardingService.loadOnboardingSteps()
    .subscribe(res => {
    });
  }

  goto(path) {
    this.router.navigateByUrl(path);
  }
  toggle() {
    this.opened = !this.opened;
  }
  start() {
    this.animating = true;
    this.tick();
  }

  done() {
    this.animating = false;
  }
  hidePopOver(): void {
    this.visible = false;
  }
  showSuportChat() {
    if (this.customer && $zopim && $zopim.livechat) {
      $zopim.livechat.addTags(this.customer.tier);
      $zopim.livechat.window.show();
    }
  }
  tick() {
    if (this.animating) {
      requestAnimationFrame(() => this.tick());
    }
  }
  goHome() {
    this.router.navigate(['/']);
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['/logout']);
  }
  showHelp() {
    const modal = this.modalService.create({
      nzContent: GenericDialogComponent,
      nzComponentParams: {
        config: {
          content: {
            type: 'video',
            url: 'http://s3-us-west-1.amazonaws.com/universal-promote/TutorialVideo_Final_010.mp4',
            autoplay: true
          },
          hideButtons: true
        }
      },
      nzFooter: null,
      nzWidth: '70%',
      nzClassName: 'video-modal'
    });
  }
  routeEvent(router: Router) {
    router.events.subscribe((e) => {
      if (e instanceof NavigationStart) {
        this.opened = !this.mobileQuery.matches;
      }
    });
  }


  handleMenuAction(action: (() => void) | string) {
    this.visible = false;
    if (action) {
      if (typeof action === 'string') {
        this.router.navigateByUrl(action);
      } else {
        action();
      }
    }
  }

  help() {
    window.open('https://calendly.com/dashboard-discovery/with-amber', 'nil');
  }

  get stepsCompltionPercent(): any {
    return this.onboardingService.stepsCompltionPercent();
  }

  get showWelcome(): boolean {
    return !!(this.customer
      // && this.customer.brokerage
      && (this.customer.logo && this.customer.logo.original)
      && (this.customer.image && this.customer.image.original)
      && this.onboardingService.onboardingSteps
      && this.onboardingService.onboardingSteps.length
      && this.onboardingService.onboardingSteps[0].completed);
  }
  get sidebarWidth() {
    if (this.mobileQuery.matches) {
      return 0;
    } else {
      return 80;
    }
  }
  goToLink(url: string) {
    window.open(url, '_blank');
  }

}
