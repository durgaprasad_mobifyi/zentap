import { Component, OnInit, Input } from '@angular/core';
import { Image } from '../../vos/image/image';
import { NzModalService } from 'ng-zorro-antd';
import { ImageGalleryComponent } from '../image-gallery/image-gallery.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ListingImageGalleryComponent } from '../listing-image-gallery/listing-image-gallery.component';
import { ImageCropperDialogComponent } from '../image-cropper-dialog/image-cropper-dialog.component';
import { ActionCableService, Channel } from 'angular2-actioncable';
import { environment } from '../../../environments/environment';
import { GlobalsService } from '../../services/globals/globals.service';

@Component({
  selector: 'app-images-reorder',
  templateUrl: './images-reorder.component.html',
  styleUrls: ['./images-reorder.component.scss']
})
export class ImagesReorderComponent implements OnInit {
  @Input() imageSize = '960x540 minimum';
  @Input() images: Image[];
  @Input() imageable_type;
  show_gallery = false;
  @Input() content: any;
  imageLoading = false;
  constructor(
    private modalService: NzModalService,
    private cableService: ActionCableService,
    private globalsService: GlobalsService
  ) { }

  ngOnInit() {

    // Set empty images to null and ensure array size is
    // at least 5
    for (let i = 0; i < 5; i++) {
      const uid = `${this.imageable_type}_${this.globalsService.randomString()}`;
      if (!this.images[i]) {
        const image = new Image;
        image.uid = uid;
        image.changed = false;
        this.images[i] = image;
      } else {
        this.images[i].uid = uid;
        this.images[i].changed = false;
      }
    }
    // decide wether to show gallery
    this.show_gallery = this.isCustomer() || this.images.length > 5;
  }

  selectFromGallery(index: number) {
    const configs = {
      nzFooter: null,
      nzComponentParams: {
        config: {
          aspectRatio: '2:1',
          cols: 3,
          gutterSize: '10px',
          instantSelection: true
        },
        images: this.images.slice(5, this.images.length)
      },
      nzWidth: '75%'
    };
    configs['nzContent'] = this.isCustomer() ? ImageGalleryComponent : ListingImageGalleryComponent;
    const modal = this.modalService.create(configs);

    modal.afterClose.subscribe(image => {
      if (image) {
        if (this.isCustomer()) {
          this.uploadStock(image, index);
        } else {
          this.swap(image, index);
        }
      }
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.images, event.previousIndex, event.currentIndex);

    // update indexes
    this.images.map((image, index) => {
      if (image) {
        if (image.order !== index) {
          image.order = index;
          image.changed = true;
        }
      }
    });
  }

  upload(asset, uid) {
    if (!asset) { return; }
    const newImage = <Image>asset;
    const index = this.images.findIndex(x => x.uid === uid);
    newImage.loading = false;
    newImage.order = index;
    newImage.stock_photo_id = null;
    newImage.changed = true;
    this.images[index] = newImage;
  }

  openImageDialog(image) {
    if (image.order){
      const uid = `Customer_${this.globalsService.randomString()}`;
      image.uid = uid; 
      this.images[image.order].uid = uid;
    }
    
    this.setImageLoadingEnd(true);
    const modal = this.modalService.create({
      nzContent: ImageCropperDialogComponent,
      nzComponentParams: {
        config: {
          id: image.id,
          uid: image.uid,
          imageableType: this.imageable_type,
          order: image.order,
          type: 'listing',
          aspectRatio: this.isCustomer() ? 2.83 : 16 / 9,
          minWidth: 275,
          minHeight: 105,
          idealWidth: 1280,
          idealHeight: 720
        }
      },
      nzFooter: null
    });
    // minWidth: this.isCustomer() ? 820 : 960,
    // minHeight: this.isCustomer() ? 312 : 540,
    // idealWidth: this.isCustomer() ? 1640 : 1920,
    // idealHeight: this.isCustomer() ? 624 : 1080
    modal.afterClose.subscribe(response => {
      if (response) {
        image.loading = true;
        this.ImageListener(image.uid);
      } else {
        this.setImageLoadingEnd(false);
      }
    });
  }

  ImageListener(uid) {
    const channel: Channel = this.cableService.cable(environment.wss_url).channel('CreateImageChannel', { uid: uid });
    channel.received().subscribe(response => {
      if (response) {
        const image = JSON.parse(response.data);
        this.upload(image, uid);
      }
      this.setImageLoadingEnd(false);
    });
  }

  isCustomer() {
    return this.imageable_type === 'Customer';
  }

  isListing() {
    return this.imageable_type === 'Listing';
  }

  swap(extra_image, index) {
    const old_image = this.images[index];
    const extra_image_index = this.images.findIndex(image => image.id === extra_image.id);

    // set extra image order
    old_image.order = null;
    extra_image.order = index;

    // mark images as changed
    old_image.changed = true;
    extra_image.order = true;

    // swap images from array
    this.images[index] = extra_image;
    this.images[extra_image_index] = old_image;
  }

  uploadStock(stock_photo, index) {
    const image = this.images[index];

    image.stock_photo_id = stock_photo.id;
    image.thumb = stock_photo.thumb;
    image.original = stock_photo.original;
    image.large = stock_photo.large;
    image.order = index;
    image.changed = true;
  }
  setImageLoadingEnd(imageLoading) {
    this.globalsService.imageLoadEnd(imageLoading);
  }
}
