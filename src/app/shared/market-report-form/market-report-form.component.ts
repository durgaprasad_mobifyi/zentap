import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  EventEmitter,
  ChangeDetectionStrategy
} from '@angular/core';
import { MarketReport } from '../../vos/market-report/market-report';
import {
  NgForm,
  ValidationErrors,
  AbstractControl,
  FormControl,
  FormGroup,
  FormArray
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { MarketReportsService } from '../../services/market-reports/market-reports.service';
@Component({
  selector: 'app-market-report-form',
  templateUrl: './market-report-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./market-report-form.component.scss']
})
export class MarketReportFormComponent implements OnInit {
  @ViewChild('marketReportForm') form: NgForm;
  @Output()
  validChange = new EventEmitter<boolean>();
  @Input()
  valid = false;
  @Output()
  errors = new EventEmitter<ValidationErrors>();
  @Input()
  requiredFields = [
    // 'region_name',
    'zips'
  ];
  @Input()
  showFields = [
    'region_name',
    'zips'
  ];
  @Input()
  limitShow = false;
  @Input()
  model = new MarketReport();
  @Input()
  extraFields = [];

  @Input()
  errorStateMatcher: ErrorStateMatcher;

  @Input()
  lockedFields = [];
  highlightInvalid = false;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  zips = [];
  zipError = true;
  disabled = false;
  maxZips = 3;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  constructor(
    private marketReportService: MarketReportsService,
    private notification: NzNotificationService
  ) { }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => {
      this.validChange.emit(this.form.valid);
      this.valid = this.form.valid;
    });
    this.limitShow = true;
    if (this.model.zips) {
      this.zips = this.model.zips;
    }
    this.checkZips();
  }
  checkControls(controls: AbstractControl[]) {
    controls.forEach(c => {
      if (c instanceof FormControl) {
        c.markAsTouched();
        c.updateValueAndValidity();
      } else if (c instanceof FormGroup) {
        this.checkControls(Object.keys(c.controls).map(k => c.controls[k]));
      } else if (c instanceof FormArray) {
        this.checkControls(c.controls);
      }
    });
  }
  checkForm() {
    this.highlightInvalid = true;
    this.valid = this.form.valid;
    this.checkControls(Object.keys(this.form.controls).map(k => this.form.controls[k]));
  }
  add(event): void {
    if (Array.isArray(event) && event.length) {
      this.zips = event;
      this.model.zips = this.zips;
      this.checkZips();
    }
  }
  remove(zip): void {
    const index = this.model.zips.indexOf(zip);
    if (index >= 0) {
      this.model.zips.splice(index, 1);
    }
  }
  disableZip() {
    if (this.zips.length === this.maxZips) {
      this.disabled = true;
    }
  }

  checkZips() {
    let foundInvalid = false;
    const validZips = [];
    this.zips.map(zp => {
      if (!this.zipValid(zp)) {
        foundInvalid = true;
        this.remove(zp);
      } else {
        validZips.push(zp);
      }
    });
    this.zips = validZips;
    this.model.zips = this.zips;
    if (foundInvalid) {
      this.notification.create(
        'error',
        'Invalid zip code',
        'You have added invalid zip code.'
      );
    }
    this.zipError = this.zips.length === 0 || !this.zips.every(this.marketReportService.is_valid_zip);
    this.disabled = this.zips.length >= this.maxZips;
  }
  zipValid(zip: string): boolean {
    return this.marketReportService.is_valid_zip(zip);

  }
}
