import { Component, OnInit, Input } from '@angular/core';
import {BreadCrumService} from '../../services/breadcrum/bread-crum.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-bread-crum',
  templateUrl: './bread-crum.component.html',
  styleUrls: [ './bread-crum.component.scss' ]
})

export class BreadCrumComponent implements OnInit {
  @Input() breadcrums: BreadCrum[];
  constructor(private router: Router, private route: ActivatedRoute, private breadCrumService: BreadCrumService) {}
  ngOnInit() {
  }

  get breadcrumbs(): any {
    return this.breadCrumService.breadcrumsList;
  }
}

export class BreadCrum {
  name: string;
  url?: string;
  icon?: string;
  tittleClass?: string;
  iconClass?: string;
}
