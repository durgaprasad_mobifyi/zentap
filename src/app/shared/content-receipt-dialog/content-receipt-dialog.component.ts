import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Router } from '@angular/router';
import { DownloaderService } from 'src/app/services/downloader.service';
import { GlobalsService } from '../../services/globals/globals.service';

@Component({
  selector: 'app-content-receipt-dialog',
  templateUrl: './content-receipt-dialog.component.html',
  styleUrls: ['./content-receipt-dialog.component.scss']
})

export class ContentReceiptDialogComponent implements OnInit {
  @Input()
  config: any;

  receipt;
  constructor(
    private globalsService: GlobalsService,
    private modal: NzModalRef,
    private downloader: DownloaderService,
    private modalService: NzModalService,
    private router: Router,
  ) { }
  ngOnInit() {
    this.receipt = this.globalsService.getReceipt()[this.config.type] || this.globalsService.getReceipt()['content'];
  }

  openLink() {
    this.router.navigateByUrl(this.config.link);
    this.modal.close('true');
  }
  close() {
    if ( this.config.overridelink ) {
      this.router.navigateByUrl('/');
    }
    this.modal.close();
  }
}
