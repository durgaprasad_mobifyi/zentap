import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { Customer, Integration } from 'src/app/vos/customer/customer';
import {
  NgForm,
  ValidationErrors,
  AbstractControl,
  FormControl,
  FormGroup,
  FormArray
} from '@angular/forms';
import {Observable} from 'rxjs';
import { PropmixService } from 'src/app/services/propmix/propmix.service';
import { IntegrationsService } from 'src/app/services/integrations/integrations.service';

import { Subject } from 'rxjs';
import { PMMLSBoard } from 'src/app/vos/propmix/propmix';
import { map, first } from 'rxjs/operators';
import { GlobalsService } from 'src/app/services/globals/globals.service';
@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit {
  console = console;
  @ViewChild('customerForm') form: NgForm;
  @Output() validChange = new EventEmitter<boolean>();
  @Output() retry = new EventEmitter<any>();
  @Input() valid = false;
  @Output() errors = new EventEmitter<ValidationErrors>();
  @Input() requiredFields = [
    'first_name',
    'last_name',
    'email',
    'name',
    'brokerage',
    'display_phone',
    'primary_phone',
    'display_email',
    'primary_email',
    'headshot',
    'logo',
    'display_name',
    'preferred_title'
  ];
  @Input() showFields = [
    'name',
    'brokerage',
    'display_phone',
    'primary_phone',
    'display_email',
    'primary_email',
    'headshot',
    'logo',
    'team',
    'images'
  ];
  @Input() limitShow = false;
  @Input() model: Customer;
  submitted = false;
  highlightInvalid = false;
  titles = Array<string>();
  credentials = Array<string>();
  mlsSearchTerm = new Subject<string>();
  mlsBoardResults$: Observable<PMMLSBoard[]>;
  mlsError = '';
  loading = false;

get headshotConfig () {
  const target = {
    customer_id: this.model.id
  };
  const config=  {
    id: this.model.id,
    imageableType: 'Customer',
    type: 'headshot',
    aspectRatio: 1,
    minWidth: 50,
    minHeight: 50,
  }
  return {
    configMeta: config,
    image: this.model ? this.model.image.large : '',
    isNew: !!this.model.image.large,
    channel: 'HeadshotChannel',
    target: target
  }
}
get logoConfig () {
  const target = {
    customer_id: this.model.id
  };
  const config=  {
    id: this.model.id,
    imageableType: 'Customer',
    type: 'logo',
    aspectRatio: 1,
    minWidth: 50,
    minHeight: 50,
    maintainRatio: false
  }
  return {
    configMeta: config,
    image: this.model ? this.model.logo.large : '',
    isNew: !!this.model.logo.large,
    channel: 'LogoChannel',
    target: target
  }
}
  constructor(private propmixService: PropmixService,
    private integrationsService: IntegrationsService,
    private globalsService: GlobalsService) {
    this.credentials = this.globalsService.getCredentials();
    this.titles = this.globalsService.getTitles();
    this.mlsBoardResults$ = this.propmixService.searchMLSTerm(this.mlsSearchTerm);
    this.getIntegrations();
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => {
      this.validChange.emit(this.form.valid);
      this.valid =
        this.form.valid &&
        this.imgaesValid('image') &&
        this.imgaesValid('logo') &&
        this.imgaesValid('banner_images');
    });
  }

  hitRetry() {
    this.retry.emit(true);
  }

  setDisplayName(conn: Integration) {
    this.propmixService.mlsBoardForId(conn.user_id_on_provider.trim())
      .pipe(first())
      .subscribe(b => {
        conn.display_name = b ? b.MLSDescription : null;
      });
  }
  checkControls(controls: AbstractControl[]) {
    controls.forEach(c => {
      if (c instanceof FormControl) {
        c.markAsTouched();
        c.updateValueAndValidity();
      } else if (c instanceof FormGroup) {
        this.checkControls(Object.keys(c.controls).map(k => c.controls[k]));
      } else if (c instanceof FormArray) {
        this.checkControls(c.controls);
      }
    });
  }

  setMLSSearchTerm(term: string, index: number) {
    this.mlsSearchTerm.next(term);
    this.model.mls_connections[index].display_name = null;
  }
  checkForm() {
    this.highlightInvalid = true;
    this.valid =
      this.form.valid &&
      this.imgaesValid('image') &&
      this.imgaesValid('logo') &&
      this.imgaesValid('banner_images');
    // this.form.control.updateValueAndValidity();
    this.checkControls(Object.keys(this.form.controls).map(k => this.form.controls[k]));
    // this.form.controls.forEach(c => {})
  }
  imageUploaded(event, field) {
    const imageURL = event['cdnUrl'];
    this.model['remote_' + field + '_url'] = imageURL;
  }


  imgaesValid(imageType: string): boolean {
    switch (imageType) {
      case 'image':
      case 'logo':
        return (
          !this.requiredFields.includes(imageType) ||
          this.model[imageType].large ||
          this.model['remote_' + imageType + '_url']
        );
      case 'banner_images':
        return (
          !this.requiredFields.includes('banner_images') ||
          this.model.images_attributes.filter(i => (i.id || i.stock_photo_id)).length >= 5
        );
    }
  }
  bannerUploaded(event, index) {
    if (!this.model.images_attributes) {
      this.model.images_attributes = [];
      this.model.images_attributes.length = 5;
    }
    const imageURL = event['cdnUrl'];
    const imageId = this.model.images[index]
      ? this.model.images[index].id
      : null;
    this.model.images_attributes[index] = {
      id: imageId,
      remote_image_url: imageURL
    };
  }

  addMLSConnection() {
    if (this.model.brokerage
      && this.model.brokerage.length > 0
      && this.model.license_number
      && this.model.license_number.length > 0) {
      this.mlsError = '';
      const newInteg = new Integration({
        provider: 'mls',
        status: 'pending',
        token: '',
        user_id_on_provider: null,
        display_name: '',
        meta_data: { feed_id: null }
      });
      this.model.integrations_attributes.push(newInteg);
    } else {
      this.mlsError = 'Please add your brokerage and license number to be able to connect your MLS.';
    }

  }

  getIntegrations() {
    this.integrationsService.list().subscribe(res => {
      this.model.integrations_attributes = res.data;
      this.model.integrations_attributes.map(async intg => {
       if (intg['user_id_on_provider'] && !isNaN(intg['user_id_on_provider'])) {
          return this.propmixService.mlsBoardForFeedId(intg.user_id_on_provider)
            .subscribe(brd => {
              intg.user_id_on_provider = brd.MLSId;
              return intg;
            });
        } else {
          return intg;
        }
      });
    });
  }

  deleteIntegration(integration) {
    const payload = {
      id: integration.id,
    };
    this.integrationsService.destroy(payload).subscribe(res => {
      this.getIntegrations();
    });
  }

  updateIntegration(integration) {
    const payload = {
      id: integration.id,
      meta_data: integration.meta_data,
      user_id_on_provider: integration.user_id_on_provider,
      token: integration.token
    };
    this.integrationsService.update(payload).subscribe(res => {
      this.getIntegrations();
    });
  }

  createIntegration(integration) {
    const paylaod = {
      provider: 'mls',
      meta_data: integration.meta_data,
      user_id_on_provider: integration.user_id_on_provider,
      token: integration.token
    };
    this.integrationsService.create(paylaod).subscribe(res => {
      integration.id = res.id;
    });
  }

  onChangeMlsConnection(integrationIndex, $event) {
    const integration = this.model.mls_connections[integrationIndex];
    this.propmixService.mlsBoardForFeedId($event)
    .subscribe(brd => {
      integration.user_id_on_provider = brd.MLSId;
    });
  }

  submitMls(integration) {
    this.submitted = true;
    const isDataOk = integration.token && integration.user_id_on_provider;
    if (integration.id && isDataOk) {
      this.updateIntegration(integration);
    } else if (isDataOk) {
      this.createIntegration(integration);
    }
    this.submitted = false;
  }

  mlsBoardFeedId(mlsId: string): Observable<any> {
    return this.propmixService
      .mlsBoardForId(mlsId)
      .pipe(map(n => n ? n.FeedId : null));
  }

  mlsBoardDisplayName(mlsId: string): Observable<string> {
    return this.propmixService
      .mlsBoardForId(mlsId)
      .pipe(map(n => n ? n.MLSDescription : 'MLS Details'));
  }

  deleteMLS(conn: Integration) {
    if (!conn.id) {
      this.model.integrations_attributes.pop();
    } else {
      this.deleteIntegration(conn);
      conn._destroy = 1;
    }

  }
  setLogo (object) {
     this.model.logo.large = object.image.logo;
  }
  setHeadshot(object) {
    this.model.image.large = object.image.headshot;
  }
}
