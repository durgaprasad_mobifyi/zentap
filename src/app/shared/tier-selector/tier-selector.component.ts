import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tier, Plan } from 'src/app/vos/tier/tier';
import * as moment from 'moment';
import { Product } from '../../vos/product/product';

export type BillingPeriod = 'mo' | 'yr';

@Component({
  selector: 'app-tier-selector',
  templateUrl: './tier-selector.component.html',
  styleUrls: [ './tier-selector.component.scss' ]
})
export class TierSelectorComponent implements OnInit {
  @Input() billingPeriod: BillingPeriod = 'mo';
  @Input() tiers: Tier[];
  @Input() currentPlan: string;
  @Input() cancelAtEnd: moment.Moment;
  @Output() selectedPlan = new EventEmitter<Plan>();
  headerColors = [
    '#00c6b7',
    '#29abe2',
    '#e02b45'
  ];
  constructor() { }

  ngOnInit() {
  }

  productsForIndex(index: number): Product[] {
    let products: Product[] = [];
    this.tiers.slice(0, index + 1)
      .forEach(t => {
        products = products.concat(t.products);
      });
    return products;
  }

  selectTier(tier: Tier) {
    this.selectedPlan.emit(tier.planFor(this.billingPeriod));
  }

  isNew(product: Product, idx: number): boolean {
    return idx === 0 || !this.tiers[ idx - 1 ].products.map(p => p.name).includes(product.name);
  }
}
