import { Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  constructor(private http: HttpClient) { }

  notifySlack(slackObject: object) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post
      (`https://${window.location.host}/error`,
        JSON.stringify(slackObject),
        { headers: headers });
  }
  errorMessage(error: any): string {
    if (error instanceof HttpErrorResponse) {
      return `There was an HTTP error.${error.message} Status code: ${(<HttpErrorResponse>error).status}`;
    } else if (error instanceof TypeError) {
      return `There was a Type error. ${error.message}`;
    } else if (error instanceof Error) {
      return `There was a general error. ${error.message}`;
    } else {
      return `Nobody threw an error but something happened! ${error}`;
    }
  }

}
