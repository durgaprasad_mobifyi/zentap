import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Notification } from 'src/app/vos/notification/notification';
import { CustomerResourceService } from '../customer-resource.service';
import { AuthenticationService } from '../authentication/authentication.service';
import {map} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';

/**
 * Config class to be wired into an injector.
 * @see CoreModule#forRoot
 * @see https://angular.io/guide/dependency-injection#optional-dependencies
 */
export class NotificationServiceConfig {
  uri = '';
}

@Injectable()
/**
 * Service class.
 */
export class NotificationService extends CustomerResourceService<Notification> {

  public endpoint = 'notifications';
  public data_key = 'notification';
  private subject = new Subject<any>();
  constructor(http: HttpClient, authService: AuthenticationService) {
    super(Notification, http, authService);
  }
  mark_read(id: number) {
    return this.http
      .post(
        `${this._customerURI}${this.customerEndpoint}/${id}/open`,
        { headers: this.headers }
      )
      .pipe(map(resp => resp));
  }
  mark_read_all() {
    return this.http
      .post(
        `${this._customerURI}${this.customerEndpoint}/open_all`,
        { headers: this.headers }
      )
      .pipe(map(resp => resp));
  }
  clear_notification(): Observable<any> {
    return this.subject.asObservable();
  }
  sendClearNotification(data) {
    this.subject.next(data);
  }
}
