import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AdsService } from './ads.service';
import { map } from 'rxjs/operators';
import { Ad } from 'src/app/vos/ads/ads';

@Injectable({
  providedIn: 'root'
})
export class AdsResolverService implements Resolve<Ad> {

  constructor(
    private adsService: AdsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ) {
    const listing = route.params[ 'id' ];
    return this.adsService.show(listing).pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
