import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../../environments/environment';
import { DataResponse } from 'src/app/models/data-response/data-response';
import { Customer, Integration } from 'src/app/vos/customer/customer';
import { CustomerResourceService } from '../customer-resource.service';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';

/**
 * Config class to be wired into an injector.
 * @see CoreModule#forRoot
 * @see https://angular.io/guide/dependency-injection#optional-dependencies
 */
export class CustomersServiceConfig {
  uri = `${environment.api_url}`;
}

@Injectable()

export class CustomersService extends CustomerResourceService<Customer> {
  public endpoint = 'customers';
  public data_key = 'customer';
  integration;

  constructor(http: HttpClient, authService: AuthenticationService) {
    super(Customer, http, authService);
  }
  update(customer: Customer): Observable<Customer> {
    return super.update(customer).pipe(
      map(c => {
        this.authService.updateCurrentCustomer(c);
        return c;
      })
    );
  }
  checkCustomer(): Observable<boolean> {
    if (!this.authService.currentCustomerValue) {
      return of(false);
    }
    return this.show(this.authService.currentCustomerValue.id).pipe(
      map(resp => {
        this.authService.updateCurrentCustomer(resp.data);
        return resp.data !== undefined && resp.data !== null;
      })
    );
  }
  refreshCurrentCustomer(): void {
    this.show(this.authService.currentCustomerValue.id).subscribe(resp => {
      this.authService.updateCurrentCustomer(resp.data);
    });
  }

  linkFB(access_token: string): Observable<Customer> {
    return this.http
      .put<DataResponse<Customer>>(`${this._uri}${this.customerEndpointFor('facebook')}.json`, JSON.stringify({ access_token }), {
        headers: this.headers
      })
      .pipe(map(resp => new Customer(resp.data)));
  }
  linkLN(value: any): Observable< Customer > {
     this.authService.currentCustomer.subscribe((c) => {
      this.integration = c.integrations_attributes.filter((i) => i.provider === 'linkedin');
    });
    if (this.integration.length > 0 && this.integration[0].expired) {
      return this.http
        .put<DataResponse<any>>(`${this._uri}linkedin/${this.integration[0].id}.json`,
          JSON.stringify(value),
          {headers: this.headers})
        .pipe(map(resp => (resp.data)));
    } else {
      return this.http
        .post<DataResponse<any>>(`${this._uri}${this.customerEndpointFor('linkedin')}`,
          JSON.stringify(value),
          {headers: this.headers})
        .pipe(map(resp => (resp.data)));
    }
  }
  requestPageAccess(pageId: string): Observable<any> {
    return this.http
      .post<DataResponse<any>>(`${this._uri}${this.customerEndpointFor('facebook_page')}.json`, JSON.stringify({ fb_page_id: pageId }), {
        headers: this.headers
      })
      .pipe(map(resp => (resp.data)));
  }
  checkFBAccess(): Observable<any> {
    return this.http
      .get<DataResponse<any>>(`${this._uri}${this.customerEndpointFor('check_fb_access')}.json`)
      .pipe(map(resp => (resp.data)));
  }
  deleteIntegration(integration: Integration): Observable<void> {
    return this.http
      .delete<void>(`${this._uri}integrations/${integration.id}.json`, {
        headers: this.headers
      });
  }
}
