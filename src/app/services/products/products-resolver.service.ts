import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ProductsService } from './product-api.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsResolverService {
  constructor(
    private productService: ProductsService
  ) { }

  resolve( route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return true;
  }
}
