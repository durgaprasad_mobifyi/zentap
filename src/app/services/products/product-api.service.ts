import { Injectable } from '@angular/core';
import { Product, ProductStyle } from '../../vos/product/product';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { DataResponse } from '../../models/data-response/data-response';
import { map } from 'rxjs/operators';
import { Resource } from 'src/app/vos/resource/resource';
import { Tier } from 'src/app/vos/tier/tier';
import { CustomerResourceService } from '../customer-resource.service';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends CustomerResourceService<Product> {
  private productsSubject: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
  public get products(): Product[] {
    return this.productsSubject.value;
  }

  /**
   * Path uri.
   * @type {string}
   * @private
   */

  /**
   * Url to endpoint api.
   * @type {string}
   */

  /**
   * Endpoint request headers.
   * @type {HttpHeaders}
   */


  /**
   * Component constructor and DI injection point.
   * @param {HttpClient} http
   *
   */
  constructor(protected http: HttpClient, authService: AuthenticationService) {
    super(Product, http, authService);
  }

  public productList(url: string, parameters?: Record<string, any>): Observable<any> {
    return super.listFrom(url, parameters).pipe(
      map( resp => {
        return resp;
      })
    );
  }

  public product(name: string): Product {
    return this.products.find((prod) => prod.name === name);
  }

  public styles(product: Product, resource: Resource, page?): Observable<any> {
    page = page || 1;

    let tags_query = '';
    if (resource.tags) {
      tags_query = resource.tags.split(',').map(function (tag_name) {
        return `tags_names[]=${tag_name}`;
      }).join('&');
    }

    return this.http.get<DataResponse<ProductStyle[]>>(
      `${this._uri}products/${product.id}/styles.json?${tags_query}&page=${page}`).pipe(map((resp) => {
        return resp;
      }));
  }

  public tiers(): Observable<Tier[]> {
    return this.http.get<DataResponse<Tier[]>>(`${this._uri}products/tiers.json`).pipe(map((resp) => {
      if (resp) {
        return resp.data.map((tier) => new Tier(tier));
      }
    }));

  }

}
