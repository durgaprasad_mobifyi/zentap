import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class DownloaderService {

  constructor(
    protected http: HttpClient
  ) { }

  save(url: string, filename: string) {

    this.http.get(url,
      {
        headers: { Accept: 'application/json' },
        observe: 'response',
        responseType: 'blob',
        params: { cachebuster: `${Math.random() * 999999}` }
      }).subscribe(r => {
        this.saveToSystem(r, filename);
      });
  }

  private saveToSystem(response: HttpResponse<any>, filename: string) {
    const contentType = response.headers.get('Content-Type');
    const contentDispositionHeader: string = response.headers.get('Content-Disposition');
    const blob = new Blob([ response.body ], { type: contentType });
    saveAs(blob, filename);
  }
}
