import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Listing } from '../../vos/listing/listing';
import { ListingsService } from './listings.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ListingResolverService implements Resolve<Listing> {

  constructor(
    private listingService: ListingsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ) {
    const listing = route.params['id'];
    return this.listingService.show(listing).pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
