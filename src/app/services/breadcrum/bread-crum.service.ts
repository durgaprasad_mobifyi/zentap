import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BreadCrum} from '../../shared/bread-crum/bread-crum.component';

@Injectable({
  providedIn: 'root',
})
export class BreadCrumService {

  public breadcrumsList: Array<BreadCrum>;
  constructor(private router: Router, private route: ActivatedRoute) {
    this.breadcrumsList = new Array<BreadCrum>();
  }
  get_breadcrum(): BreadCrum[] {
    return this.breadcrumsList;
  }
  public set_breadcrum() {
    const router_url = this.router.url;
    const router_url_hash = router_url.split('/');
    switch (router_url_hash[1]) {
      case 'content': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Content', url: '/content'}];
        break;
      }
      case 'profile': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Profile'}];
        break;
      }
     case 'plan': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Plan'}];
        break;
      }
      case 'gallery': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Gallery'}];
        break;
      }
      case 'ads': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Ads'}];
        break;
      }
      case 'contacts': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Contacts'}];
        break;
      }
      case 'leads': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Leads'}];
        break;
      }
      case 'insights': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Email Insights'}];
        break;
      }
      case 'templates': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Templates', url: '/templates'}];
        break;
      }
      case 'email_campaigns': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'New Email Campaign'}];
        break;
      }
      case 'social_media_posts': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Socal Media Posts'}];
        break;
      }
      case 'market_reports': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Market Reports'}];
        break;
      }
      case 'listings': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Listings', url: '/listings'}];
        break;
      }
      case 'websites': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Websites'}];
        if (router_url === '/websites/new') {
          this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Websites', url: '/websites'}, {name: 'New'}];
        }
        break;
      }
      case 'websites/new': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Websites', url: '/websites'}, {name: 'New'}];
        break;
      }
      case 'onboarding': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Onboarding'}];
        break;
      }
      case 'select_resource': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Select Resource'}];
        break;
      }
      case 'additional_services': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Additional Services'}];
        break;
      }
      case 'notifications': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Notifications'}];
        break;
      }
      case 'settings': {
        this.breadcrumsList = [{name: 'Home', url: '/'}, {name: 'Settings'}];
        break;
      }
      default: {
        this.breadcrumsList = [{name: 'Home', url: '/', icon: 'home', iconClass: 'bread-crum-icon',
          tittleClass: 'breadcrum-tittle'}];
        break;
      }
    }
    return this.breadcrumsList;
  }
  public push_breadcrum(breadcrum) {
    this.set_breadcrum();
    this.breadcrumsList.push(breadcrum);
  }
}
