import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BillingSource } from 'src/app/vos/billing/billing';
import { AuthenticationService } from '../authentication/authentication.service';
import { CustomerResourceService } from '../customer-resource.service';

/**
 * Config class to be wired into an injector.
 * @see CoreModule#forRoot
 * @see https://angular.io/guide/dependency-injection#optional-dependencies
 */
export class BillingsServiceConfig {
  uri = '';
}

@Injectable()
/**
 * Service class.
 */
export class BillingsService extends CustomerResourceService<BillingSource> {
  public endpoint = 'billing';
  public data_key = 'billing';

  constructor(http: HttpClient, authService: AuthenticationService) {
    super(BillingSource, http, authService);
  }
}
