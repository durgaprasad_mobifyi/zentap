import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GlobalsService {
  private credentials = [
    'ABR®',
    'ALC',
    'RLI',
    'CCIM',
    'CDPE',
    'CIPS',
    'CPM®',
    'CRB',
    'CRS',
    'CRE®',
    'GAA',
    'GREEN',
    'GRI',
    'PMN',
    'RCE',
    'RAA',
    'SRS',
    'SIOR',
    'SRES®',
    'AHWD',
    'BPOR',
    'e-PRO®',
    'MRP',
    'PSA',
    'RENE',
    'RSPS',
    'SFRS®',
    'MilRES',
    'CNE®',
    'ALHS',
    'CLHMS',
    'CRP',
    'ABR',
    'PPS',
    'PMN',
    'CRMS',
    'CBR®',
    'CIRE',
    'RELO',
    'Cx',
    'MBA',
    'LLB',
    'LLM'
  ];

  private colors: string[] = [
    '#FF6633',
    '#FFB399',
    '#FF33FF',
    '#FFFF99',
    '#00B3E6',
    '#E6B333',
    '#3366E6',
    '#999966',
    '#99FF99',
    '#B34D4D',
    '#80B300',
    '#809900',
    '#E6B3B3',
    '#6680B3',
    '#66991A',
    '#FF99E6',
    '#CCFF1A',
    '#FF1A66',
    '#E6331A',
    '#33FFCC',
    '#66994D',
    '#B366CC',
    '#4D8000',
    '#B33300',
    '#CC80CC',
    '#66664D',
    '#991AFF',
    '#E666FF',
    '#4DB3FF',
    '#1AB399',
    '#E666B3',
    '#33991A',
    '#CC9999',
    '#B3B31A',
    '#00E680',
    '#4D8066',
    '#809980',
    '#E6FF80',
    '#1AFF33',
    '#999933',
    '#FF3380',
    '#CCCC00',
    '#66E64D',
    '#4D80CC',
    '#9900B3',
    '#E64D66',
    '#4DB380',
    '#FF4D4D',
    '#99E6E6',
    '#6666FF',
  ];
  private titles = [
    'Realtor®',
    'REALTOR®',
    'REALTORS®',
    'Associate Broker',
    'Broker',
    'Brokers',
    'Broker/Owner',
    'Broker/Realtors',
    'Broker Associate',
    'Executive Broker',
    'Executive Assistant',
    'Homeowner Advocate',
    'Licensed Real Estate ',
    'Managing Broker',
    'Mortgage Broker',
    'Salesperson',
    'Real Estate Professional',
    'Real Estate Agent',
    'Real Estate Broker',
    'Real Estate Salesperson',
    'Real Estate Consultant',
    'Real Estate Associate Broker',
    'Real Estate Advisor',
    'Property Manager',
    'President CEO',
    'Team Leader',
    'Investor'
  ];
  private swiperOptions = {
    slidesPerView: 7,
    initialSlide: 0,
    spaceBetween: 8,
    slidesPerGroup: 7,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    breakpoints: {
      1600: {
        slidesPerView: 6,
        spaceBetween: 10,
        slidesPerGroup: 6
      },
      1400: {
        slidesPerView: 5,
        spaceBetween: 10,
        slidesPerGroup: 5
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 10,
        slidesPerGroup: 4
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10,
        slidesPerGroup: 3
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 30,
        slidesPerGroup: 2
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20,
        slidesPerGroup: 1
      }
    }
  };
  private phoneNumber = '(310) 997-4142';
  private email = 'support@zentap.com';
  private newCardConfig = {
    id: 'new',
    title: 'CREATE',
    actionText: '',
    icon: 'fas plus',
    link: '',
    colors: {
      bg: '#f4faff',
      text: '#326771',
      textSelected: '#f4faff'
    },
    queryParams: {
      name: ''
    }
  };

  private contentTabs = [
    { name: 'Listings', type: 'Listing', display: 'Listing' },
    { name: 'Marketing', type: 'Customer', display: 'Marketing' },
    // { name: 'Banner', type: 'Banner',  display: 'Banner'},
    { name: 'Market Reports', type: 'MarketReport', display: 'Market Report' }];

  private receiptCopies = {
    'local_market_videos': {
      header: `Your video is on it’s way!`,
      caption: `Make more content, or view this market report details by clicking 'View Details'.`,
      image: `content.png`,
      close: 'Maybe Later',
      linkText: 'View Details'
    },
    'single_data_snapshot': {
      header: `Your flyer is on it’s way!`,
      caption: `Make more content, or view this market's insights by clicking 'View Details'.`,
      image: `content.png`,
      close: 'Maybe Later',
      linkText: 'View Details'
    },
    'email_newsletter': {
      header: `Your Email Newsletter is on!`,
      caption: `Congrats! Your email will be sent out once a month on the day & time you specified. To view email details, click View Details.`,
      image: `mail.png`,
      close: 'Maybe Later',
      linkText: 'View Details'
    },
    'listing_video': {
      header: `Your video is on it’s way!`,
      caption: `Make more content, or view this listing’s details by clicking 'View Details'.`,
      image: `content.png`,
      close: 'Maybe Later',
      linkText: 'View Details'
    },
    'listing_image': {
      header: `Your flyer is on it’s way!`,
      caption: `Make more content, or view this listing’s details by clicking 'View Details'.`,
      image: `content.png`,
      close: 'Maybe Later',
      linkText: 'View Details'
    },
    'marketing_video': {
      header: `Your video is on it’s way!`,
      caption: `Make more content, or close this window by clicking 'Close Window'.`,
      image: `content.png`,
      close: 'Maybe Later',
      linkText: 'View Details'
    },
    'website': {
      header: `Your website is ready to go!`,
      caption: `Make another website, or close this window clicking 'Close Window'.`,
      image: `content.png`,
      close: 'Close Window',
      linkText: 'Make More'
    },
    'content': {
      header: `Your video is on it’s way!`,
      caption: `Make more content, or close this window by clicking 'Close Window'.`,
      image: `content.png`,
      close: 'Close Window',
      linkText: 'Make More',
    },
  };
  private imagePending = false;
  // private imageType; // Image Type , FILE: image-handler.component.ts[string]
  // private content; // Image Handler Content FILE: image-handler.component.ts[object]
  // private config; // Image Cropper Config FILE: image-cropper.component.ts[object]

  constructor() { }

  getCredentials(): string[] {
    return this.credentials;
  }
  getTitles(): string[] {
    return this.titles;
  }
  getPhoneNumber(): string {
    return this.phoneNumber;
  }
  getEmail(): string {
    return this.email;
  }
  getNewCard(type, product) {
    this.newCardConfig.link = type === 'listing' ? 'listings/new' : 'market_reports/new';
    this.newCardConfig.actionText = `NEW ${this.normalizeTitle(type)}`;
    this.newCardConfig.queryParams.name = product;
    return this.newCardConfig;
  }
  getContentTabs() {
    return this.contentTabs;
  }
  normalizeTitle(name) {
    return name
      .split('_')
      .join(' ')
      .capitalizeAll();
  }
  getSwiperOptions() {
    return this.swiperOptions;
  }

  getReceipt() {
    return this.receiptCopies;
  }

  getColors() {
    return this.colors;
  }
  randomString() {
    let result = '';
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 10; i > 0; --i) { result += chars[Math.round(Math.random() * (chars.length - 1))]; }
    return result;
  }
  imageLoaded() {
    return this.imagePending;
  }
  imageLoadEnd(status) {
    this.imagePending = status;
  }

}
