import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LoggerService } from './logger.service';
import { AuthenticationService } from './authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandler extends ErrorHandler {
  private sentencesForWarningLogging: string[] = [];

  constructor(private injector: Injector) {
    super();
  }

  public handleError(error) {
    const logService: LoggerService = this.injector.get(LoggerService);
    const authService: AuthenticationService = this.injector.get(AuthenticationService);
    const message = error.message ? error.message : error.toString();

    if (error.status) {
      error = new Error(message);
    }
    const user = authService.currentCustomerValue;
    const errorTraceStr = `*${message}*`;

    const attachments: any = [
      {
        title: 'Active URL',
        text: `\`\`\` ${window.location.href}\`\`\``,
        mrkdwn_in: [ 'text', 'pretext' ],
        color: '#00ff00'
      },
      {
        title: 'Stack Trace',
        text: `\`\`\`${error.stack}\`\`\``,
        mrkdwn_in: [ 'text', 'pretext' ],
        color: '#ff0000'
      },
      {
        title: 'User Agent',
        text: `\`\`\` ${window.navigator.userAgent}\`\`\``,
        mrkdwn_in: [ 'text', 'pretext' ],
        color: '#00ff00'
      }
    ];

    if (user) {
      const fields = Object.entries(user).map(([ key, val ]) => {
        return {
          title: key,
          value: val,
          short: true
        };
      });
      attachments.push({
        title: 'User Details',
        fields: fields,
        mrkdwn_in: [ 'text', 'pretext' ],
        color: '#0000ff'
      });
    }

    const isWarning = this.isWarning(errorTraceStr);

    const errorDetails = {
      text: errorTraceStr,
      attachments: attachments,
      icon_emoji: ':face_with_symbols_on_mouth:',
      as_user: false
    };
    logService.notifySlack(errorDetails).subscribe();


    throw error;
  }
  private isWarning(errorTraceStr: string) {
    let isWarning = true;
    // Error comes from app
    if (errorTraceStr.includes('/src/app/')) {
      isWarning = false;
    }

    this.sentencesForWarningLogging.forEach((whiteListSentence) => {
      if (errorTraceStr.includes(whiteListSentence)) {
        isWarning = true;
      }
    });

    return isWarning;
  }
}
