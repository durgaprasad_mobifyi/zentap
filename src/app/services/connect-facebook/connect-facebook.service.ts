import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import { AuthenticationService } from '../authentication/authentication.service';
import {AuthResponse, FacebookService, LoginResponse} from 'ngx-facebook';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {CustomersService} from '../customers/customers.service';
import {Customer} from '../../vos/customer/customer';
import {FacebookStepsComponent} from '../../components/facebook-steps/facebook-steps.component';

@Injectable({
  providedIn: 'root',
})
/**
 * Service class.
 */
export class ConnectFacebookService {
  isFbClicked = false;
  fbConnected = false;
  customer: Customer;
  private reloadAnalytics = new Subject<any>();
  constructor(http: HttpClient,
              authService: AuthenticationService,
              private fbService: FacebookService,
              private modalService: NzModalService,
              private customerService: CustomersService,
              private message: NzMessageService) {
    // super(Customer, http, authService);
  }
  getReloadAnalytics(): Observable<any> {
    return this.reloadAnalytics.asObservable();
  }
  setReloadAnytics(data) {
    this.reloadAnalytics.next(data);
  }
  selectPage () {
    if (!this.isFbClicked) {
      this.isFbClicked = true;
      this.fbService.getLoginStatus()
        .then((status) => {
          if (status.authResponse && status.status === 'connected') {
            this.fbService.api(`/v3.2/me/accounts?fields=name,id,picture`)
              .then(response => {
                this.isFbClicked = false;
                const pages = response.data.map(p => ({ label: p.name, value: p.id, image: p.picture.data.url }));
                if (pages.length) {
                  this.openFacebookSteps();
                } else {
                   this.linkFB();
                }
              });
          } else {
            this.linkFB();
          }
        });
    }
  }
  openFacebookSteps() {
    const modal = this.modalService.create({
      // nzTitle: 'Modal Title',
      nzContent: FacebookStepsComponent,
      nzFooter: null
    });
    modal.afterClose.subscribe(response => {
      this.setReloadAnytics(true);
    });
  }
  linkFB() {
     this.fbService.login({
      scope: 'public_profile,user_friends,email,pages_show_list',
      return_scopes: true,
      enable_profile_selector: true
    })
      .then((response: LoginResponse) => {
          this.isFbClicked = false;
          this.connectFB(response.authResponse);
        }
      )
      .catch((error: any) => {
        console.error(error);
      });
     return this.customer;
  }
  connectFB(response: AuthResponse) {
     this.customerService.linkFB(response.accessToken).subscribe(customer => {
        this.customer = customer;
        this.fbConnected = true;
        this.message.create('success', 'Connected');
        this.selectPage();
      },
      (err: any) => {
        console.error(err);
      });
  }
}
