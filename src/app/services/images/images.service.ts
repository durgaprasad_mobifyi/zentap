import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Customer } from 'src/app/vos/customer/customer';
import { CustomerResourceService } from '../customer-resource.service';
import { AuthenticationService } from '../authentication/authentication.service';

/**
 * Config class to be wired into an injector.
 * @see CoreModule#forRoot
 * @see https://angular.io/guide/dependency-injection#optional-dependencies
 */
export class CustomersServiceConfig {
  uri = `${environment.api_url}`;
}

@Injectable()

export class ImagesService extends CustomerResourceService<Customer> {
  public endpoint = 'images';
  private config; // Image Cropper Config FILE: image-cropper.component.ts[object]
  photoParams(image) {
    if (this.config.type === 'listing' && !this.config.id) {
      return {
        'listing': {
          params: {
            'image': {
              'image': image,
              'imageable_type': this.config.imageableType,
              'order': this.config.order,
              'uid': this.config.uid,
            }
          },
          isNew: true,
          url: `images`
        }
      }
    }
    if (this.config.type === 'listing' && this.config.id) {
      return {
        'listing': {
          params: {
            'image': {
              'image': image,
              'imageable_type': this.config.imageableType,
              'uid': this.config.uid
            }
          },
          isNew: false,
          url: `images/${this.config.id}` // "images/${ImageId}"
        }
      }
    }
    return {
      'headshot': {
        params: {
          'customer': { 'image': image }
        },
        isNew: true,
        aspectRatio: 1
      },
      'logo': {
        maintainAspectRatio: false,
        params: {
          'customer': {
            'logo': image,
          }
        },
        isNew: true,
        aspectRatio: 1
      },
      'family_photo': {
        params: {
          'listing': {
            'family_photo': image
          }
        },
        isNew: true,
        url: `listings/${this.config.id}/family_photo`
      },
      'attestant_photo': {
        params: {
          'image': {
            'image': image,
            'uid': this.config.uid,
            'imageable_type': 'Content',
            'key': 'FamilyPhoto',
          }
        },
        isNew: true,
        url: `images`
      }
    };
  }
  constructor(http: HttpClient, authService: AuthenticationService) {
    super(Customer, http, authService);
  }

  getPhotoParams(config, image) {
    this.config = config;
    return this.photoParams(image)[config.type];
  } 
}
