import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Customer } from '../../vos/customer/customer';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';
import { WebsiteAttributes } from '../../shared/interfaces/website-attributes';
import { ListingsService } from '../listings/listings.service';
import { CustomersService } from '../customers/customers.service';

@Injectable({
  providedIn: 'root'
})
export class WebsiteResolverService implements Resolve<WebsiteAttributes[]> {
  customer: Customer;
  constructor(
    private customerService: CustomersService,
    private authService: AuthenticationService,
    private listingService: ListingsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ) {
    const listingId = route.params['id'];
    this.customer = this.authService.currentCustomerValue;
     if (listingId) {
       return this.listingService.show(listingId).pipe(
         map((res) => {
           return res.data.websites_attributes;
         })
       );
     } else {
       return this.customer.websites_attributes;
     }

  }
}
