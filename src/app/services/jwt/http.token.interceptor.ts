import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { JwtService } from './jwt.service';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (
      req.url.indexOf('herokuapp.com') !== -1
      || req.url.indexOf('universalpromote.com') !== -1
      || req.url.indexOf('localhost') !== -1
    ) {
      const headersConfig = {
        'Accept': 'application/json'
      };


      const token = this.jwtService.getToken();
      let headers = req.headers.append('Accept', 'application/json');

      if (token) {
        headersConfig[ 'Authorization' ] = `${token}`;
        headers = headers.append('Authorization', `${token}`);
      } else {
        headers = headers.delete('Authorization');
      }
      const request = req.clone({ headers: headers });

      return next.handle(request);
    } else if (req.url.indexOf('slack') !== -1) {
      // if (req.method === 'OPTIONS') {
      //   return next.handle(req);
      // }
      // const headers = new HttpHeaders({
      //   'Authorization': 'Bearer xoxb-297220487605-509784639365-L2xTbHPXavqgc5YZnAxTur9S',
      //   'Content-type': 'application/json; charset=utf-8'
      // });
      // const access_token = 'xoxb-297220487605-509784639365-L2xTbHPXavqgc5YZnAxTur9S';
      // let headers = req.headers.append();
      // headers = headers.append('Content-type', 'application/json; charset=utf-8');


      // // headers = headers.delete('Content-Type');
      // const request = req.clone({ headers: headers });

      return next.handle(req);
    } else {
      return next.handle(req);
    }
  }
}
