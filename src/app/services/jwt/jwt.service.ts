import { Injectable } from '@angular/core';
import { Customer } from 'src/app/vos/customer/customer';
@Injectable()
export class JwtService {
  getToken(): String {
    return window.localStorage[ 'jwtToken' ];
  }

  getSavedCustomer(): Customer {
    const customerDetails = window.localStorage.getItem('customer');

    if (customerDetails !== undefined && customerDetails !== null) {
      const custDetails = JSON.parse(customerDetails);
      return new Customer(custDetails);
    } else {
      return undefined;
    }
  }

  saveToken(token: String) {
    if (token) {
      window.localStorage[ 'jwtToken' ] = token;
    }
  }

  saveCurrentCustomer(customer: Customer) {
    if (customer) {
      window.localStorage[ 'customer' ] = JSON.stringify(customer);
    } else {
      window.localStorage.removeItem('customer');
    }
  }

  destroyToken() {
    window.localStorage.removeItem('jwtToken');
    window.localStorage.removeItem('customer');
    window.localStorage.removeItem('awsToken');
    window.localStorage.removeItem('awsIdentity');
  }

  saveAWSIdentity(identity: String, token: String) {
    window.localStorage[ 'awsToken' ] = token;
    window.localStorage[ 'awsIdentity' ] = identity;
  }

  getAWSIdentity(): String {
    return window.localStorage[ 'awsIdentity' ];
  }

  getAWSToken(): String {
    return window.localStorage[ 'awsToken' ];
  }
}
