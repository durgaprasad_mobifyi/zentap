import { Injectable } from '@angular/core';
import { ContentsService } from './contents.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ContentResolverService {
  constructor(
    private contentService: ContentsService,
    private authService: AuthenticationService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ) {
    return this.contentService.list().pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
