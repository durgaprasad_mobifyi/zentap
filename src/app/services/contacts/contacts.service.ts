import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { Contact } from 'src/app/vos/contact/contact';
import { CustomerResourceService } from '../customer-resource.service';
import { AuthenticationService } from '../authentication/authentication.service';

/**
 * Config class to be wired into an injector.
 * @see CoreModule#forRoot
 * @see https://angular.io/guide/dependency-injection#optional-dependencies
 */
export class ContactsServiceConfig {
  uri = '';
}

@Injectable()
/**
 * Service class.
 */
export class ContactsService extends CustomerResourceService<Contact> {
  public endpoint = 'contacts';
  public data_key = 'contact';

  constructor(http: HttpClient, authService: AuthenticationService) {
    super(Contact, http, authService);
  }

  deleteContacts(pyaload): Observable<any> {
    return this.http.post(
      `${this._customerURI}${this.customerEndpoint}/destroy_mutiple`,
      pyaload,
    );
  }


}
