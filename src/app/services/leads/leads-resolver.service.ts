import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot } from '@angular/router';
import { LeadsService } from './leads.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LeadResolverService {

  constructor(
    private leadService: LeadsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ) {
    // const listing = route.params['id'];
    return this.leadService.list().pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
