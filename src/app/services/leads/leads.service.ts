import { Injectable } from '@angular/core';
import { CustomerResourceService } from '../customer-resource.service';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../authentication/authentication.service';
import { Lead } from 'src/app/vos/lead/lead';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

/**
 * Config class to be wired into an injector.
 * @see CoreModule#forRoot
 * @see https://angular.io/guide/dependency-injection#optional-dependencies
 */
export class LeadsServiceConfig {
  uri = '';
}

@Injectable()
/**
 * Service class.
 */
export class LeadsService extends CustomerResourceService<Lead> {

  public endpoint = 'leads';
  public data_key = 'lead';

  public _urls = `${environment.api_url}customers`;

  constructor(http: HttpClient, authService: AuthenticationService) {
    super(Lead, http, authService);
  }

  exportLeads(): Observable<any> {
    return this.http.get(
      `${this._customerURI}${this.customerEndpoint}.csv`,
      { responseType: 'text' }
    );
  }

}
