import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MarketReport } from '../../vos/market-report/market-report';
import { MarketReportsService } from './market-reports.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MarketReportsResolverService implements Resolve<MarketReport> {

  constructor(
    private marketReportService: MarketReportsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ) {
    const market_report = route.params['id'];
    return this.marketReportService.show(market_report).pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
