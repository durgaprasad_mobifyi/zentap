import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable()
export class WebsocketService {

  constructor(private socket: Socket) { }

  sendMessage(channel, msg: string) {
    this.socket.emit(channel, msg);
  }
  getMessage(channel) {
    return this.socket.fromEvent(channel);
  }
}
