// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_url: 'https://dev-api.universalpromote.com/',
  wss_url: 'wss://dev-api.universalpromote.com/cable',
  // api_url: ' http://localhost:3000/',
  env_string: 'Development',
  prop_mix_url: 'https://staging-api.propmix.io/mls/v1',
  prop_mix_token: 'f91e3d5a77ad4608189bbddeb94abd3c61cbce674edbd4dc7ca406ed38fad45f',
  stripe_key: 'pk_test_bDNhDbHHvHrvvM5cTMc7QNLW',
  linkedin_configs: {
    linkedin_url: 'https://www.linkedin.com/oauth/v2/authorization?response_type=code',
    redirect_uri: 'https://localhost:4200/linkedin',
    client_id: '869yyt2403h0rs',
    scope: 'r_emailaddress r_ads rw_ads r_basicprofile r_liteprofile w_organization_social r_organization_social rw_organization_admin w_member_social'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.
