export const environment = {
  production: false,
  api_url: 'http://localhost:3000/',
  env_string: 'Local',
  prop_mix_url: 'https://staging-api.propmix.io/mls/v1',
  prop_mix_token: 'f91e3d5a77ad4608189bbddeb94abd3c61cbce674edbd4dc7ca406ed38fad45f',
  stripe_key: 'pk_test_bDNhDbHHvHrvvM5cTMc7QNLW',
  wss_url: 'ws://localhost:3000/cable',
  linkedin_configs: {
    linkedin_url: 'https://www.linkedin.com/oauth/v2/authorization?response_type=code',
    redirect_uri: 'http://localhost:3000/linkedin',
    client_id: '869yyt2403h0rs',
    scope: 'r_liteprofile r_emailaddress w_member_social'
  }
};
