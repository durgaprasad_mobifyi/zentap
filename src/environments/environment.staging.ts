export const environment = {
  production: true,
  api_url: 'https://staging-api.universalpromote.com/',
  env_string: 'Staging',
  prop_mix_url: 'https://staging-api.propmix.io/mls/v1',
  prop_mix_token: 'f91e3d5a77ad4608189bbddeb94abd3c61cbce674edbd4dc7ca406ed38fad45f',
  stripe_key: 'pk_test_bDNhDbHHvHrvvM5cTMc7QNLW',
  wss_url: 'wss://staging-api.universalpromote.com/cable',
  linkedin_configs: {
    linkedin_url: 'https://www.linkedin.com/oauth/v2/authorization?response_type=code',
    redirect_uri: 'https://staging.zentap.com/linkedin',
    client_id: '869yyt2403h0rs',
    scope: 'r_emailaddress r_ads rw_ads r_basicprofile r_liteprofile w_organization_social r_organization_social rw_organization_admin w_member_social'
  }
};
